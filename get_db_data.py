import pickle
import pandas as pd
from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.LoadEnv import load_env
import datetime

def main(selected_hidrologies:list=None):
    def create_index(table:str, index_name:str, columns:str):
        obj_db = DBQueryMySQL()
        operation = f'CREATE INDEX {index_name} ON {table} ({columns})'
        try:
            obj_db.execute_query(query='standard', operation=operation)
        except:
            print(f'No se ha creado el indice {index_name}')

    def get_fieldvalues_from_table(table:str, field:str):
        obj_db = DBQueryMySQL()
        operation = f'SELECT DISTINCT({field}) FROM {table}'
        results = obj_db.execute_query(query='standard', operation=operation) 
        df = pd.DataFrame.from_dict(results)
        return list(df[field].unique())

    def get_data_from_table(hidrologies:list, table:str, columns:str,
                            index_name:str, index_cols:str):
        if hidrologies == None:
            hidrologies = get_fieldvalues_from_table(table=table,
                                                     field='hidrology')
        names = get_fieldvalues_from_table(table=table,
                                           field='name')
        
        create_index(table=table, index_name=index_name, columns=index_cols)
        d_df = {}
        obj_db = DBQueryMySQL()

        for hidro in hidrologies:
            d_df.update({hidro: None})
            now = datetime.datetime.now()
            print(f'Solicitando data para "{table}, {hidro}": {now}')
            for name in names:
                operation = f"""SELECT {columns} FROM {table}
                                WHERE hidrology = "{hidro}"
                                AND name = "{name}";"""
                results = obj_db.execute_query(query='standard',
                                               operation=operation)
                df = pd.DataFrame.from_dict(results)
                d_df[hidro] = pd.concat([d_df[hidro], df], ignore_index=True)
        return d_df

    ##################################################################
    ## The good stuff!
    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')
    d_df_bus = None
    d_df_gen = None
    cols_busbars_str = 'date_time, name, price_value'
    cols_generators_str = 'date_time, name, gen_value'
    index_cols_str = 'hidrology, name'
    
    print(f'Comenzando consulta para barras {datetime.datetime.now()}')
    d_df_bus = get_data_from_table(hidrologies=selected_hidrologies,
                                   table='busbars',
                                   columns=cols_busbars_str,
                                   index_name='inx2',
                                   index_cols=index_cols_str)
    
    print(f'Comenzando consulta para generadores {datetime.datetime.now()}')
    d_df_gen = get_data_from_table(hidrologies=selected_hidrologies,
                                   table='generators',
                                   columns=cols_generators_str,
                                   index_name='inx2',
                                   index_cols=index_cols_str)
    
    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo total de ejecucion: {t_delta}')

    # Save to disk
    with open('dict_df.pkl', 'wb') as f:
        pickle.dump([d_df_gen, d_df_bus], f)

    return d_df_gen, d_df_bus

# Getting back the objects:
# with open('dict_df.pkl', 'rb') as f:
#     d_df_gen, d_df_bus = pickle.load(f)