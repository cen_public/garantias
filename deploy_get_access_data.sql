use garantias_renova;
drop table if exists retiros;
CREATE TABLE IF NOT EXISTS retiros (
    day CHAR(2),
    month CHAR(2),
    hour CHAR(3),
    type CHAR(2),
    date_time DATETIME,
    clave_ano_mes INT,
    hora_mensual INT,
    barra nvarchar(30),
    suministrador nvarchar(30),
    retiro nvarchar(100),
    clave nvarchar(500),
    tipo CHAR(3),
    medida_kWh FLOAT);
