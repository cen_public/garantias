import os, sys, clr
import pandas as pd
sys.path.append(r"C:\Program Files\Energy Exemplar\PLEXOS 9.0 API")
clr.AddReference('PLEXOS_NET.Core')
clr.AddReference('EEUTILITY')
clr.AddReference('EnergyExemplar.PLEXOS.Utility')
import PLEXOS_NET.Core as plx
from configobj import ConfigObj
from EEUTILITY.Enums import SimulationPhaseEnum, CollectionEnum
from EnergyExemplar.PLEXOS.Utility.Enums import PeriodEnum, SeriesTypeEnum
# from System import Enum, Type

def list_enum_names(enum):
    try:
        if not enum.IsEnum:
            return ''
        return '\n\t'.join([''] + ['{} = {}'.format(a, int(b)) for a, b in zip(enum.GetEnumNames(), enum.GetEnumValues())])
    except:
        return ''

folder = os.path.dirname(__file__)
with open(os.path.join(folder, 'query_enums_collection.txt'),'w') as fout:
    # traverse all enums
    for t in clr.GetClrType(CollectionEnum).Assembly.GetTypes():
        if t.IsEnum:
            fout.write('{}{}\n\n'.format(t.Name, list_enum_names(t)))