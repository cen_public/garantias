import os
import mysql.connector
from mysql.connector.errors import ProgrammingError
from SharedCode.Database.AbstractDBQuery import AbstractQueryODBC

class DBQueryMySQL(AbstractQueryODBC):
    """ Class to run queries on MySQL databases
        Important: Answers should be dictionary objects
        ref: https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html """
    
    def __init__(self):
        super().__init__()
        self.cnxn_dict = {'user': os.environ.get('DB_USER', ''),
                          'password': os.environ.get('DB_PASS', ''),
                          'host': os.environ.get('DB_SERVER', ''),
                          'database': os.environ.get('DB_NAME')}

        self.queries_str.update({'show_tables': self.pre_stored_query_0,
                                 'insert': self.pre_stored_insert,
                                 'get_ins_credentials': self.pre_stored_query_1,
                                 'get_az_connstring': self.pre_stored_query_2,
                                 'check_login': self.pre_stored_query_3})

        self.tables = {'credentials': 'credentials_vault'}
        self.connect_args = {'allow_local_infile': True} # It is a must

    def set_cnxn(self, username:str='', password:str='') -> bool:
        result = False
        if username != '' and password != '': # if user and pass need to be changed
            self.cnxn_dict.update({'user': username,
                                   'password': password})
        if self.cnxn is None:
            n_retries = 0
            while n_retries < 4:
                try:
                    self.cnxn = mysql.connector.connect(**self.cnxn_dict)
                except ProgrammingError as pe: # Wrong credentials most likely
                    raise 
                except Exception as e:
                    print(e)
                    print(f'Retry {n_retries}')
                    n_retries += 1
                else:
                    result = True
                    break
        return result
        
    def set_cursor(self):
        if self.cnxn is not None and self.cursor is None:
            try:
                self.cursor = self.cnxn.cursor(dictionary=True) # Dictionary
            except Exception as e:
                print(e)

    def execute_query(self, query:str, **kwargs):
        """ It executes a pre-stored query using args
            query: (str), name of a pre-stored query
            kwargs: (dict), args."""
        self.set_cnxn()        
        self.set_cursor()
        # is_destructive = kwargs.get('is_destructive', False)
        operation, params, is_destructive = self.queries_str[query.lower()](**kwargs)       
        answer = None
        if self.cursor is not None:
            self.cursor.execute(operation=operation, params=params)
            self.cnxn.commit() if is_destructive else lambda x: None
            answer = self.cursor.fetchall() # fetchall return a List object
            # answer = answer[0] if len(answer) > 0 else {} # dict
            self.cursor.close()
            self.cnxn.close()
            self.cursor = None
            self.cnxn = None
        return answer

    def standard_query(self, **kwargs):
        return super().standard_query(**kwargs)
    
    def drop_index(self, table, name):
        super().drop_index(table=table, name=name)
    
    def create_index(self, table, name, columns):
        super().create_index(table=table, name=name, columns=columns)

    def insert_df(self, table:str, df, if_exists:str='fail', index:bool=False, index_label:list=None):
        super().insert_df(table=table, df=df, if_exists=if_exists, index=index, index_label=index_label)
    
    def insert_from_file(self, table:str, csv_file, fieldsterminatedby:str='\';\'',
                         linesterminatedby:str=repr('\n')):
        super().set_engine_sqlalchemy()
        con = self.engine.connect()
        trans = con.begin()
        operation = ('LOAD DATA LOCAL INFILE \''
                    + csv_file.replace('\\', '\\\\')
                    + '\' INTO TABLE '
                    + table
                    + f' FIELDS TERMINATED BY {fieldsterminatedby}'
                    + f' LINES TERMINATED BY {linesterminatedby}'
                    + ' IGNORE 1 LINES')
        con.execute(operation)
        trans.commit()

    def pre_stored_insert(self, **kwargs):
        """ table: str, columns=tuple, values=tuple """
        try:
            table = kwargs.get('table')
            columns = kwargs.get('columns')
            params = kwargs.get('params')
        except KeyError:
            raise

        # Check types in table, values and columns
        if not isinstance(table, str):
            raise TypeError('table must be a str')

        if not isinstance(params, tuple):
            raise TypeError('values must be a tuple')
        
        # Check dimension
        if len(columns) != len(params):
            raise ValueError('columns and values must have same length')
        
        for column in columns:
            if not isinstance(table, str):
                raise TypeError(f'{column} must be a str')
        
        n_cols = len(columns)
        columns_str = str(columns).replace('\'', '')
        suffix = str(tuple(n_cols * [None])).replace('None', '%s')
        operation = f'INSERT INTO {table} {columns_str} VALUES {suffix}'
        # Return operation, params, is_destructive
        return operation, params, True

    def pre_stored_query_0(self, **kwargs):
        """ Show tables query (Basic test)"""
        operation = 'SHOW TABLES'
        # Return operation, params, is_destructive
        return operation, None, False

    def pre_stored_query_1(self, **kwargs):
        """ Get insurance credentials query """
        try:
            broker_key = kwargs['broker_key']
        except KeyError:
            raise
        if isinstance(broker_key, str):
            operation = ('SELECT insurancecompany, username, password FROM '
                         + self.tables['credentials']   
                         + ' WHERE broker_key = ' + broker_key)
            # Return operation, params, is_destructive
            return operation, None, False
        else:
            raise TypeError ('broker_key should be str')
        
    def pre_stored_query_2():
        pass

    def pre_stored_query_3(**kwargs):
        try:
            username = kwargs['username']
            password = kwargs['password']
        except KeyError:
            raise
        if isinstance(username, str) and isinstance(password, str):
            operation = ('SELECT insurancecompany, username, password FROM '
                         + self.tables['credentials']   
                         + ' WHERE broker_key = ' + broker_key)
            # Return operation, params, is_destructive
            return operation, None, False
        else:
            raise TypeError ('username and password should be str')

