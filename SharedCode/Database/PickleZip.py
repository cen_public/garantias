import io, pickle, zipfile, gc, tempfile, os

class PickleByte():
    def __init__ (self):
        self.content = None

    def write(self, pkl):
        # TypeError: object of type '_io.BytesIO' has no len()
        # io.BytesIO.read() is a file-like object with len
        self.content = io.BytesIO(pkl).read()
    
    # def read(self, size): 
    #     return self.content_r.read(size)

    # def readline(self):
    #     return self.content_r.readline()


class PickleZip():
    """ Class to zip pickle file to save disk space"""

    def dump(varname:object, zip_file:str):
        pb = PickleByte()
        # pickle.dump(varname, pb)
        pb.content_w = pickle.dumps(varname)
        with zipfile.ZipFile(file=zip_file, mode='w',
                             compression=zipfile.ZIP_DEFLATED) as zfile:
            zfile.writestr('pickle.pkl', pb.content_w)
        
        del pb
        gc.collect()

    def load(zip_file:str):
        with tempfile.TemporaryDirectory() as td:
            with zipfile.ZipFile(file=zip_file, mode='r',
                                 compression=zipfile.ZIP_DEFLATED) as zfile:
                zfile.extract('pickle.pkl', path=td)
            pkl_file = os.path.join(td, 'pickle.pkl')

            with open(file=pkl_file, mode='rb') as f:
                return pickle.load(f)
    
    def dump_str(varname, zip_file:str):
        var_str = pickle.dumps(varname)
        with zipfile.ZipFile(file=zip_file, mode='w') as zfile:
            zfile.writestr('pickle.pkl', var_str)
        
        del var_str
        gc.collect()
    
    def load_str(zip_file:str):
        with tempfile.NamedTemporaryFile(mode='wb') as tf:
            with zipfile.ZipFile(file=zip_file, mode='r') as zfile:
                zfile.extract('pickle.pkl', tf.name)
                var = pickle.load(tf.name)
        del var_str
        gc.collect()
        return var
