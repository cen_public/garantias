from abc import ABC, abstractmethod
import os
from sys import path
import pyodbc
import mysql.connector
import time
from sqlalchemy import create_engine

class AbstractQueryODBC(ABC):
    @abstractmethod
    def __init__(self):
        self.cnxn = None # Connection object
        self.cursor = None # Cursor object
        self.queries_str = {'standard': self.standard_query}
        self.connect_args= {}

    @abstractmethod
    def set_cnxn(self):
        pass

    @abstractmethod
    def set_cursor(self):
        pass
    
    @abstractmethod
    def execute_query(self, query_str:str):
        pass

    @abstractmethod
    def standard_query(self, **kwargs):
        is_destructive = kwargs.get('is_destructive', False)
        try:
            operation = kwargs['operation']
            params = kwargs.get('params', None)
        except KeyError:
            raise

        # Check operation, params and is_destructive. Then return
        if not isinstance(operation, str):
            raise TypeError ('operation should be a str')
        if params is not None and not isinstance(params, tuple):
            raise TypeError ('params should be a tuple')
        if not isinstance(is_destructive, bool):
            raise TypeError ('is_destructuve should be a bool')
        return operation, params, is_destructive

    def set_engine_sqlalchemy(self):
        self.set_cnxn()
        self.set_cursor()
        db = os.environ.get('DB_NAME', '')
        user = os.environ.get('DB_USER', '')
        passw = os.environ.get('DB_PASS', '')
        engine_str = f'mysql+mysqlconnector://{user}:{passw}@localhost/{db}'
        self.engine = create_engine(engine_str,
                                    connect_args=self.connect_args)


    @abstractmethod
    def insert_df(self, table:str, df, if_exists:str='fail', index:bool=False, index_label:list=None):
        self.set_engine_sqlalchemy()
        df.to_sql(name=table, con=self.engine, if_exists=if_exists,
                  index=index, index_label=index_label)
    
    @abstractmethod
    def insert_from_file(self, table:str, csv_file, fieldsterminatedby,
                         linesterminatedby:str):
        pass

    @abstractmethod
    def drop_index(self, table:str, name:str):
        try:
            operation = f'ALTER TABLE {table} DROP INDEX {name}'
            self.execute_query(query='standard', operation=operation)
        except Exception as e:
            print(f'drop_index: no se pudo eliminar el indice {name} desde {table}')
            print(e)

    @abstractmethod
    def create_index(self, table:str, name:str, columns:tuple):
        print(columns)
        columns_str = self._tuple_to_str(arg_tuple=columns)
        try:
            
            operation = f'CREATE INDEX {name} ON {table} {columns_str}'
            print(operation)
            self.execute_query(query='standard', operation=operation)
        except Exception as e:
            print(f'create_index: no se pudo crear el indice {name} en {table}')
            print(e)
    
    def _tuple_to_str(self, arg_tuple:tuple):
        result = str(arg_tuple)
        result = result.replace('\'', '')
        result = result.replace('"', '')
        return result   