import pickle
import pandas as pd
from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.LoadEnv import load_env
import datetime


def create_index(table:str, index_name:str, columns:str):
    obj_db = DBQueryMySQL()
    operation = f'CREATE INDEX {index_name} ON {table} ({columns})'
    try:
        obj_db.execute_query(query='standard', operation=operation)
    except:
        print(f'No se ha creado el indice {index_name}')

def get_fieldvalues_from_table(table:str, field:str):
    obj_db = DBQueryMySQL()
    operation = f'SELECT DISTINCT({field}) FROM {table}'
    results = obj_db.execute_query(query='standard', operation=operation) 
    df = pd.DataFrame.from_dict(results)
    return list(df[field].unique())

def get_data_from_table_by_hidro(hidrologies:list, table:str, columns:str):
    if hidrologies == None:
        hidrologies = get_fieldvalues_from_table(table=table,
                                                 field='hidrology')
    names = get_fieldvalues_from_table(table=table,
                                       field='name')
    
    d_df = {}
    obj_db = DBQueryMySQL()

    for hidro in hidrologies:
        d_df.update({hidro: None})
        now = datetime.datetime.now()
        print(f'Solicitando data para "{table}, {hidro}": {now}')
        for name in names:
            operation = f"""SELECT {columns} FROM {table}
                            WHERE hidrology = "{hidro}"
                            AND name = "{name}";"""
            results = obj_db.execute_query(query='standard',
                                            operation=operation)
            df = pd.DataFrame.from_dict(results)
            d_df[hidro] = pd.concat([d_df[hidro], df], ignore_index=True)
    return d_df


   