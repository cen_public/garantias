import pandas as pd
import numpy as np

class BerncReader:
    def __init__(self, cfg):
        self.cfg = cfg

    def read_excel(self, bernc_path:str):
        sheet_name = self.cfg['sheet_name']
        skiprows = int(self.cfg['skiprows'])
        usecols = self.cfg['usecols']
        df = pd.read_excel(bernc_path, 
                        sheet_name=sheet_name,
                        skiprows=skiprows,
                        usecols=usecols)
        cols_names = self.cfg['cols_names']
        df = df[cols_names]
        return df
    
    def normalize_df(self, df:pd.DataFrame, version:str):
        pos_nan = list(np.where(df.iloc[:,0].isna())[0])
        df_norm = df
        if version == 'old':
            df_norm = df.iloc[pos_nan[1]+1:pos_nan[2],:]
            df_norm.reset_index(inplace=True, drop=True)
        return df_norm