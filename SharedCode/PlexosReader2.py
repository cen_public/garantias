import os, sys, clr, gc, datetime, time
import pandas as pd
sys.path.append(r"C:\Program Files\Energy Exemplar\PLEXOS 9.0 API")
clr.AddReference('PLEXOS_NET.Core')
clr.AddReference('EEUTILITY')
clr.AddReference('EnergyExemplar.PLEXOS.Utility')
import PLEXOS_NET.Core as plx
from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from EEUTILITY.Enums import SimulationPhaseEnum, CollectionEnum
from EnergyExemplar.PLEXOS.Utility.Enums import PeriodEnum, SeriesTypeEnum
# from tempfile import NamedTemporaryFile

class PlexosReader2:
    collection_enum = {'generators': CollectionEnum.SystemGenerators,
                       'gen_companies': CollectionEnum.GeneratorCompanies,
                       'busbars': CollectionEnum.SystemNodes}
    properties = ConfigObj('SharedCode/properties.cfg')

    def __init__(self, sol_file):
        """sol_file: solution file path"""
        self.sol = plx.Solution()
        self.sol.Connection(sol_file) 
        
    def get_results(self, type:str, property:str=''):
        props = self.properties[type][property] if property !='' else ''
        results = self.sol.Query(SimulationPhaseEnum.MTSchedule,
                                 self.collection_enum[type],
                                 '',
                                 '',
                                 PeriodEnum.Interval,
                                 SeriesTypeEnum.Values,
                                 props)
        
        # res_rows = results.GetRows()
        # df = pd.DataFrame([[res_rows[i, j] for i in range(res_rows.GetLength(0))]
        #                    for j in range(res_rows.GetLength(1))],
        #                    columns = [x.Name for x in results.Fields] )
        
        # return df
        return results
    
    def get_results_csv(self, type:str, property:str='', usecols:list=None,
                        datetime_col:str=None, rename_cols:dict=None,
                        file:str='output.csv', table:str=None,
                        column_split:str=None, index_db:list=None,
                        value_col:str=None, index:bool=False,
                        index_label:list=None, upload_from_file:bool=False,
                        return_df:bool=False):

        # f = NamedTemporaryFile()
        props = self.properties[type][property] if property !='' else ''
        print('PlexosReader: getting info from Plexos file')
        results = self.sol.QueryToCSV(file, False,
                                      SimulationPhaseEnum.MTSchedule,
                                      self.collection_enum[type],
                                      '',
                                      '',
                                      PeriodEnum.Interval,
                                      SeriesTypeEnum.Values,
                                      props)       
        del results
        gc.collect()

        csv_folder = os.getcwd()
        csv_path = os.path.join(csv_folder, file)

        obj_db = DBQueryMySQL()
        
        # Part 1 Create table
        with open(file='sql_op/query_upload_plx_part1.sql', mode='r') as f:
            operation = f.read()
        operation = operation.replace('table_name', table)
        res = obj_db.execute_query(query='standard', operation=operation)
        
        # Part 2: Upload csv
        obj_db.execute_query(query='standard', operation='SELECT 1') # flush
        retries = 1
        while retries < 3:
            try:
                obj_db.insert_from_file(table=table, csv_file=csv_path, fieldsterminatedby='\',"\'')
            except:
                time.sleep(2**retries)
                retries +=1
            else:
                retries = 3
        
        # Part 3: do some changes in table
        with open(file='sql_op/query_upload_plx_part2.sql', mode='r') as f:
            operation = f.read()
        operation = operation.replace('table_name', table)
        obj_db.execute_query(query='standard', operation=operation)
        obj_db.execute_query(query='standard', operation='SELECT 1') # flush

        obj_db.create_index(table=table, name='inx', columns=('hidrology', 'name', 'date_time'))
    