class Dict(dict):
    def __missing__(self, key):
        return None
    
    def pop(self, key):
        super().pop(key) if key in self.keys() else lambda x: None