import pyodbc
import pandas as pd

class ValReader:
    def __init__(self, cfg):
        self.origin = cfg['origin']
        self.cfg = cfg[self.origin]
        self.readers = {
            'CSV': self._read_csv, 
            'MDB': self._read_mdb
            }
        
    def read(self, val_path:str):
        return self.readers[self.origin](val_path)
        
    def _read_csv(self, val_path):
        
        df = pd.read_csv(val_path, usecols=self.cfg['cols'])
        df = df.sort_values(by=['clave', 'Cuarto de Hora'])
        df_grouped = df.groupby(['clave', df.groupby('clave').cumcount() // 4]).agg({
            'nombre_barra': 'first',
            'Cuarto de Hora': 'first',
            'propietario': 'first',
            'descripcion': 'first',
            'MedidaHoraria': 'sum',
            'tipo1': 'first',}).reset_index()
        df_grouped['Cuarto de Hora'] = df_grouped.groupby('clave').cumcount() + 1
        return df_grouped[self.cfg['cols']]

    def _read_mdb(self, val_path:str):
        driver = self.cfg['driver'][0] + ', ' + self.cfg['driver'][1]
        table_val = self.cfg['table_val']
        cnxn_str = f'Driver={driver};DBQ={val_path};'
        conn = pyodbc.connect(cnxn_str)
        df = pd.read_sql(f'select * from {table_val}', conn)
        try:
            cols = self.cfg['cols']
            df = df[cols]
        except KeyError:
            pass
        # Multi-index
        try:
            cols_multiindex = self.cfg['cols_multiindex']
        except KeyError:
            return df
        else:
            df.index = pd.MultiIndex.from_frame(df=df[cols_multiindex]) 
            return df