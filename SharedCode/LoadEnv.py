import os
from configobj import ConfigObj

def load_env(filename):
    try:
        cfg = ConfigObj(filename, list_values=False)
    except:
        print('error inesperado')
    else:
        for k, v in cfg.items():
            os.environ[k] = str(v).lower()