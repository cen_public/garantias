import os, sys, clr, gc, datetime
import pandas as pd
sys.path.append(r"C:\Program Files\Energy Exemplar\PLEXOS 9.0 API")
clr.AddReference('PLEXOS_NET.Core')
clr.AddReference('EEUTILITY')
clr.AddReference('EnergyExemplar.PLEXOS.Utility')
import PLEXOS_NET.Core as plx
from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.Database.PickleZip import PickleZip
from EEUTILITY.Enums import SimulationPhaseEnum, CollectionEnum
from EnergyExemplar.PLEXOS.Utility.Enums import PeriodEnum, SeriesTypeEnum
# from tempfile import NamedTemporaryFile

class PlexosReader:
    collection_enum = {'generators': CollectionEnum.SystemGenerators,
                       'gen_companies': CollectionEnum.GeneratorCompanies,
                       'busbars': CollectionEnum.SystemNodes}
    properties = ConfigObj('SharedCode/properties.cfg')

    def __init__(self, sol_file):
        """sol_file: solution file path"""
        self.sol = plx.Solution()
        self.sol.Connection(sol_file) 
        
    def get_results(self, type:str, property:str=''):
        props = self.properties[type][property] if property !='' else ''
        results = self.sol.Query(SimulationPhaseEnum.MTSchedule,
                                 self.collection_enum[type],
                                 '',
                                 '',
                                 PeriodEnum.Interval,
                                 SeriesTypeEnum.Values,
                                 props)
        
        return results
    
    def get_results_csv(self, type:str, year:int, property:str='',
                        usecols:list=None, datetime_col:str=None,
                        rename_cols:dict=None, file:str='output.csv',
                        table:str=None, column_split:str=None,
                        index_db:list=None, value_col:str=None,
                        index:bool=False, index_label:list=None,
                        upload_from_file:bool=False, return_df:bool=False):

        props = self.properties[type][property] if property !='' else ''
        print('PlexosReader: getting info from Plexos file')
        results = self.sol.QueryToCSV(file, False,
                                      SimulationPhaseEnum.MTSchedule,
                                      self.collection_enum[type],
                                      '',
                                      '',
                                      PeriodEnum.Interval,
                                      SeriesTypeEnum.Values,
                                      props)       
        del results
        gc.collect()

        now = datetime.datetime.now()
        print(f'PlexosReader: reading {file} {now}')
        df = pd.read_csv(file)
        cols_to_drop = []
        for col in df.columns:
            cols_to_drop.append(col) if col not in usecols else None
        df.drop(columns=cols_to_drop, inplace=True)
        if rename_cols is not None:
            df.rename(columns=rename_cols, inplace=True)
            df = df[rename_cols.values()] # order is established in config file
        
        # Check/Change value_col format
        if value_col is not None:
            foo = df[value_col].astype(str).str.replace(',', '.')
            df[value_col] = foo.astype(float)
        
        # Check/Change date_time format 
        if datetime_col is not None:
            df[datetime_col] = pd.to_datetime(df[datetime_col], dayfirst=True) 
   
        # delete row from differt year
        initial_date = datetime.datetime(year=year, month=1, day=1)
        final_date = datetime.datetime(year=year + 1 , month=1, day=1)
        inx_year = (df[datetime_col] >= initial_date) & (df[datetime_col] < final_date)
        df = df.loc[inx_year].copy(deep=True)

        # Re-write csv file or files, if column_split is not None
        list_df = []
        dict_df = {}
        if column_split is not None:
            column_split_values = df[column_split].unique()
            for foo in column_split_values:
                df_f = df[df[column_split] == foo]
                list_df.append(df_f)
                dict_df.update({foo: df_f})
        else:
            list_df.append(df)

        # Create pickle file with a dictionary
        print(f'PlexosReader: {table} creating pickle file')
        picklezip_file = f'pkl//input//plx_{type}.zip'
        PickleZip.dump(varname=dict_df, zip_file=picklezip_file)

        for inx, df_i in enumerate(list_df, start=0):
            file_name = f'{str(inx)}{file}'
            now = datetime.datetime.now()
            print(f'PlexosReader: writing {file_name} {now}')
            df_i.to_csv(file_name, sep=';', index=False)
            
        # if table is not None: # which means, info must be uploade into db
        #     # Send values to Database
        #     obj_db = DBQueryMySQL()
        #     index = 'inx'
        #     if upload_from_file:
        #         # Drop index
        #         try:
        #             op_drop_inx = f'DROP INDEX {index} ON {table}'
        #             obj_db.execute_query(query='standard', operation=op_drop_inx)
        #         except Exception as e:
        #             print(f'PlexosReader:no se pudo botar el indice, {e}')

        #         for inx in range(0, len(list_df)):
        #             file_name = f'{str(inx)}{file}'
        #             now = datetime.datetime.now()
        #             print(f'PlexosReader: uploading {file_name} into df {now}')
        #             obj_db.insert_from_file(table=table, csv_file=file_name)
                
        #         # create index again
        #         print(f'PlexosReader: {table} creating index on DB')
        #         op_create_inx = f'CREATE INDEX {index} ON {table} {tuple(index_db)}'
        #         op_create_inx = op_create_inx.replace('\'', '')
        #         obj_db.execute_query(query='standard', operation=op_create_inx)
        #     else:    
        #         try:
        #             obj_db.insert_df(table=table, df=df, if_exists='append',
        #                              index=index, index_label=index_label) 
        #         except:
        #             print(f'No se pudo insertar el dataframe')
        
        # Remove csv files
        for inx in range(0, len(list_df)):
            os.remove(str(inx) + file)

        if not return_df:
            del df
            del list_df
            del dict_df
            gc.collect()
        else:
            return df

        
    