import os, gc
from numpy import var
import pandas as pd
from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.Database.PickleZip import PickleZip
from SharedCode.ValReader import AccessReader
from SharedCode.LoadEnv import load_env
from SharedCode.PlexosReader import PlexosReader
import datetime
from dateutil.relativedelta import relativedelta

class Dict(dict):
    def __missing__(self, key):
        return key

    
def main(mdb_path:str=None, drop_tables:bool=True, upload_to_db:bool=False):
    def run_drop_tables():
        pass
        obj_db = DBQueryMySQL()
        with open(file='deploy_get_access_data.sql', mode='r') as f:
            deploy_str = f.read()
        obj_db.execute_query(query='standard',
                             operation = deploy_str,
                             params=None,
                             is_destructive=False) # It is non_destructive :\

    def create_df_ret(mdb_path:str, month_date:datetime):
        # Create a DataFrame from MS Access DB
        cfg_db = cfg['GARANTIAS']['RETIROS_DB']
        a_reader = AccessReader(cfg=cfg_db)
        df_ret = a_reader.read_db(db_path=mdb_path)
        
        # Agregar una columna datetime
        cols = df_ret.columns.to_list()
        cols[0] = cols[0].replace('\ufeff', '')  # weird char
        cols[0] = cols[0].replace('Ã±', 'ñ')  # weird char
        df_ret.columns = cols
        df_ret.rename(columns=cfg_db['COLUMNS_RENAME'], inplace=True)
        df_ret['date_time'] = (pd.to_datetime(df_ret['clave_ano_mes'], format='%y%m')
                              + (df_ret['hora_mensual'] - 1).astype('timedelta64[h]'))
        df_ret['date_time'] = df_ret['date_time'].astype('datetime64')
        df_ret.drop(df_ret[df_ret['tipo'] == 'L_S'].index, inplace=True)

        # Eliminar datos que no son del mes
        limit_date = month_date + relativedelta(months=1)
        df_ret.drop(df_ret[df_ret['date_time'] >= limit_date].index, inplace=True)
        
        # Convert datetime to string with the format "%w-%m-%H"
        map_day = Dict()
        map_hour = Dict()
        map_day.update(cfg['GARANTIAS']['PLEXOS_DATE']['DAYS'])
        map_hour.update(cfg['GARANTIAS']['PLEXOS_DATE']['HOURS'])
        
        if len(df_ret) > 0:
            foo = df_ret['date_time'].dt.strftime('%d-D%w-%m-H%H')
            foo = foo.str.split('-', expand=True)
            df_ret['day_month'] = foo[0]
            foo[1] = foo[1].map(map_day) # week day (0:Sunday)
            foo[3] = foo[3].map(map_hour) #hour
            foo[4] = df_ret['tipo'].str.replace('L_D', 'LD') # type
            foo.rename(columns={1:"day", 2:"month", 3:"hour", 4:'type'}, inplace=True)
            df_ret.index = pd.MultiIndex.from_frame(foo[['month', 'day', 'hour', 'type']])    
            df_ret.sort_index(inplace=True)
            df_ret['suministrador_clave'] = (df_ret['suministrador'].astype(str)
                                            + '_&_' + df_ret['clave'].astype(str))
        else:
            df_ret['suministrador_clave'] = []
            foo = pd.DataFrame({'month': [], 'day': [], 'hour': [], 'type': []})
            df_ret.index = pd.MultiIndex.from_frame(foo)                 

        df_ret = df_ret[['date_time', 'clave_ano_mes', 'hora_mensual',
                        'barra', 'suministrador_clave', 'suministrador',
                        'retiro', 'clave', 'tipo', 'medida_kWh']]
        return df_ret
    
    def write_into_db(df:pd.DataFrame):
        # Send values to Database    
        obj_db = DBQueryMySQL()
        csv_file = 'output_temp.csv'
        df.to_csv(csv_file, sep=';', date_format='%Y-%m-%d %H:%M:%S')
        try:
            obj_db.insert_from_file(table='retiros',
                                    csv_file=csv_file)
        except Exception as e:
            print(f'No se pudo insertar el dataframe')
            print(e)
        os.remove(csv_file)

    ###################################################
    # For develpoing purposes only
    if mdb_path is None:
        cwd = os.getcwd()
        mdb_path = os.path.join(cwd, 'Input_test', 'Garantias', 'retiros')
    
    ##################################################################
    ## The good stuff!
    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')
    run_drop_tables() if drop_tables == True else lambda x:None
    o_db = DBQueryMySQL()
   
    # mdb files iterator
    iter_mdb_files = os.scandir(mdb_path)
    mdb_files = (entry for entry in iter_mdb_files
                 if entry.is_file() and entry.name[-4:] == '.mdb')

    # Iter over mdb files    
    for mdb_f in mdb_files:
        print(f'Revisando {mdb_f.name}')
        mdb_abspath= os.path.join(mdb_path, mdb_f.name)
        month = int(mdb_f.name[-6:-4])
        year = int(mdb_f.name[-8:-6]) + 2000
        month_date = datetime.datetime(year=year, month=month, day=1)
        df = create_df_ret(mdb_path=mdb_abspath, month_date=month_date)
        now =  datetime.datetime.now()
        if upload_to_db:
            print(f'Fin lectura de {mdb_f.name}. Escribiendo a BD {now}')
            write_into_db(df=df)
        
        # Pickle file
        foo_str = mdb_f.name.replace('.mdb', '')
        picklezip_file = f'pkl//input//{foo_str}.zip'
        now =  datetime.datetime.now()
        print(f'Escribiendo {picklezip_file} {now}')
        PickleZip.dump(varname=df, zip_file=picklezip_file)
        del df
        gc.collect()
    
    # Restore index on retiros
    o_db.create_index(table='retiros', name='inx',
                      columns=('month', 'day', 'hour', 'type'))
        
    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo total de ejecucion: {t_delta}')

    # return df

# Getting back the objects:
# with open('dict_df.pkl', 'rb') as f:
#     d_df_gen, d_df_bus = pickle.load(f)