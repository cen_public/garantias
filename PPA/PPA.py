from __future__ import annotations
from argparse import ArgumentError
from dataclasses import dataclass, field
from time import sleep, time
from datetime import datetime
from SharedCode.Database.PickleZip import PickleZip
from SharedCode.Dicts import Dict
from SharedCode.exceptions import PowerPurchaseError
from PPA.Rule import Rule
from PPA.Results import Results
import pandas as pd
import gc, os

        

class PPAParties():
    """Class to represent PPA parties involved"""
    def __init__(self, declarant:str, seller:str, buyer:str):
        self.declarant = declarant.upper()
        self.seller = seller.upper()
        self.buyer = buyer.upper()
        self.counterpart = self._identify_counterpart()
    
    def __repr__(self):
        return "PPAParties()"

    def __str__(self):
        return f'declarant:{self.declarant}; buyer:{self.buyer}; seller:{self.seller}; counterpart:{self.counterpart}'
    
    def __eq__(self, other):
        eq = 1
        eq = eq * (self.declarant == other.declarant)
        eq = eq * (self.counterpart == other.counterpart)
        eq = eq * (self.buyer == other.buyer)
        eq = eq * (self.seller == other.seller)
        return True if eq == 1 else False

    def is_mirror_party(self, other:PPAParties):
        if self == other:
            # msg = 'Las partes involucradas son las mismas'
            # raise PowerPurchaseError(msg)
            return False

        if (self.declarant == other.counterpart and self.seller == other.seller
            and self.buyer == other.buyer):
            return True
        else:
            # msg = 'Las partes involucradas no son concordantes entres si'
            # raise PowerPurchaseError(msg)
            return False
    
    def _identify_counterpart(self):
        if self.declarant == self.buyer:
            return self.seller

        if self.declarant == self.seller:
            return self.buyer


@dataclass
class PPA:
    """Class to represent a Power Purchase Agreement"""
    ppa_id:str
    parties:PPAParties
    rule:Rule=None
    busbar:str=None
    results:Results=None
    initial_date:str = field(default=None)
    final_date:str = field(default=None)

    def __post_init__(self):
        def change_format(d, h):
            """ From YYYY-MM-DD to YY-MM-DD- HH:MM:SS """
            if d != None:
                foo = datetime.strptime(d, '%Y-%m-%d')
                foo = foo.replace(hour=h)
                return foo.strftime('%Y-%m-%d %H:%M:%S')

        self.is_fixed = True if self.rule.rule_type == '_fixed_vector_rule' else False
        
        if self.initial_date is None or self.initial_date == 'nan':
            self.initial_date = '2022-01-01'
        if self.final_date is None or self.initial_date == 'nan':
            self.final_date = '2022-12-31'            
        try:
            self.initial_date = change_format(d=self.initial_date, h=0)
            self.final_date = change_format(d=self.final_date, h=23)
        except Exception as e:
            print(e)
            msg = f'{self.ppa_id} no pudo ser creado por problemas en las fechas de inicio y fin'
            raise PowerPurchaseError(msg)
    
    def eval_rule(self, hidro:str, month:int, iny:pd.DataFrame,
                  ret:pd.DataFrame, fix_:pd.DataFrame,
                  df_bus:pd.DataFrame, map_gen_kb:dict,
                  map_ret_bus:dict, map_ivt_plx:dict):
        """ It evals rule declared in PPA's """
        
        self.results = Results()
        self.rule.to_pandas_format()
        fixed_id = None
        key_ = None
        
        if self.rule.rule_type == '_fixed_vector_rule':
            key_, fixed_id = self._get_fixed_params()
            
        # Apply rule
        res = self.rule.eval_rule(iny=iny, ret=ret, fix_=fix_,
                                  df_bus=df_bus, map_gen_kb=map_gen_kb,
                                  map_ret_bus=map_ret_bus,
                                  map_ivt_plx=map_ivt_plx,
                                  fixed_id=fixed_id, key_=key_,
                                  busbar=self.busbar)
        
        # Set results. Then, add more columns
        self.results.new_result(result=res)
        self.results.df['hidrology'] = hidro
        self.results.df['month'] = month
        self.results.df['ppa_id'] = self.ppa_id
        self.results.df['seller'] = self.parties.seller
        self.results.df['buyer'] = self.parties.buyer
    
    def _get_fixed_params(self):
         # set col name in fix_ DataFrame
        foo = self.ppa_id.split('_&_')
        if len(foo) != 5:
            msg = f'{self.ppa_id} no tiene las partes esperadas'
            raise PowerPurchaseError(msg)
        else:
            key_ = foo[4]
            fixed_id = f'{self.parties.declarant}_&_{foo[0]}_&_{foo[1]}'
            return key_, fixed_id

@dataclass
class PPADeclaration:
    """ Class to represent a declaration of a PPA """
    ppa_id:str
    rule:Rule


class PPACollection(Dict):
    """ Dictionary to store PPA objects. PPA's id should be used as key, and their elements should be
        dictionaries with company name as a key and a PPA object as element."""
    def __init__(self):
        # Every element should be {id: list with PPA objects}
        super().__init__()
    
    def add_ppa(self, ppa_id:str, parties:PPAParties, rule:Rule,
                busbar:str, initial_date:str, final_date:str):
        """ Add a new PPA """
        new_ppa = PPA(ppa_id=ppa_id, parties=parties, rule=rule,
                      busbar=busbar, initial_date=initial_date,
                      final_date=final_date)
        if ppa_id in self.keys():
            self[ppa_id].append(new_ppa)
        else:
            self.update({ppa_id: [new_ppa]}) 
            
    def check_integrity(self):
        """ It checks if every object has two ppa's and both are "mirror".
            It returns a list with ppa id to be removed """
        tbr = [] # to be removed
        f = open(file='check_integrity.txt', mode='w')
        for ppa_id, ppa_list in self.items():
            if len(ppa_list) != 2:
                tbr.append(ppa_id)
                line = f'ppa_id: {ppa_id}; cardinalidad igual a {len(ppa_list)}'
                f.write(line + '\n')
            else:
                # Check if parties are "mirror"
                if not ppa_list[0].parties.is_mirror_party(ppa_list[1].parties):
                    tbr.append(ppa_id)
                    line = f'ppa_id: {ppa_id}; parte_0:{ppa_list[0].parties}; parte_1:{ppa_list[1].parties}; no son declaraciones espejo'
                    f.write(line + '\n')

        print(f'len tbr: {len(tbr)}')
        f.close()
        return tbr 

    def remove_ppa(self, ppa_id:str):
        self.pop(ppa_id)
    
    def eval_rules_by_month(self, hidro:str, month:int, iny:pd.DataFrame,
                            ret:pd.DataFrame, fix_:pd.DataFrame,
                            df_bus:pd.DataFrame, map_gen_ck:dict,
                            map_gen_kb:dict, map_ret_bus:dict,
                            map_ivt_plx:dict, output_file:str):
        
        def trim_res(df, initial_date, final_date):
            df2 = df.copy(deep=True)
            if (initial_date != None and final_date != None):
                in_dt = (df2.index >= initial_date) & (df2.index <= final_date)
                df2.loc[~in_dt, 'PxQ'] = 0
                df2.loc[~in_dt, 'value_MWh_1'] = 0
            return df2
        
        print(f'   Evaluando reglas')
        df_month = pd.DataFrame({})
        
        # Do some make_up to iny
        iny = iny[iny['date_time'] < '2022-12-31 23:59:59'].copy(deep=True)
        # iny_cols_index = ['date_time']
        # iny.index = pd.MultiIndex.from_frame(iny[iny_cols_index])
        iny.index = iny['date_time']
        iny.sort_index(inplace=True)
        iny['clave'] = iny['name'].map(map_gen_ck)
        iny_cols = ['month', 'clave', 'busbar', 'value_MWh', 'price_value', 'PxQ'] 
        iny = iny[iny_cols]
                    
        # Do some make_up to ret
        ret = ret[ret['date_time'] < '2022-12-31 23:59:59'].copy(deep=True)
        # ret_cols_index = iny_cols_index
        ret_cols = iny_cols 
        # ret.index = pd.MultiIndex.from_frame(ret[ret_cols_index])
        ret.index = ret['date_time']
        ret.sort_index(inplace=True)
        ret = ret[ret_cols]

        ppa_fixed_vectors = {}
        ppa_non_fixed_vectors = {}
        for id_, ppa in self.items():
            if ppa[0].is_fixed and ppa[1].is_fixed:
                ppa_fixed_vectors.update({id_: [ppa[0], ppa[1]]})
            else:
                ppa_non_fixed_vectors.update({id_: [ppa[0], ppa[1]]})

        count = 1
        # Iter by ppa's
        
        for id_, ppa in ppa_non_fixed_vectors.items():#self.items():
            print(f'  Evaluando {hidro}-{month} {id_}')
            
            try:
                for ppa_i in ppa: # two ppa's (ppa_i is a PPA object)
                    ppa_i.eval_rule(hidro=hidro, month=month,
                                    iny=iny, ret=ret,
                                    fix_=fix_, df_bus=df_bus,
                                    map_gen_kb=map_gen_kb,
                                    map_ret_bus=map_ret_bus,
                                    map_ivt_plx=map_ivt_plx)
              
            except PowerPurchaseError as ppe:
                print(ppe)
                pass
            else:
                # check consistency
                foo = ppa[0].rule.pandas_format == ppa[1].rule.pandas_format
                
                if foo: # if rules are the same form the string point of view, ok
                    ok_consistency = foo
                else: # if not, check if the results are equals
                    bar = ppa[0].results == ppa[1].results
                    ok_consistency = bar
                    pass
                
                # add a column to keep a track on consistency
                # print(f'   consistencia: {ok_consistency} regla: {id_}')    
                df_res = ppa[0].results.df
                if len(df_res) >= 1: #i.e, it is not an empty frame
                    df_res['is_consistent'] = ok_consistency
                    df_res['ppa_id'] = id_
                    df_res['rule_type'] = ppa[0].rule.rule_type
                    first_cols = ['ppa_id', 'PxQ', 'rule_type']
                    df_res = (df_res[first_cols
                    + [col for col in df_res if col not in first_cols]])
                    # Replace with zeros outside initial and final date
                    # if it's consistent
                    if ok_consistency:
                        df_res = trim_res(df=df_res,
                                          initial_date=ppa[0].initial_date,
                                          final_date=ppa[0].final_date)
                        
                        # ver si hay que eleiminar una indentacion
                        df_month = pd.concat([df_month, df_res])
                        print(f'Concatenacion nº {count}')
                        count += 1
                    else:
                        print(f'{id_} no es consistente')
                # Release memory
                del ppa[0].results
                del ppa[1].results
                del df_res
                gc.collect()
        
        ## PROCESAR MASIVAMENTE LOS VECTORES FIJOS
        # drop rows where ppa_id or declarant is None
        fix_ = fix_[(fix_['ppa_id'].notna()) & (fix_['declarant'].notna())] 

        inx_to_remove = []
        g_ppa_id = fix_.groupby('ppa_id')
        for g in g_ppa_id.__iter__():
            declarants = g[1]['declarant'].unique()
            if len(declarants) != 2: 
                inx_to_remove += list(g[1].index.values)
            else:
                g_declarant = g[1].groupby('declarant')
                g_0 = g_declarant.get_group(declarants[0]) 
                g_1 = g_declarant.get_group(declarants[1]) 
                
                sum_0 = g_0['PxQ'].sum()
                sum_1 = g_1['PxQ'].sum()
                if ( (sum_0 != 0 and abs(sum_0 - sum_1)/abs(sum_0) > 0.05)
                     or (sum_0 == 0 and sum_1 != 0)):
                    print(f'{g[0]} sera eliminado por error ')
                    inx_to_remove += list(g_0.index.values)
                    inx_to_remove += list(g_1.index.values)
                else: # data is correct
                    # keep one of the declarant, otherwise, ppa will be stored twice
                    inx_to_remove += list(g_0.index.values)
    
        df_res = fix_.drop(inx_to_remove)

        # Shave values if they are not inside [initial-date, final-date]
        df_res['Initial_Date'].fillna('1981-01-01', inplace=True)
        df_res['Final_Date'].fillna('2100-12-31', inplace=True)
        df_res['Initial_Date'] = pd.to_datetime(df_res['Initial_Date'], format='%Y-%m-%d')
        df_res['Final_Date'] = pd.to_datetime(df_res['Final_Date'], format='%Y-%m-%d')
        in_dt = (df_res['date_time'] >= df_res['Initial_Date']) & (df_res['date_time'] <= df_res['Final_Date'])
        df_res.loc[~in_dt, 'PxQ'] = 0
        df_res.loc[~in_dt, 'value_MWh_1'] = 0

                
        df_res.set_index('date_time', inplace=True)
        df_res.index.rename('date_time', inplace=True) 
        df_res.rename(columns={'busbar': 'busbar_1', 'clave': 'clave_1',
                                    'price_value': 'price_value_1'}, inplace=True)
        cols_reindex = ['clave_1', 'busbar_1', 'price_value_1', 'value_MWh_1',
                        'clave_2', 'busbar_2', 'price_value_2', 'value_MWh_2',
                        'clave_3', 'busbar_3', 'price_value_3', 'value_MWh_3',
                        'clave_4', 'busbar_4', 'price_value_4', 'value_MWh_4']

        cols_reindex += ['PxQ', 'ppa_id', 'seller', 'buyer']
        df_res = df_res.reindex(columns=cols_reindex)
        df_res['is_consistent'] = True
        df_res['hidrology'] = hidro
        df_res['month'] = month
        df_res['rule_type'] = 'fixed_vector_rule'
        first_cols = ['ppa_id', 'PxQ', 'rule_type']
        df_res = (df_res[first_cols
                  + [col for col in df_res if col not in first_cols]])
                        
        df_month = pd.concat([df_month, df_res])
        if 'date_time' in df_month.columns:
            df_month.drop(columns=['date_time'], inplace=True)

        # Write PickleZip file
        print(f'   Escribiendo {output_file}')
        PickleZip.dump(varname=df_month, zip_file=output_file)
