from dataclasses import dataclass, field
from PPA.PPA import PPADeclaration, PPAParties
from SharedCode.Dicts import Dict


@dataclass
class Company:
    """ Class to represent a company with its ppa's"""
    name:str=None
    ppa_declarations:Dict=field(default_factory=Dict)
    ppa_declarations_with_errors:list=field(default_factory=list)

    def add_ppa_declaration(self, ppa_declaration:PPADeclaration):
        """ It adds a ppa declaration """
        ppa_id = ppa_declaration.ppa_id
        if ppa_id in self.ppa_declarations.keys():
            msg = f'Intento de ingresar nuevamente el id {ppa_id}. '
            msg = msg + f'Se eliminan todos los ppa con este id'
            self.ppa_declarations.pop(ppa_id)
            self.ppa_declarations_with_errors.append(ppa_id)
            return False
        else:
            self.ppa_declarations.update({ppa_id: ppa_declaration})
            return True

class CompanyCollection(Dict):
    """ Class to represent a collection of "company" objects """
    def __init__(self):
        super().__init__()
        
    def check_companies_in_parties(self, parties:PPAParties):
        """ Add a new company from the data. Declarant and counterpart
            are visited"""
        possible_new_names = [parties.declarant, parties.counterpart]
        
        for pnm in possible_new_names:
            if pnm not in self.keys():
                self.update({pnm: Company(name=pnm)})

    def remove_ppa(self, ppa_id:str):
        for co in self.values(): # Items are Company objects
            co.ppa_declarations.pop(ppa_id)
            co.ppa_declarations_with_errors.append(ppa_id)



                
        
        

