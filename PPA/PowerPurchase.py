from dataclasses import dataclass
from __future__ import annotations
from SharedCode.exceptions import PowerPurchaseError

@dataclass
class PPA_parties():
    """Class to represent PPA parties involved"""
    declarant:str=None
    counterpart:str=None
    seller:str=None
    buyer:str=None

    def is_mirror_party(self, other:PPA_parties):
        if self == other:
            msg = 'Las partes involucradas son las mismas'
            raise PowerPurchaseError(msg)

        if (self.declarant == other.counterpart and self.seller == other.buyer
            and self.buyer == other.seller):
            pass
        else:
            msg = 'Las partes involucradas no son concordantes entres si'
            raise PowerPurchaseError(msg)
            

class PPA:
    """Class to represent a Power Purchase Agreement"""
    def __init__(self, id:str, parties:PPA_parties, rule:str) -> None:
        self.id = id
        self.parties = parties
        rule = rule
        hourly_vector = None

    def eval_rule():
        pass

class Company:
    """ Class to represent a company with its ppa's"""

    def __init__(self, name:str) -> None:
        self.name = name
        self.ppa_dict = {}
        self.ppa_dict_prevalidated = {}
        self.ppa_dict_witherrors = {}

    def add_ppa(self, ppa:PPA):
        if ppa.id in self.ppa_dict.keys():
            msg = f'Intento de ingresar nuevamente el id {ppa.id}. '
            msg = msg + f'Se eliminan todos los ppa con este id'
            self.ppa_dict.pop(ppa.id)
        else:
            self.ppa_dict.update({ppa.id: ppa})

class CompanyPPACollection:
    def __init__(self) -> None:
        self.collection = {} # dict with Company objects

    def add_ppa(self, parties:PPA_parties, rule:str):
        # 1.- Identify the counterpart
        counterpart_name = self._identify_counterpart(parties=parties)
        
        # 2.- Update collection of companies: Create new ones if it is necessary
        self._update_collection(companies=[parties.declarant,
                                           counterpart_name])
       
        # 3.- Create new ppa. Then, add to company's list of ppa's
        new_ppa = PPA(id=id, parties=parties, rule=rule)
        self.collection[parties.declarant].add_ppa(new_ppa)
    
    def pre_validate_ppa_by_company(self):
        for co in self.collection.values():
            for id, ppa in co.ppa_dict.items(): # iter over o_co's ppa dict.
                try:
                    # declarant must be equal to "company"
                    self._is_declarant_ok(company_name=co.name, ppa=ppa)

                    # counterpart must be included in collection
                    counterpart_name = ppa.counterpart # str
                    self._does_counterpart_exist(counterpart_name)
                    
                    # For every ppa, declarant and counterpart both must have a ppa
                    # with same id
                    counterpart = self.collection[counterpart_name]
                    self._is_id_in_counterpart(id=id, counterpart=counterpart)

                    # parties identified by declarant must be a mirror of those
                    # identified by counterpart in the same ppa (same id)
                    counterpart_parties = counterpart.ppa_dict[id].parties
                    ppa.parties.is_mirror_party(counterpart_parties)

                except PowerPurchaseError as ppe:
                    print(f'{co.name}: {ppe}')
                    co.ppa_dict_witherrors.update({ppa.id: ppe})
                else:
                    co.ppa_dic_prevalidated.update({ppa.id: ppa})
    
    def _is_declarant_ok(self, company_name, ppa):
        declarant_name = ppa.parties.declarant
        if company_name != declarant_name:
            msg = 'problemas en el nombre del declarante'
            raise PowerPurchaseError(msg)
            
    def _does_counterpart_exist(self, counterpart_name:str):
        if counterpart_name not in self.collection.keys():
            msg = f'contraparte {counterpart_name} no encontrada'
            raise PowerPurchaseError(msg)

    def _is_id_in_counterpart(self, id:str, counterpart:Company):
        if id not in counterpart.ppa_dict.keys():
            msg = f'contraparte no contiene un ppa con id {id}'
            raise PowerPurchaseError(msg)

    def _update_collection(self, companies:list):
        for company in companies:
            foo = {company: Company(company)
                   if company not in self.collection.keys else {}}
            self.collection.update(foo)
    
    def _identify_counterpart(parties:PPA_parties):
        counterpart = None
        if parties.counterpart is not None:
            counterpart = (parties.seller
                           if parties.declarant == parties.buyer else None)

            counterpart = (parties.buyer
                           if parties.declarant == parties.seller else None)
        parties.counterpart = counterpart