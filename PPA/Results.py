from dataclasses import dataclass
import pandas as pd

@dataclass
class Results:
    df:pd.DataFrame=pd.DataFrame({})

    def new_result(self, result):
        if isinstance(result, pd.DataFrame):
            # self._check_results(result=result)
            self.df = result
        else:
            print(result)
            msg = 'result no es pandas.DataFrame'
            raise TypeError(msg)
   
    def __eq__(self, other):
        try:
            num_ = self.df['PxQ'].sum()
            den_ = other.df['PxQ'].sum()
        except KeyError: # assume not_equal IF PxQ is not a valid key
            return False
        else:
            tol = 0.05
            if ((1 - tol) * abs(den_) <= abs(num_)
                and abs(num_) <= (1 + tol) * abs(den_)):
                return True
            else:
                return False