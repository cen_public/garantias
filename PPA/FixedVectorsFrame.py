import pandas as pd
import datetime
from SharedCode.Database.PickleZip import PickleZip
from SharedCode.exceptions import PowerPurchaseError
from dateutil.relativedelta import relativedelta
import os, gc

class FixedVectorsFrame:
    """ Class to represent and manipulate a frame with fixed vectors data """

    def __init__(self, fixed_path:str, zip_path:str, df_form:pd.DataFrame,
                 plx_busbars:dict, dict_bus:dict, year=int):
        self.fixed_path = fixed_path
        self.zip_path = zip_path
        self.df_form = df_form # Frame with all PPA's
        self.plx_busbars = plx_busbars # busbars and Marg. Cost by hidrology
        self.dict_bus = dict_bus # busbar_ivt : busbar_plexos 
        self.year = year

        self._check_df_form_index_has_duplicates()
    
    def set_df_from_files(self, hidrologies:list):
        def set_fixed_frame():
            # xlsx files iterator
            iter_xlsx_files = os.scandir(self.fixed_path)
            xlsx_files = (entry for entry in iter_xlsx_files
                          if entry.is_file() and entry.name[-5:] == '.xlsx'
                          and entry.name[0] != '~')

            df_fv = pd.DataFrame({})
            
            # Iter over xlsx files    
            for xlsx_f in xlsx_files:
                print(f'Revisando {xlsx_f.name}')
                df = self._set_df_from_file(file=xlsx_f)  # CEN form
                df_fv = pd.concat([df_fv, df], axis=0, ignore_index=True)
            
            # map busbar column. Then, identify those columns with issues
            df_fv['busbar'] = df_fv['busbar_ivt'].map(self.dict_bus)
            df_fv.drop(columns='busbar_ivt', inplace=True)
            return df_fv
        
        def prepare_df_bus_by_hidro(hidrology):
            # Prepare df_bus
            df_bus = self.plx_busbars[hidrology] # df with busbars and their Marg. Cost
            df_bus.rename(columns={'name': 'busbar'}, inplace=True)
            index_keys = ['date_time', 'busbar']
            df_bus.set_index(keys=index_keys, drop=True, inplace=True)
            df_bus.sort_index(inplace=True)
            return df_bus

        def create_monthly_temporary_files(df):
            """split df in smaller frames (by month) and also join those frames with df_bus """
            for m in range(1, 13):
                # Filtering by month
                start = datetime.datetime(year=self.year, month=m, day=1)
                end = start + relativedelta(months=1) - relativedelta(hours=1)
                dt = pd.date_range(start=start, end=end, freq='1H')
                df_m = df[df['date_time'].isin(dt)].copy(deep=True)

                # Dump to zip file
                mm = "{:02d}".format(m)
                zip_file = f'fixed_vectors_{mm}.zip'
                zip_file = os.path.join(self.zip_path, zip_file)
                print(f'   Escribiendo {zip_file}')
                PickleZip.dump(varname=df_m, zip_file=zip_file)
        
        def add_price_value_to_monthly_frames(df_bus_h, h):
            for m in range(1, 13):
                # Read to zip file
                mm = "{:02d}".format(m)
                zip_file = f'fixed_vectors_{mm}.zip'
                zip_file = os.path.join(self.zip_path, zip_file)
                print(f'   Leyendo {zip_file}')
                df_m = PickleZip.load(zip_file=zip_file)

                #Join witth df_bus_h and calculate PxQ
                df_m.set_index(keys=['date_time', 'busbar'], drop=True, inplace=True)
                df_m = df_m.join(other=df_bus_h['price_value'], how='left')
                df_m['PxQ'] = df_m['price_value'] * df_m['value_MWh_1']
                df_m.reset_index(drop=False, inplace=True)
                
                # Dump to zip file
                zip_file = f'{h}___fixed_vectors_{mm}.zip'
                zip_file = os.path.join(self.zip_path, zip_file)
                print(f'   Re-Escribiendo {zip_file}')
                PickleZip.dump(varname=df_m, zip_file=zip_file)
        
        def delete_temporary_files():
            # zip files iterator
            iter_zip_files = os.scandir(self.zip_path)
            zip_files = (entry for entry in iter_zip_files
                         if entry.is_file() and entry.name[-4:] == '.zip'
                         and entry.name[0].startswith('fixed'))
            for zf in zip_files:
                file_ = os.path.join(self.zip_path, zf.name)
                os.remove(file_)
        
        df = set_fixed_frame() # read forms a create a Frame with all PPA's
        create_monthly_temporary_files(df) # split frame by month
        
        # release some memory
        del df
        gc.collect()

        # Iter by hidrology. Create monthly files with marginal cost by hidrology
        for h in hidrologies:
            df_bus_h = prepare_df_bus_by_hidro(h)
            add_price_value_to_monthly_frames(df_bus_h, h)
        
        delete_temporary_files()

    def _set_df_from_file(self, file:str):
        """ It reads every file with fixed vectors.
            It returns a frame with columns as follow:
            - fixed_id, value_MWh_1 , and a date_time index"""
        def df_from_file():
            df = pd.DataFrame({})
            try:
                df = pd.read_excel(io=file, sheet_name='Compraventas', skiprows=1)
                print(f'   Fin lectura')
            except Exception as e:
                print(f'{file} no pude ser leido, {e}')
            return df
        
        def set_frame(df):
            # concat a fixed_id col. Then, set it as index and drop some columns
            fixed_id_cols = ['Empresa', '[ID Plataforma Contratos]', 'Id_Contrato_FIFC']
            df['fixed_id2'] = df[fixed_id_cols].astype(str).agg('_&_'.join, axis=1)
            df.set_index(keys='fixed_id2', drop=True, inplace=True)
            df.drop(columns=['Transacción'] + fixed_id_cols, inplace=True)
        
            # Melt df to get a "vertical" frame with MWh values
            df = pd.melt(df, value_vars=df.columns, value_name='value_MWh_1',
                         ignore_index=False)
            # remove any weird value in "variable" such as Unnamed
            bool_unnamed = df['variable'].astype(str).str.contains('Unnamed')
            df = df.loc[~bool_unnamed]
            df.rename(columns={'variable': 'date_time'}, inplace=True)
            df.astype({'date_time': 'datetime64'})
            df['value_MWh_1'] = df['value_MWh_1'] / 1000
            # future version: check if df.index has all the hours

            # add columns new columns from df_form
            new_cols = ['declarant', 'clave', 'busbar_ivt', 'ppa_id',
                        'seller', 'buyer', 'Initial_Date', 'Final_Date']
            df = df.join(other=self.df_form[new_cols], how='left')
            
            # create a new column "fixed_id"
            df.reset_index(drop=False, inplace=True)
            df.rename(columns={'index': 'fixed_id'}, inplace=True)
            return df

        df = df_from_file()
        df = set_frame(df)
        return df

    def _check_df_form_index_has_duplicates(self):
        idx = self.df_form.index
        if idx.has_duplicates:
            raise ValueError('df_form contiene indices duplicados')

