from dateutil.relativedelta import relativedelta
import datetime, re, copy
from SharedCode.exceptions import PowerPurchaseError
import pandas as pd
import numpy as np # it's necessary to eval min_max_rules
import time

d_ncols = {'clave_1': [], 'busbar_1': [], 'price_value_1': [], 'value_MWh_1': [],
           'clave_2': [], 'busbar_2': [], 'price_value_2': [], 'value_MWh_2': [],
           'clave_3': [], 'busbar_3': [], 'price_value_3': [], 'value_MWh_3': [],
           'clave_4': [], 'busbar_4': [], 'price_value_4': [], 'value_MWh_4': []}

remove_ = ['iny["', 'ret["', '"]', 'max(', 'min(', ')', '(']

class Rule(object):
    """ Class to represent PPA rules. It also includes methods to
        write rule in Pandas format and get values from monthly dataframes """
    def __init__(self, original_format:str=None, year:int=2022,
                 month:int=None, key_column:str='clave', ppa_id:str=None):

        self.original_format = original_format
        self.original_format2 = original_format # it is useful to monthly factors
        self.year = year
        self.month = month
        self.key_column = key_column
        self.ppa_id = ppa_id
        self.pandas_format = None
        self.rule_type = None
       
    def  __str__(self):
         return f'original_format:{self.original_format}'
    
    def __repr__(self):
        return f'Rule(): "{self.original_format}"'

    def eval_rule(self, iny:pd.DataFrame, ret:pd.DataFrame, fix_:pd.DataFrame,
                  df_bus:pd.DataFrame, map_gen_kb:dict, map_ret_bus:dict,
                  map_ivt_plx:dict, fixed_id:str, key_:str, busbar:str):
        
        def eval_standard_minmax(iny, ret):
            res = pd.DataFrame(d_ncols)
            n_keys = len(self.pandas_format[0])
            d_mwh = {}
            d_price = {}
            d_pxq = {}
         
            for i in range(0, n_keys):
                res[f'busbar_{i+1}'] = eval(self.pandas_format[0][i][1])
                res[f'price_value_{i+1}'] = eval(self.pandas_format[0][i][2])
                res[f'value_MWh_{i+1}'] = eval(self.pandas_format[0][i][3])
                res[f'clave_{i+1}'] = self.pandas_format[0][i][0]
                d_price.update({i: eval(self.pandas_format[0][i][2])})
                d_mwh.update({i: eval(self.pandas_format[0][i][3])})
                d_pxq.update({i: eval(self.pandas_format[0][i][4])})
            
            res['PxQ'] = eval(self.pandas_format[1])
            
            return res
        
        def eval_fixed_vector(fix_:pd.DataFrame, df_bus:pd.DataFrame,
                              map_gen_kb:dict, map_ret_bus:dict,
                              fixed_id:str, busbar:str):
            res = pd.DataFrame(d_ncols)
            
            fix_filt = fix_[fix_['fixed_id'] == fixed_id].copy(deep=True)

            if len(fix_filt) > 0:
                res['value_MWh_1'] = fix_filt['value_MWh_1']
                res['date_time'] = fix_filt['date_time']
            
                # Map busbar (IVT) with plexos busbar
                try:
                    res['name'] = map_ivt_plx[busbar]
                except KeyError:
                    msg = f'{busbar} no encotrada en ningun diccionario'
                    raise PowerPurchaseError(msg)
                    
                # add price_value (fix_ doesn't have index, so res don't)
                index_cols = ['date_time', 'name']
                res.index = pd.MultiIndex.from_frame(res[index_cols])

                # join
                res = res.join(other=df_bus, how='left') # df.index=['date_time', 'name']
                res.index = res.index.droplevel('name') # df.index=['date_time']
                res['busbar_1'] = res['name']
                res['price_value_1'] = res['price_value']
                            
                # PxQ
                res['PxQ'] = res['price_value_1'] * res['value_MWh_1']

                # Final make up
                cols_to_drop = ['price_value', 'name', 'date_time']
                res.drop(columns=cols_to_drop, inplace=True)
            return res

        res = None
        # Eval every element of the script to form the rule, depending on function type
        if self.rule_type == '_standard_rule':
            res = eval_standard_minmax(iny, ret)
        elif self.rule_type == '_min_max_rule':
            res = eval_standard_minmax(iny, ret)
        elif self.rule_type == '_monthly_factors_rule':
            res = eval_standard_minmax(iny, ret)
        elif self.rule_type == '_fixed_vector_rule':
            res = eval_fixed_vector(fix_=fix_, df_bus=df_bus,
                                    map_gen_kb=map_gen_kb, 
                                    map_ret_bus=map_ret_bus,
                                    fixed_id=fixed_id,
                                    busbar=busbar)

        # print(f'regla: {self.original_format}')
        # print(self.pandas_format)
        # print(res)
        return res
        
    def to_pandas_format(self):
        # detect type of rule
        self._identify_rule()
        
        # Transform into Pandas format. Remove blanks
        func = self._transformation_function()
        rule_list = func()
        # rule_str = rule_str.replace(' ', '')
        self.pandas_format = rule_list

    def _identify_rule(self):
        rof = self.original_format.lower() # rule in original format lower case
        if rof.find('vector') >= 0:
            self.rule_type = '_fixed_vector_rule'
        elif rof.find('factores') >= 0 and rof.find('clave') >= 0:
            self.rule_type = '_monthly_factors_rule'
        elif rof.find('min') >= 0 or rof.find('max') >= 0:
            self.rule_type = '_min_max_rule'
        else:
            self.rule_type = '_standard_rule'
        
    def _transformation_function(self):
        return getattr(self, self.rule_type)

    def _fixed_vector_rule(self):
        # Los vectores fijos vienen en kWh
        return self.original_format

    def _standard_rule(self):
        def mod_minmax(text:str):
            if text[0 : 3].lower() == 'min':
                return text.replace(text[0:3], 'np.minimum')
            elif text[0 : 3].lower() == 'max':
                return text.replace(text[0:3], 'np.maximum')
            else:
                msg = f'tipo de regla minimo-maximo no valida: {text}'
                raise PowerPurchaseError(msg)

        rule_splitted = self._split_rule()
        rule_splitted_mod = copy.deepcopy(rule_splitted)

        # Identify type of content in every substring. Then, create a list
        # with a list for each key with intermediate steps to get: busbar,
        # price_value and value_MWh
        inter_steps = []
        key_count = 0
        for k, v in rule_splitted_mod.items():
            foo = []
            if v.find('iny["') >= 0 or v.find('ret["') >= 0:
                # get clave
                clave = self._remove_substr(text=v)
                foo.append(clave)
                bar = [self._set_iny_ret(text=v, cols_to_return='"busbar"'),
                       self._set_iny_ret(text=v, cols_to_return='"price_value"'),
                       self._set_iny_ret(text=v, cols_to_return='"value_MWh"')]
                foo.extend([self._remove_substr(b, ['min(', 'max(']) for b in bar])
                foo.append(f'{foo[2]}*{foo[3]}') # PxQ
                
                # add foo list to inter_steps.
                inter_steps.append(foo)

                # Update rule_splitted mod:
                # For standard rule:
                #    Call a d_pxq dict with PxQ associated to each key
                # For min_max rule:
                #    Call d_mwh dict with value_MWh associated to each key.
                rule_splitted_mod.update({k: f'd_pxq[{key_count}]'})
                if self.rule_type == '_min_max_rule':
                     rule_splitted_mod.update({k: f'd_mwh[{key_count}]'})
                key_count += 1
        
        # Modify the original rule
        f_rule = self.original_format2
        for k, v in rule_splitted_mod.items():
            rem_ = ['max(', 'min(', ')', '(']
            rs_k_mod = self._remove_substr(text=rule_splitted[k],
                                           list_to_remove=rem_) 
            v_mod = self._remove_substr(text=v,
                                        list_to_remove=rem_)
            f_rule = f_rule.replace(rs_k_mod, v_mod)
            
        # If it is called from eval_min_max:
        # - replace min and max by np.xxx
        # - add mean value of marginal costs
        if self.rule_type == '_min_max_rule':
            f_rule = mod_minmax(f_rule)
            price_sum = ''
            for i in range(0, key_count):
                price_sum += f'd_price[{i}]+'
            price_sum = price_sum[0:-1]
            f_rule = f'{f_rule}*({price_sum}).mean()'
        
        foo = True
        if foo:
            return [inter_steps, f_rule]
        else:
            msg = 'regla no pudo ser validada'
            raise PowerPurchaseError(msg)   
    
    def _min_max_rule(self):
        return self._standard_rule()
    
    def _monthly_factors_rule(self):
        def preliminary_changes():
             # Preliminary changes
            text = self.original_format2
            replace_dict = {'iny["': '\'iny["',
                            'ret["': '\'ret["',
                            '"],': '"]\',',
                            '"], ': '"]\','}
            for k, v in replace_dict.items():
                text = text.replace(k, v)
            
            return text

        def repair_factors(factors:list):
            if len(factors) == 1:
                text = factors[0]
                text = text.replace('"', '')
                text = text.replace('\'','')
                text = f'[{text}]' # list as string
                new_factors = eval(text)
                return new_factors
            else:
                return factors

        def check_factors_and_month(factors:list):
            # Check length
            if len(factors) != 12:
                msg= f'{factors} es de longitud {len(factors)}'
                raise PowerPurchaseError(msg)
            
            # Check type of elements
            for f in factors:
                if not (isinstance(f, int) or isinstance(f, float)):
                    msg = f'{factors} contiene elementos no numericos'
                    raise PowerPurchaseError(msg)
            
            # Check month
            if not isinstance(self.month, int):
                msg = f'month no ha sido especificado'
                raise PowerPurchaseError(msg)

        # check if rule has a dict structure
        foo = preliminary_changes()
        print(foo)
        rule_dict = eval(foo)
        
        if not isinstance(rule_dict, dict):
            msg = f'Regla no esta en formato correcto: {self.original_format2}'
            raise PowerPurchaseError(msg)
        
        # Repair factors.
        factors = repair_factors(factors=rule_dict['factores'])
        check_factors_and_month(factors=factors)

        # Change self.original_format2. Then, call standar_rule
        iny_ret = rule_dict['clave']
        ff = factors[self.month]
        self.original_format2 = f'{ff}*{iny_ret}'
        return self._standard_rule()

    def _set_month_datetime(self, month:int):
        start = datetime.datetime(year=self.year, month=month, day=1)
        end = start + relativedelta(months=1) - relativedelta(hours=1)
        start = start.strftime('%Y-%m-%d %H:%M:%S')
        end = end.strftime('%Y-%m-%d %H:%M:%S')
        dt = pd.DatetimeIndex(start=start, end=end, freq='1H')
        return dt.values

    def _set_iny_ret(self, text:str, cols_to_return:str):
        # get type_
        if text.find('iny') >= 0:
            type_ = 'iny'
        elif text.find('ret') >= 0:
            type_ = 'ret'
                       
        key_start = text.find('["') + 2
        key_end = text.find('"]')
                
        if key_start > key_end or key_start == -1 or key_end == -1:
            msg = f'regla en formato no valido: {text}'
            raise PowerPurchaseError(msg)

        key_ = text[key_start : key_end]
        substr_replace = f'{type_}["{key_}"]' # Text to be replaced

        foo = (f'{type_}[{type_}["{self.key_column}"] == "{key_}"]'
               f'[{cols_to_return}]')
        text = text.replace(')', '')
        return text.replace(substr_replace, foo)
    
    def _split_rule(self):
        rule_splitted = re.split('[*+/,-]', self.original_format2)
        rule_splitted = {i: substr_i
                         for i, substr_i in enumerate(rule_splitted)}
        
        return rule_splitted

    def _remove_substr(self, text:str, list_to_remove:list=remove_):
        for bar in list_to_remove:
            text = text.replace(bar, '')
        return text  
