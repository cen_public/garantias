import logging

# from events import Events
from PPA.PPA import PPACollection, PPADeclaration, PPAParties
from PPA.Company import CompanyCollection
from PPA.Rule import Rule
from SharedCode.exceptions import PowerPurchaseError
from SharedCode.Database.PickleZip import PickleZip
import pandas as pd
import os, time, gc


class PPASimulation():
    
    def __init__(self, df_ppa:pd.DataFrame, plx_busbars:dict, dict_gen_ck:dict,
                 dict_gen_kb:dict, dict_ret_bus:dict, dict_ivt_plx:dict):
        self.ppa_with_observations = []
        self.df_ppa = df_ppa
        self.plx_busbars = plx_busbars
        self.ppa_collect = PPACollection()
        self.co_collect = CompanyCollection()
        self.map_gen_ck = dict_gen_ck
        self.map_gen_kb = dict_gen_kb
        self.map_ret_bus = dict_ret_bus
        self.map_ivt_plx = dict_ivt_plx

        
    def process_forms(self):
        # Porcess each row in self.df_ppa
        for i, row in self.df_ppa.iterrows():
            self._process_row(row=row)
        
        print(f'len PPACollection: {len(self.ppa_collect)}')
        # remove the ppa with observations from PPA and Company collections
        self._remove_ppa_list(ppa_list=self.ppa_with_observations)

        # Check cardinality of PPA Collection objects. Then, remove those with
        # problems from PPA and Company Collections
        ppa_with_problems = self.ppa_collect.check_integrity()
        self.ppa_with_observations += (pwp for pwp in ppa_with_problems)
        self._remove_ppa_list(ppa_list=ppa_with_problems)
        print(f'len PPACollection: {len(self.ppa_collect)}')

    def simulate_ppa_collection(self, ret_path:str, iny_path:str,
                                fix_path:str, out_path:str, hidrologies:list):
        def load_picklezip(h, suffix, mm, f_path):
            f_name = f'{h}{suffix}{mm}.zip'
            f_file = os.path.join(f_path, f_name)
            print(f'Iterando por {f_file}')
            return PickleZip.load(f_file)
            
        # hidrologies = get_hidrologies(ret_path, iny_path)
        # print(f'hidrologias a analizar: {hidrologies}')
                
        for h in hidrologies:
            # Load data from generators. Files are divided by hidrology only
            df_g = load_picklezip(h, '___generators', '', iny_path)

            # Prepare df_bus
            df_bus = self.plx_busbars[h] # df with busbars and their Marg. Cost
            index_cols = ['name', 'date_time'] 
            df_bus.index = pd.MultiIndex.from_frame(df_bus[index_cols])
            df_bus.drop(columns=index_cols + ['hidrology', 'unit'], inplace=True)
            df_bus.sort_index(inplace=True)
            print(f'Creado df_bus para {h}')
                       
            # Load data from loads. Files are divided by hidrology and month
            for m in range(1, 13):
                mm = "{:02d}".format(m)
                df_r = load_picklezip(h, '___RETIROS_22', mm, ret_path)
                # df_f = load_picklezip('', 'fixed_vectors_', mm, fix_path)
                df_f = load_picklezip(h, '___fixed_vectors_', mm, fix_path)
                # print(f'Mes {mm}')
                # print(df_r)
                # print(df_f)
                
                # Filter df_g by month.
                df_iny = df_g[df_g['month'] == mm].copy(deep=True)
                
                # output_file
                output_name = f'{h}___ppa_eval_{mm}.zip'
                output_file = os.path.join(out_path, output_name)

                # Eval ppa by hidro-month
                self.ppa_collect.eval_rules_by_month(hidro=h, month=m,
                                                     iny=df_iny, ret=df_r,
                                                     fix_=df_f,
                                                     df_bus=df_bus,
                                                     map_gen_ck=self.map_gen_ck,
                                                     map_gen_kb=self.map_gen_kb,
                                                     map_ret_bus=self.map_ret_bus,
                                                     map_ivt_plx=self.map_ivt_plx,
                                                     output_file=output_file)
        
        # Release some memory
        del df_r
        del df_f
        del df_g
        del df_bus
        gc.collect()

    def secure_ppa_consistency_between_parties(self):
        """ It check consistency of result between parties of a PPA.
            It also removes those unconsistent PPA's from collections """
        ok_, not_ok_ = self.ppa_collect.check_consitency_between_parties()
        self.ppa_with_observations.append(not_ok_)
        self._remove_ppa_list(ppa_list=not_ok_)

    def _process_row(self, row):
        # Split/get info from row
        ppa_id = row['ppa_id']
        declarant = row['declarant']
        buyer = row['buyer']
        seller = row['seller']
        rule = row['rule']
        busbar = row['busbar_ivt']
        initial_date = str(row['Initial_Date'])
        final_date = str(row['Final_Date'])

        try:
            # Create a PPAParties object
            ppa_parties = PPAParties(declarant=declarant,
                                     seller=seller,
                                     buyer=buyer)
            
            # Add new companies to company collection, in case the companies
            # included in PPAParties object are not found in company
            # collection
            self.co_collect.check_companies_in_parties(parties=ppa_parties)

            # Add a PPA declaration to the declarant company in company
            # collection. If declaration is succesfully added, return True
            rule_obj = Rule(original_format=rule, ppa_id=ppa_id)
            ppa_declaration = PPADeclaration(ppa_id=ppa_id, rule=rule_obj)
            res = self._add_ppa_declaration_to(company=ppa_parties.declarant,
                                               ppa_declaration=ppa_declaration)
                       
            # Add a new element to PPA Collection, if ppa_declaration was
            # succesfully added to the declarant company object. Otherwise,
            # add the ppa_id to PPA with observations list
            if res:
                print(f'Adding {ppa_id}')
                self.ppa_collect.add_ppa(ppa_id=ppa_id, parties=ppa_parties,
                                         rule=rule_obj, busbar=busbar,
                                         initial_date=initial_date,
                                         final_date=final_date)
            else:
                print(f'Removing {ppa_id}')
                self.ppa_with_observations.append(ppa_id)

        except PowerPurchaseError as ppe:
            print(ppe)
    
    def _add_ppa_declaration_to(self, company:str,
                                ppa_declaration:PPADeclaration):
        try:
            co = self.co_collect[company] 
            res = co.add_ppa_declaration(ppa_declaration)
        except KeyError:
            msg = f'{company} no encontrado en company collection'
            raise PowerPurchaseError(msg)
        else:
            return res
    
    def _remove_ppa_list(self, ppa_list:list):
        # print(f'remove_ppa_list: removing {ppa_list}')
        time.sleep(3)
        for ppa_id in ppa_list:
            self.co_collect.remove_ppa(ppa_id=ppa_id)
            self.ppa_collect.remove_ppa(ppa_id=ppa_id)
    
    def _set_map_key_bus(self):
        """ It re-arranges and combine two dictionaries to create
            a new one to link keys (claves) and Plexos busbars """
