import flask
import os
from configobj import ConfigObj
from Renova.RenovaCSV import RenovaCSVCreation as RC
from Renova.RenovaCSV import CreationError

app = flask.Flask(__name__)
app.config['SECRET_KEY']='clavesupersecreta'

PREFIX_PATH = ConfigObj('config.cfg')['RENOVA']['DATA']['path_base']
PATHS = ConfigObj('config.cfg')['RENOVA']['PATHS_BASE']
ORIGIN = ConfigObj('config.cfg')['RENOVA']['VALORIZADO']['origin']
PATHS = {k: os.path.join(PREFIX_PATH, v) for k, v in PATHS.items()}

PROCESSING_CSV = 'not busy'
DATE = ''
MSJ = []
INPUTS = False
SHOW_RESULT = False

@app.route('/', methods=['GET'])
def main():
    return flask.redirect(flask.url_for('renova'))



# @app.route('/', methods=['GET', 'POST'])
@app.route('/renova', methods=['GET', 'POST'])
# def main():
def renova():
    FILES = {'BERNC_LIST': os.listdir(PATHS['ernc_path']),
            'VALORIZADO_LIST': os.listdir(PATHS['val_path']),
            'DICCIONARIO_GEN_LIST': os.listdir(PATHS['dict_gen_path']),
            'DICCIONARIO_CLIENT_LIST': os.listdir(PATHS['dict_client_path']),
            'DICCIONARIO_CENTRALES_LIST': os.listdir(PATHS['dict_central_path']),
            'DECIMALES_LIST': os.listdir(PATHS['dec_path'])}
    
    if ORIGIN == 'CSV':
        FILES['VALORIZADO_LIST'] = [f for f in FILES['VALORIZADO_LIST'] if f.endswith('.csv')]
    elif ORIGIN == 'MDB':
        FILES['VALORIZADO_LIST'] = [f for f in FILES['VALORIZADO_LIST'] if f.endswith('.accdb')]

    global INPUTS
    global SHOW_RESULT
    INPUTS = False
    SHOW_RESULT = False
    if PROCESSING_CSV == 'not busy':
        if flask.request.method == 'POST':
            form = flask.request.form
            flask.session['fecha'] = form['fecha']
            flask.session['path_bernc'] = os.path.join(PATHS['ernc_path'], form['bernc path'])
            flask.session['path_val'] = os.path.join(PATHS['val_path'], form['val path'])
            flask.session['path_gen_dicc'] = os.path.join(PATHS['dict_gen_path'], form['dicc gen path'])
            flask.session['path_client_dicc'] = os.path.join(PATHS['dict_client_path'], form['dicc client path'])
            flask.session['dict_central_path'] = os.path.join(PATHS['dict_central_path'], form['dicc central path'])
            flask.session['path_dec_last_month'] = os.path.join(PATHS['dec_path'], form['dec last month path'])
            INPUTS = True
            createCSV()
            return flask.redirect(flask.url_for('result'))
        # return flask.render_template('./main.html', processing_CSV = PROCESSING_CSV, FILES=FILES)
        return flask.render_template('./renova.html', processing_CSV = PROCESSING_CSV, FILES=FILES)
    else:
        SHOW_RESULT = True
        return flask.redirect(flask.url_for('result'))


def createCSV():
    global  PROCESSING_CSV
    global DATE
    global MSJ
    DATE = flask.session['fecha']
    PROCESSING_CSV = 'busy'
    try:
        h = RC(cfg=os.path.join('config.cfg'),
                    val_path=flask.session['path_val'],
                    ernc_path=flask.session['path_bernc'],
                    dict_gen_path=flask.session['path_gen_dicc'],
                    dict_client_path=flask.session['path_client_dicc'],
                    dict_central_path=flask.session['dict_central_path'],
                    dec_path=flask.session['path_dec_last_month'],
                    date=flask.session['fecha'])
        h.run_csv_creation()
        MSJ = ['Se han generado los archivos para el periodo']
        PROCESSING_CSV = 'not busy'
    except CreationError as e:
        print(e)
        MSJ = str(e).split('_&_')
        PROCESSING_CSV = 'not busy'

    # h = RC(cfg='D:/frontera-proyectos/P3 Coordinador Renova/Proyecto real/cen_garantias_renova/config.cfg',
    #                 mdb_path=flask.session['path_accdb'],
    #                 ernc_path=flask.session['path_bernc'],
    #                 dict_gen_path=flask.session['path_gen_dicc'],
    #                 dict_client_path=flask.session['path_client_dicc'],
    #                 dict_central_path=flask.session['dict_central_path'],
    #                 dec_path=flask.session['path_dec_last_month'],
    #                 date=flask.session['fecha'])
    # h.run_csv_creation()
    # MSJ = ['Se han generado los archivos para el periodo']
    # PROCESSING_CSV = 'not busy'

    return None

@app.route('/result', methods=['GET'])
def result():
    if SHOW_RESULT:
        return flask.render_template('./result.html', 
                                    fecha=DATE,
                                    mensaje=MSJ, 
                                    processing_CSV = PROCESSING_CSV)
    else:
        return flask.redirect(flask.url_for('renova'))
        # return flask.redirect(flask.url_for('main'))