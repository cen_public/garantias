from operator import index
import datetime, gc
import pandas as pd
from configobj import ConfigObj
from SharedCode.Database.PickleZip import PickleZip
from SharedCode.LoadEnv import load_env
from dateutil.relativedelta import relativedelta


class Dict(dict):
    def __missing__(self, key):
        return key

def print_msg(text:str):
    now = datetime.datetime.now()
    print(f'{text} {now}')

def set_df_sum(df:pd.DataFrame):
    df_sum = df.groupby(df.index).sum()
    inx = df_sum.index
    inx_multiindex = pd.MultiIndex.from_tuples(inx)
    inx_multiindex.names = ['month', 'day', 'hour', 'type']
    df_sum.index = inx_multiindex
    return df_sum

def set_df_lod_esc_extended(df:pd.DataFrame, year:int):
    # Create a df_dt with hours, using df_lod_esc['date_time'] as index
    df_dt = pd.DataFrame({})
    df_dt.index = df['date_time']
    df_dt = df_dt.loc[df_dt.index.repeat(24 * 7 )]
    
    n_rows = len(df_dt)
    row = df.loc[0]
    start_dt = datetime.datetime(year=row['YEAR'],
                                 month=row['MONTH'],
                                 day=row['DAY'])
    df_dt['date_time'] = pd.date_range(start=start_dt,
                                       periods=n_rows,
                                       freq='1H')
   
    # add index to df
    df.index = df['date_time']
    df.drop(columns=['YEAR', 'MONTH', 'DAY', 'date_time'], inplace=True)

    # Join and trim
    df_dt = df_dt.join(other=df, how='left')
    df_dt = df_dt[df_dt['date_time'] <= '2023-12-31 23:00:00']
    df_dt.index = df_dt['date_time']
    df_dt.drop(columns=['date_time'], inplace=True)

    return df_dt

def set_df_avg(df:pd.DataFrame, columns:list):
    if len(df) > 0:
        df1 = df.copy(deep=True)
        df1.reset_index(inplace=True)
        df_avg = df1.groupby(columns).mean()
        df_avg.drop(columns=['clave_ano_mes', 'hora_mensual'], inplace=True)
        df_avg['avg_MWh'] = -1E-03 * df_avg['medida_kWh']
        df_avg.drop(columns=['medida_kWh'], inplace=True)
        df_avg.sort_index(inplace=True)
    else:
        foo = pd.DataFrame({'day': [], 'hour': [], 'month': []})
        df_avg = pd.DataFrame({'suministrador_clave': [], 'avg_MWh': []})
        df_avg.index = pd.MultiIndex.from_frame(foo)
    return df_avg

def calculate_new_profiles(pkl_files:list, df_lod_lod_sum:pd.DataFrame,
                           df_lod_esc:pd.DataFrame, year:int, cfg): 
    def apply_lod_lod(df:pd.DataFrame):
        # Calculate Plexos factor
        print_msg(f'   Aplicando lod_lod')
        df_sum = set_df_sum(df=df)
        df_sum.drop(columns=['f1'], inplace=True)
        df_f = df.join(other=df_lod_lod_sum, how='left')  # new column: VALUE
        df_f = df_f.join(other=df_sum, how='left',
                         rsuffix='_r' )    # new column: avg_MWh_r
        f_plexos = df_f['VALUE'] / df_f['avg_MWh_r'] # It's a pd.Series
        
        
        df_f['f2'] = f_plexos
        df_f['avg_MWh'] = df_f['avg_MWh'] * df_f['f2']
        df_f.drop(columns=['VALUE', 'avg_MWh_r'], inplace=True)
        return df_f

    def df_to_time(df:pd.DataFrame):
        """ Antitransform into time domain """
        print_msg(f'   Creando perfil en el dominio del tiempo')
        # Set date_time range
        month = int(df.index[0][0])
        start_dt = datetime.datetime(year=year, month=month, day=1)
        end_dt = start_dt + relativedelta(months=1) - relativedelta(hours=1)
        date_time = pd.date_range(start=start_dt, end=end_dt, freq='1H')

        # Create a DataFrame df_new to store values per hour
        # Dimension: len(date_time) * len(df['claves'].unique())
        values_keys = df['suministrador_clave'].unique()
        df_time = pd.DataFrame({'suministrador_clave': values_keys})
        df_time = df_time.loc[df_time.index.repeat(len(date_time))]
        df_time.reset_index(drop=True, inplace=True)
        df_time['date_time'] = len(values_keys) * list(date_time)

        map_day = Dict()
        map_hour = Dict()
        map_day.update(cfg['GARANTIAS']['PLEXOS_DATE']['DAYS'])
        map_hour.update(cfg['GARANTIAS']['PLEXOS_DATE']['HOURS'])
        foo = df_time['date_time'].dt.strftime('D%w-%m-H%H')
        foo = foo.str.split('-', expand=True)
        foo[0] = foo[0].map(map_day) #day
        foo[2] = foo[2].map(map_hour) #hour
        foo.rename(columns={0:"day", 1:"month", 2:"hour"}, inplace=True)
        foo = foo[['month', 'day', 'hour']]
        df_time = pd.concat([foo, df_time], axis=1)

        # Drop index on df
        df.reset_index(drop=False, inplace=True)
        cols_sorted = ['month', 'day', 'hour', 'suministrador_clave',
                       'type', 'avg_MWh', 'f1', 'f2']
        df = df[cols_sorted]

        # Join. Due to the size of dataframes, group by (month, day, hour)
        print_msg(f'   Realizando el join por grupos')
        list_df = []
        cols_groupby = ['month', 'day', 'hour']
        df_time_groups = df_time.groupby(cols_groupby)
        df_groups = df.groupby(cols_groupby)

        for group in df_time_groups.__iter__():
            g_name = group[0]
            
            cols_time_g = ['suministrador_clave', 'date_time']
            df_time_g = group[1][cols_time_g].copy(deep=True)
            df_time_g.index = df_time_g['suministrador_clave']
            df_time_g.drop(columns=['suministrador_clave'], inplace=True)
            
            cols_g = ['suministrador_clave', 'type', 'avg_MWh', 'f1', 'f2']
            df_g = df_groups.get_group(g_name)[cols_g].copy(deep=True)
            df_g.index = df_g['suministrador_clave']
            df_g.drop(columns=['suministrador_clave'], inplace=True)
            list_df.append(df_time_g.join(other=df_g, how='left'))
            
        # Release some memory
        del df_time_g
        del df_g
        del df_time_groups
        del df_groups
        gc.collect()

        print_msg(f'   Concatenando lista de dataframes')
        df_ready = pd.concat(list_df)
        df_ready.reset_index(drop=False, inplace=True)
        return df_ready
    
    def apply_load_escalate(df:pd.DataFrame):
        print_msg(f'   Creando perfil en el dominio del tiempo')
        types = ['R', 'L', 'LD']
        list_df = []

        for t in types:
            factor = df_lod_esc[t]
            df_t = df[df['type'] == t].copy(deep=True)
            df_t.index= df_t['date_time']
            df_t = df_t.join(other=factor, how='left')
            df_t.rename(columns={t: 'f3'}, inplace=True)
            df_t['value_MWh'] = df_t['avg_MWh'] * df_t['f3']
            df_t.rename(columns={'avg_MWh': 'plx_MWh'})
            list_df.append(df_t)

        return pd.concat(list_df)

    # Iter over each pickle_file/monthly dataframe
    for pkl in pkl_files:
        print_msg(f'Calculando nuevo perfil a partir de {pkl}')
        dfo = PickleZip.load(pkl)
        dfo = apply_lod_lod(df=dfo)
        dfo = df_to_time(df=dfo)
        dfo = apply_load_escalate(df=dfo)
        
        # Final make_up
        foo = dfo['suministrador_clave'].str.split('_&_', expand=True)
        dfo['suministrador'] = foo[0]
        dfo['clave'] = foo[1]
        dfo.drop(columns=['suministrador_clave'], inplace=True)
        dfo['month'] = dfo['date_time'].dt.strftime('%m')
        dfo.reset_index(drop=True, inplace=True)

        # write to pickle file
        print(f'pkl: {pkl}')
        pkl_new = pkl.replace('22', '23') # new year
        pkl_new = pkl_new.replace('intermediate//',
                                  'intermediate//new_profiles//') # new folder
        pkl_new = pkl_new.replace('zip', 'pkl') 
        pkl_new = pkl_new.replace('_plx', '') # It is not in Plexos domain
        print(f'pkl_new: {pkl_new} {len(dfo)}')
        print(dfo)
        try:
            dfo.to_pickle(pkl_new)
        except OSError as ose:
            print(ose)
            csv_new = pkl_new.replace('.pkl', '.csv')
            dfo.to_csv(csv_new, encoding='latin', index=False)
        
def main():    
    def factor_i_base(df_i:pd.DataFrame):
        df_i_avg = set_df_avg(df=df_i, columns=['day', 'hour', 'type'])
        f_i_base = df_i_avg['avg_MWh'].div(other=df_base_avg['avg_MWh'],
                                    axis=0)
        f_i_base.sort_index(inplace=True)
        f_i_base.rename('factor_i_base', inplace=True)
        f_i_base.fillna(1.0, inplace=True)
        return f_i_base

    def transf_plx_list():
        # Iter over each pickle_file/monthly dataframe
        for pkl in pkl_by_month:
            print_msg(f'transf_plx_list: Leyendo {pkl}')
            df = PickleZip.load(pkl)
            month = pkl[-6:-4]
            
            print_msg('transf_plx_list: transformando df_1 (llaves que están en el mes base)')
            if len(df) > 0:
                in_base = df['suministrador_clave'].isin(
                    values=df_base_plx['suministrador_clave'].unique())
                df_1= df[in_base].copy(deep=True)
                df_1_avg = set_df_avg(df=df_1,
                                    columns=['day', 'hour', 'type'])
                df_1_anc = df_1_avg.copy(deep=True)
                df_1_anc['avg_MWh'] = -0.01 # to identify missing values
                df_1_plx, foo1, foo2 = set_df_month_plx(df=df_1, df_anc=df_1_anc)
                df_1_plx['f1'] = 1.0 # no need to escale over base month
            else:
                foo = pd.DataFrame({'day': [], 'hour': [], 'type': [], 'month': []})
                df_1_plx = pd.DataFrame({'suministrador_clave': [], 'avg_MWh': [], 'f1': []})
                df_1_plx.index = pd.MultiIndex.from_frame(foo)
               
            print_msg('transf_plx_list: transformando df_2 (llaves en df_base_plx que NO están en el mes en curso')
            f1 = factor_i_base(df_i=df) # length = 4 * 24 * 3 = 288
            
            not_in_i_month = ~ df_base_plx['suministrador_clave'].isin(
                values=df['suministrador_clave'].unique())
            df_2_plx = df_base_plx[not_in_i_month].copy(deep=True)
            df_2_plx = df_2_plx.join(other=f1, how='left')
            df_2_plx.rename(columns={'factor_i_base': 'f1'}, inplace=True)
            df_2_plx['avg_MWh'] = df_2_plx['avg_MWh'] * df_2_plx['f1'] 
            # foo = df_2_plx.reset_index(drop=False)[['day', 'hour', 'type', 'month']]
            # foo['month'].fillna(1.0, inplace=True)
            # df_2_plx.index = pd.MultiIndex.from_frame(foo)
            
            df_out_plx = pd.concat([df_1_plx, df_2_plx])

            # add month to index
            df_inx = df_out_plx.index.to_frame()
            df_inx['month'] = month
            df_out_plx.index = pd.MultiIndex.from_frame(
                df_inx[['month', 'day', 'hour', 'type']])
            # Write into pickle file
            pkl_out_plx = pkl.replace('input', 'intermediate')
            pkl_out_plx = pkl_out_plx.replace('RETIROS', 'RETIROS_plx')
            PickleZip.dump(varname=df_out_plx, zip_file=pkl_out_plx)
            
            # Garbage collector
            del df_out_plx
            del df_inx
            gc.collect()
    
    def repair_group(df:pd.DataFrame, key:str, df_anc:pd.DataFrame):
        # Procedure: join with an ancillary df to identify missing values.
        # then replace missing values w/average values from the same key
        cols_index = ['day', 'hour', 'type']
        df.index = pd.MultiIndex.from_frame(df[cols_index])
        df.drop(columns=cols_index, inplace=True)
        df_repaired = df.join(other=df_anc,
                                how='right',
                                rsuffix='_r')
        df_repaired['avg_MWh'].fillna(df_repaired['avg_MWh_r'],
                                        inplace=True)
        
        load_type = df.index[0][2]
        foo = df_repaired.index.get_loc_level(key=load_type, level=2)
        df_repaired = df_repaired[foo[0]]
        df_repaired['avg_MWh'].replace(to_replace=-0.01,
                                        value=0.0,
                                        inplace=True)
        
        df_repaired['suministrador_clave'].fillna(key, inplace=True)
        df_repaired.drop(columns=['avg_MWh_r'], inplace=True)
        df_repaired.sort_index(inplace=True)
    
        return df_repaired

    def set_df_month_plx(df:pd.DataFrame, df_anc:pd.DataFrame):
        print_msg(f'set_df_month_plx: Preparando df')
      
        columns=['suministrador_clave', 'month', 'day', 'hour', 'type']
        df_0 = set_df_avg(df=df, columns=columns)
        df_0.index = df_0.index.droplevel(1) # Drop "month"
        df_0.reset_index(inplace=True) # clave, day, type), avg_MWh)
        
        # Group by 'clave'. Then check 
        groups_key = df_0.groupby(['suministrador_clave'])
        groups_repaired = []
        keys_ok = []
    
        for group in groups_key.__iter__():
            g_name = group[0]
            g_df = group[1]
            dim = len(g_df.index.unique())
            keys_ok.append(
                g_name) if dim  == 96 else lambda x:None
            groups_repaired.append(
                repair_group(g_df, g_name, df_anc)) if dim  != 96 else lambda x:None

        rows_ok = df_0['suministrador_clave'].isin(keys_ok)
        df_ok = df_0[rows_ok].copy(deep=True) ####aqui
        cols_index = ['day', 'hour', 'type']
        df_ok.index = pd.MultiIndex.from_frame(df_ok[cols_index])
        df_ok.drop(columns=cols_index, inplace=True)
    
        df_repaired = pd.concat(groups_repaired)
        df_plx = pd.concat([df_ok, df_repaired])
        df_plx.sort_index(inplace=True)
      
        # foo = pd.DataFrame({'day': [], 'hour': [], 'type': []})
        # df_plx = pd.DataFrame({'suministrador_clave': [], 'avg_MWh': []})
        # df_ok = pd.DataFrame({'suministrador_clave': [], 'avg_MWh': []})
        # df_repaired = pd.DataFrame({'suministrador_clave': [], 'avg_MWh': []})
        # df_repaired.index = pd.MultiIndex.from_frame(foo)

        
        return df_plx, df_ok, df_repaired

    ###################################################
    ## The good stuff !!
    
    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')
    df_plx_lod_lod = pd.read_pickle('pkl//input//plx_lod_lod.pkl')
    df_plx_lod_esc = pd.read_pickle('pkl//input//plx_lod_esc.pkl')
    
    #####################
    # Business' rules
    # 1.- Promediar el mes base según tupla (D, H, T). Esto servirá como
    #     input para el cálculo de factores de escalamiento del mes i, y
    #     además para reparar claves incompletas en el mes base
    print_msg('main: paso 1, trabajando con mes base')
    pkl_base = 'pkl//input//RETIROS_2212.zip'
    df_base = PickleZip.load(zip_file=pkl_base)
    df_base_avg = set_df_avg(df=df_base, columns=['day', 'hour', 'type'])
    
    # 2.- En mes base, comenzar reparando las claves incompletas-
    #     Luego, para cada clave, obtener promedio para cada
    #     tupla (clave, D, H), i.e, transformar al "dominio Plexos". 
    #     df_base_plx tiene de indice (D, H, T) y columnas (clave, avg_MWh)
    print_msg('main: paso 2, reparando mes base y transformado a dominio Plexos')
    df_base_aux = df_base_avg.copy(deep=True)
    df_base_aux['avg_MWh'] = -0.01 # to identify missing values
    df_base_plx, df_ok, df_repaired = set_df_month_plx(df=df_base,
                                                       df_anc=df_base_aux)
    df_base_plx.to_pickle('df_base_plx.pkl')
    df_ok.to_pickle('df_ok.pkl')
    df_repaired.to_pickle('df_repaired.pkl')
    # Write df_base into pickle file
    pkl_base_plx = pkl_base.replace('input', 'intermediate')
    pkl_base_plx = pkl_base_plx.replace('RETIROS', 'RETIROS_plx')
    PickleZip.dump(varname=df_base_plx, zip_file=pkl_base_plx)   
        
    # 3.- Escribir el "mes i" en términos del mes base, mediante:
    #     - Un factor de escala mes_i/mes base si la llave existe en el mes
    #       base.
    #     - Promedios para cada llave y cada tupla (D, M, H, T) que no exista
    #       en mes base
    print_msg('main: paso 3, transformando cada mes a dominio Plexos')
    pkl_by_month = ['pkl//input//RETIROS_22{:02}.zip'.format(i)
                        for i in range(1, 12)]
    transf_plx_list() 
    
    # Make up df_base
    df_base_plx['f1'] = 1.0 # no need to escale over itself
    df_inx = df_base_plx.index.to_frame()
    df_inx['month'] = '12'
    df_base_plx.index = pd.MultiIndex.from_frame(
        df_inx[['month', 'day', 'hour', 'type']])
    # write to pickle file
    PickleZip.dump(varname=df_base_plx,
                   zip_file='pkl//intermediate//RETIROS_plx_2212.zip')
    del df_base_plx
    gc.collect()

    # 4.- Cada mes i (incluido el base), escalarlo para el nuevo año a partir de las simulaciones
    #     en Plexos, usando los archivos "Lod_Lod.csv" y "Load_Escalator"
    #     y como resultado, se obtienen horarios para cada clave que
    #     fueron consideradas en el mes base
    print_msg('main: paso 4, generando perfiles para nuevo año en el dominio del tiempo')
    df_lod_lod = pd.read_pickle('pkl/input//plx_lod_lod.pkl')
    df_lod_lod_sum = set_df_sum(df=df_lod_lod)
    df_lod_esc= pd.read_pickle('pkl/input//plx_lod_esc.pkl')
    df_lod_esc_extended = set_df_lod_esc_extended(df=df_lod_esc, year=2023)
    df_lod_esc_extended.to_pickle('df_lod_esc_extended.pkl')

    pkl_plx_by_month = ['pkl//intermediate//RETIROS_plx_22{:02}.zip'.format(i)
                         for i in range(1, 13)]
    
    calculate_new_profiles(pkl_files=pkl_plx_by_month,
                           df_lod_lod_sum=df_lod_lod_sum,
                           df_lod_esc=df_lod_esc_extended, year=2023, cfg=cfg)
    
    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo de ejecucion: {t_delta}')
    # return df_base_plx, df_base_ok, df_base_to_repair, df_base_to_repair_plx, df_base_avg
  
# idx = pd.IndexSlice
# df.loc[idx[:, '01', 'H1', :]]