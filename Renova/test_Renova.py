import unittest
import sys
sys.path.append('D:/frontera-proyectos/P3 Coordinador Renova/Proyecto real/cen_garantias_renova')
CFG='D:/frontera-proyectos/P3 Coordinador Renova/Proyecto real/cen_garantias_renova/config.cfg'
from RenovaCSV import RenovaCSVCreation as RC
import pandas as pd
from configobj import ConfigObj
## Crear archivo config con las rutas para los datos 11-2021 y 12-2021, y los resultados esperados 11-2021 y 12-2021 /listo
#### Test para 2 periodos, menor o igual a 11-2021 y mayor a 11-2021  
 
## obtener dataframes de la ejecución del programa /listo
## obtener dataframes de resultados esperados /listo

############ dataframes necesarios:
## dataframe CSV intermedio
## dataframe CSV final
## dict dataframes warnings: {df_dict_centrales:DF, 
#                             df_dict_generadores:DF,
#                             df_dict_clientes:DF, 
#                             df_BERNC:DF, 
#                             df_asignacion:DF, 
#                             df_decimales_no_traspasados:DF}
## dataframes decimales sobrantes
class Result():
    def __init__(self,date:str):
        self.df_csv_intermedio = pd.DataFrame({})
        self.df_csv_final = pd.DataFrame({})
        self.dict_df_warnings = dict()
        self.df_decimals_next_month = pd.DataFrame({})
        self.date = date

class ExpectedResults(Result):
    """Clase que permite obtener los resultados esperados"""
    def __init__(self,date):
        super().__init__(date)
    
    def get_results(self):
        """"Leer resultados esperados"""
        paths = ConfigObj('config_test.cfg')['PATHS']['RESULTS'][self.date]
        self.df_csv_final = pd.read_csv(paths['CSV_final']) # listo
        self.df_csv_intermedio = pd.read_csv(paths['CSV_intermedio']) # listo
        warnings_sheet = ['dicc_clientes',
                          'dicc_generadores', 
                          'dicc_centrales', 
                          'balance_ERNC', 
                          'Asignación', 
                          'decimales no traspasados']
        for sheet in warnings_sheet:
            self.dict_df_warnings[sheet] = pd.read_excel(paths['warnings'], sheet_name=sheet, index_col=0) # listo
        self.df_decimals_next_month = pd.read_excel(paths['decimales_sobrantes'], index_col=0) # listo
        pass


class ObtainedResults(Result):
    """Clase que permite obtener los resultados de la ejecución del programa"""
    def __init__(self, date):
        super().__init__(date)
        

    def get_results(self):
        """"Ejecuta el programa y obtiene resultados"""
        paths = ConfigObj('config_test.cfg')['PATHS']['DATA'][self.date]
        h = RC(date=self.date, is_test=True, cfg=CFG,
                mdb_path=paths['mdb_path'],
                ernc_path=paths['ernc_path'],
                dict_gen_path=paths['dict_gen_path'],
                dict_client_path=paths['dict_client_path'],
                dict_central_path=paths['dict_central_path'],
                dec_path=paths['dec_path'])
        h.run_csv_creation()
        self.df_csv_final = h.test_df_csv_final # listo
        self.df_csv_intermedio = h.test_df_csv_intermedio
        self.df_csv_intermedio.reset_index(drop=True, inplace=True)  #listo
        self.dict_df_warnings = h.test_dict_df_warnings # listo
        self.df_decimals_next_month = h.test_df_decimals_next_month #listo
        

r_er_old = ExpectedResults(date='2021-11')
r_or_old = ObtainedResults(date='2021-11')
r_er_new = ExpectedResults(date='2021-12')
r_or_new = ObtainedResults(date='2021-12')



############## TESTS ################################################
# class TestModifyDFOld(unittest.TestCase):
#     def test_normalize_valorizado(self):
#         self.assertEqual([1,2],[1,2])

#     def test_dict_info(self):
#         pass

#     def test_bal_info(self):
#         pass

#     def test_attribute_assignment(self):
#         pass

#     def test_set_attributes(self):
#         pass



# class TestModifyDFNew(unittest.TestCase):
#     def test_normalize_valorizado(self):
#         pass

#     def test_dict_info(self):
#         pass

#     def test_bal_info(self):
#         pass

#     def test_attribute_assignment(self):
#         pass

#     def test_set_attributes(self):
#         pass

# if __name__ == '__main__':
#     unittest.main()