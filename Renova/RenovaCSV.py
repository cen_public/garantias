from __future__ import annotations
from collections import defaultdict
import pandas as pd
from SharedCode.ValReader import ValReader
from SharedCode.BerncReader import BerncReader
from Renova.ModifyDF import ModifyDF
from configobj import ConfigObj

class CreationError(Exception):
    pass

class RenovaCSVCreation:
    """ Class to represent the csv creation process to be
        uploaded to Renova platform"""
    
    

    def __init__(self, cfg:str=None, val_path:str=None,
                ernc_path:str=None, dict_gen_path:str=None,
                dec_path:str=None, dict_central_path:str=None, 
                dict_client_path:str=None, date:str=None,
                is_test:bool=False):
        self.is_test = is_test
        self.__date = date
        if cfg != None:
            self.__cfg = ConfigObj(cfg)['RENOVA']
        else:
            self.__cfg = ConfigObj('config.cfg')['RENOVA']
        if val_path != None:
            self.__cfg['PATHS']['val_path'] = val_path
        if ernc_path != None:
            self.__cfg['PATHS']['ernc_path'] = ernc_path
        if dict_gen_path != None:
            self.__cfg['PATHS']['dict_gen_path'] = dict_gen_path
        if dict_client_path != None:
            self.__cfg['PATHS']['dict_client_path'] = dict_client_path
        if dec_path != None:
            self.__cfg['PATHS']['dec_path'] = dec_path
        if dict_central_path != None:
            self.__cfg['PATHS']['dict_central_path'] = dict_central_path
        if (date != None):
            if (int(date[0:4])<=2021) & (int(date[5:7])<=11):
                self.__cfg['VERSION']['version'] = 'old'
    
        self.df_month = pd.DataFrame({})
        self.ernc_bal = pd.DataFrame({})
        self.df_dict_gen = pd.DataFrame({})
        self.df_dict_client = pd.DataFrame({})
        self.df_decimals = pd.DataFrame({})
        self.df_dict_central = pd.DataFrame({})

        self.test_df_csv_intermedio = pd.DataFrame({})
        self.test_df_csv_final = pd.DataFrame({})
        self.test_dict_df_warnings = pd.DataFrame({})
        self.test_df_decimals_next_month = pd.DataFrame({})

    def run_csv_creation(self):
        """ It runs all the process involved into csv file creation """
        
        self.get_input_data()
        self.normalize_data()
        self.generate_warnings()
        
    
    def get_input_data(self):
        cfg_paths = self.__cfg['PATHS'] 
        self._ernc_balance_reader(ernc_path=cfg_paths['ernc_path'])
        # print(self.ernc_bal)
        self._dictionary_gen_reader(dict_gen_path=cfg_paths['dict_gen_path'])
        # print(self.df_dict_gen)
        self._dictionary_client_reader(dict_client_path=cfg_paths['dict_client_path'])
        # print(self.df_dict_client)
        self._dictionary_central_reader(dict_central_path=cfg_paths['dict_central_path'])
        # print(self.df_dict_central)
        self._decimals_reader(dec_path=cfg_paths['dec_path'])
        # print(self.df_decimals)
        self._val_reader(val_path=cfg_paths['val_path'])
        # print(self.df_month)

        return None
        
    def normalize_data(self):
        print('normalize data...')
        try:
            mod_df = ModifyDF(df=self.df_month,
                            df_ernc=self.ernc_bal,
                            df_dict_gen=self.df_dict_gen,
                            df_dict_clients=self.df_dict_client,
                            df_dec=self.df_decimals,
                            df_dict_central=self.df_dict_central,
                            cfg=self.__cfg,
                            date=self.__date,
                            test_df_csv_intermedio=self.test_df_csv_intermedio,
                            test_df_csv_final=self.test_df_csv_final,
                            test_dict_df_warnings=self.test_dict_df_warnings,
                            test_df_decimals_next_month=self.test_df_decimals_next_month,
                            is_test=self.is_test)
            mod_df.execute()
            self.ernc_bal = mod_df.df_ernc
            self.df_month = mod_df.df
            self.test_df_csv_intermedio=mod_df.test_df_csv_intermedio
            self.test_df_csv_final=mod_df.test_df_csv_final
            self.test_dict_df_warnings=mod_df.test_dict_df_warnings
            self.test_df_decimals_next_month=mod_df.test_df_decimals_next_month
        except Exception as e:
            msj_error = '''Error en el procesamiento.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e))
        return None

    def generate_warnings(self):
        pass


    def _val_reader(self, val_path:str):
        """ It reads valorizado csv or mdb and writes the df_month """
        print('val_reader')
        try:
            cfg_val = self.__cfg['VALORIZADO']
            val_reader = ValReader(cfg=cfg_val)
            self.df_month = val_reader.read(val_path=val_path)
            fixed_column_names = self.__cfg['FIXED_COLUMN_NAMES']['cols_names_valorizado']
            map_column_names = dict(zip(list(self.df_month.columns),fixed_column_names))
            self.df_month.rename(map_column_names, axis='columns', inplace=True)
        except Exception as e:
            msj_error = '''Error lectura Valorizado Access.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e))

    def _ernc_balance_reader(self, ernc_path:str):
        print('ernc_balance_reader')
        version = self.__cfg['VERSION']['version']
        if version == 'old':
            cfg_ernc = self.__cfg['ERNC_BALANCE_OLD']
        if version == 'new':
            cfg_ernc = self.__cfg['ERNC_BALANCE_NEW']
        try:
            bernc_reader = BerncReader(cfg=cfg_ernc)
            df = bernc_reader.read_excel(bernc_path=ernc_path)
            self.ernc_bal = bernc_reader.normalize_df(df=df, version=version)
            fixed_column_names = self.__cfg['FIXED_COLUMN_NAMES']['cols_names_ernc']
            map_column_names = dict(zip(list(self.ernc_bal.columns),fixed_column_names))
            self.ernc_bal = self.ernc_bal.rename(map_column_names, axis='columns')
        except KeyError as e:
            msj_error = '''Error lectura Balance ERNC.
                            No se encontraron las siguientes columnas, verifique cols_names en del archivo de configuracion'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e))
        except ValueError as e:
            msj_error = '''Error lectura Balance ERNC.
                            Verifique la fecha ingresada y/o el nombre de la hoja de excel correspondiente sheet_name en la configuración'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e))
        except Exception as e:
            msj_error = '''Error lectura Balance ERNC.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e)) 
        


    def _dictionary_gen_reader(self, dict_gen_path:str):
        print('dictionary_gen_reader')
        try:
            cfg_dict = self.__cfg['DICTIONARY']
            self.df_dict_gen = pd.read_excel(dict_gen_path, 
                                        header=int(cfg_dict['header']), 
                                        usecols=cfg_dict['usecols'],
                                        dtype={'ID_Barra_Infotecnica':str, 'ID_Unidad_Generadora_Infotecnica': str})
        
            # self.df_dict_gen = self.df_dict_gen[(self.df_dict_gen['Nombre'].notnull())&
            #                                     (self.df_dict_gen['Barra'].notnull())&
            #                                     (self.df_dict_gen['PMGD'].notnull())]
        except Exception as e:
            msj_error = '''Error lectura diccionario generadores.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e)) 

    def _dictionary_client_reader(self, dict_client_path:str):
        print('dictionary_client_reader')
        try:
            self.df_dict_client = pd.read_excel(dict_client_path, dtype={'ID INFOTECNICA':str})
            self.df_dict_client.set_index('id', inplace=True)
          
        except Exception as e:
            msj_error = '''Error lectura diccionario clientes.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e)) 


    def _decimals_reader(self, dec_path:str):
        print('decimals_last_month_reader')
        try:
            self.df_decimals = pd.read_excel(dec_path)
            index_names = ['key_', 'Hora_Mensual']
            self.df_decimals[index_names[1]] = 1
            self.df_decimals.set_index(['Unnamed: 0', index_names[1]], inplace=True)
            self.df_decimals.index.names = index_names
        except Exception as e:
            msj_error = '''Error lectura decimales mes anterior.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e)) 


    def _dictionary_central_reader(self, dict_central_path:str):
        print('dictionary_central_reader')
        try:
            self.df_dict_central = pd.read_excel(dict_central_path)
        except Exception as e:
            msj_error = '''Error lectura diccionario centrales.'''
            print(msj_error)
            print(e)
            raise CreationError(msj_error+'_&_'+str(e)) 

# c = RenovaCSVCreation()
# c.get_input_data()
   ##### correr en consola, directorio cen_garantias_renova
# >>> from Renova.RenovaCSV import RenovaCSVCreation as RC
# >>> h = RC(date='2021-11')
# >>> h.run_csv_creation()

