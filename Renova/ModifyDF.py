from ast import Return
from dataclasses import dataclass
import dataclasses
from unicodedata import decimal
from wsgiref.simple_server import demo_app
import pandas as pd
import numpy as np
import os

# pd.set_option('display.max_rows', None)
# pd.set_option('display.max_columns', None)
# pd.set_option('display.width', None)
# pd.set_option('display.max_colwidth', -1)

class Dict(dict):
    def __missing__(self, key):
        return key

@dataclass
class ModifyDF():
    is_test:bool
    test_df_csv_intermedio:pd.DataFrame
    test_df_csv_final:pd.DataFrame
    test_dict_df_warnings:pd.DataFrame
    test_df_decimals_next_month:pd.DataFrame
    df:pd.DataFrame
    df_ernc:pd.DataFrame
    df_dict_gen:pd.DataFrame
    df_dict_central:pd.DataFrame
    df_dict_clients:pd.DataFrame
    df_dec:pd.DataFrame
    cfg:dict
    date:str
    # no_asignados:list = dataclasses.field(default_factory=list)
    warnings:dict = dataclasses.field(default_factory=dict)

    results_path = ''

    def _decimal_shift(self, df):
        df = pd.DataFrame(df)
        df['parte_entera'] = df[df.columns[0]].apply(lambda x: int(x))
        df['parte_decimal'] = df[df.columns[0]].apply(lambda x: (x-int(x)))
        df['decimal_acumulado'] = df.groupby(['key_'])['parte_decimal'].cumsum()
        len_horas = len(df.index.get_level_values('Hora_Mensual').unique())
        # print(len_horas)
        len_key = len(df.index.get_level_values('key_').unique())
        # print(len_key)
        df_dec_ac_mod = list(df['decimal_acumulado'].apply(lambda x: int(x)))
        df_dec_ac_dec = list(df['decimal_acumulado'].apply(lambda x: (x-int(x))))
        sobras_mes = []

        for i in range(0,len_key):
            df_dec_ac_mod.insert(i*len_horas+i, 0)

        for i in range(0,len_key):
            df_dec_ac_mod.pop((i+1)*len_horas)

        for i in range(0,len_key):
            sobras_mes.append(df_dec_ac_dec[len_horas*(i+1)-1])

        df['ac_aux'] = df_dec_ac_mod

        df['add'] = df['decimal_acumulado'].apply(lambda x: int(x)) - df['ac_aux']

        df['A_final'] = df['parte_entera']+df['add']
        decimales_next_month = dict(zip(df.index.get_level_values('key_').unique(),sobras_mes))
        dff = df['A_final']
        return dff, decimales_next_month
        

    def _fill_nan(self, df, col, value):
        df[col] = df[col].fillna(value)
        return None

    def _norm_index(self, df, clave, desc, prop, nbar):
        df['key_'] = df[clave]+'_&_'+df[desc]+'_&_'+df[prop]+'_&_'+df[nbar]
        df.set_index(['key_'], inplace=True)
        # df.drop([clave, prop, nbar], axis=1, inplace=True)
        return None

    def set_tipo1_and_operatoria(self):
        print('Eliminando tipo1 N y T...')
        parametros = self.cfg['PARAMETROS']
        lista_consumos = list(parametros['lista_consumos'])
        lista_generadores = list(parametros['lista_generadores'])
        self.df = self.df[(self.df['tipo1'].isin(lista_consumos))|(self.df['tipo1'].isin(lista_generadores))]
        self.df['Operatoria'] = 'por definir'
        self.df.loc[self.df['MedidaHoraria']<0, 'Operatoria'] = 'R'
        self.df.loc[self.df['MedidaHoraria']>0, 'Operatoria'] = 'I'
        return None

    def create_index(self):
        print('Create key_ index...')
        mapeo_none = Dict({None: ''})
        self.df['descripcion'] = self.df['descripcion'].map(mapeo_none)
        self._norm_index(self.df, 'clave', 'descripcion', 
                    'propietario', 'nombre_barra')
        self._norm_index(self.df_ernc, 'Clave_BAEN', 'Descripcion_BAEN', 
                    'Propietario o Contratante (1)', 'Barra')
        return None

    def add_ernc_bal_info(self):
        print('añadiendo información del balance ERNC...')
        ## warnings 2.0

        ## duplicados
        list_key_duplicados = list(self.df_ernc[self.df_ernc.index.duplicated()]['Clave_BAEN'])
        self.df_ernc = self.df_ernc[~self.df_ernc.index.duplicated()]
        df_duplicados = pd.DataFrame({'clave': list_key_duplicados})
        df_duplicados['Observacion'] = 'Llave duplicada en el balance ERNC.'

        ## información faltante
        list_key_df_month = list(self.df[self.df['tipo1']=='G'].index.unique())
        list_key_bal = list(self.df_ernc.index.unique())

        ## si valorizado no BERNC
        si_val_no_ernc = [x for x in list_key_df_month if x not in list_key_bal]
        list_key_info_faltante = list(self.df.loc[si_val_no_ernc]['clave'].unique())
        df_info_faltante_BERNC = pd.DataFrame({'clave': list_key_info_faltante})
        df_info_faltante_BERNC['Observacion'] = 'Llave no se encuentra en el balance ERNC, si en el valorizado.'
        
        ## no valorizado si BERNC
        
        no_val_si_ernc = [x for x in list_key_bal if x not in list_key_df_month]
        list_key_info_faltante = list(self.df_ernc.loc[no_val_si_ernc]['Clave_BAEN'].unique())
        df_info_faltante_valorizado = pd.DataFrame({'clave': list_key_info_faltante})
        df_info_faltante_valorizado['Observacion'] = 'Llave no se encuentra en el valorizado, si en el balance ERNC.'

        ## genera warning
        df_warning = pd.concat([df_duplicados, df_info_faltante_BERNC, df_info_faltante_valorizado])
        if df_warning.empty:
            df_warning = pd.DataFrame({'No se han generado Warnings'})
        self.warnings['balance_ERNC'] = df_warning

        self.df = self.df.join(self.df_ernc)
        self.df['Tecnología']=self.df['Tecnología'].str.capitalize()
        self.df['ERNC']=self.df['ERNC'].str.capitalize()
        
        return None


    def add_dictionary_info_clients(self):
        print('añadiendo información del diccionario de clientes...')
        
        self.df['id'] = self.df['clave']+self.df['descripcion']+self.df['nombre_barra']
        ## warnings 2.0

        ## duplicados
        list_key_duplicados = list(self.df_dict_clients[self.df_dict_clients.index.duplicated()]['Clave'])
        self.df_dict_clients = self.df_dict_clients[~self.df_dict_clients.index.duplicated()]
        df_duplicados = pd.DataFrame({'clave': list_key_duplicados})
        df_duplicados['Observacion'] = 'Llave duplicada'

        # ## información faltante 
        parametros = self.cfg['PARAMETROS']
        lista_consumos = list(parametros['lista_consumos'])
        claves_dict = list(self.df_dict_clients.index.unique())
        claves_df_val = list(self.df[self.df['tipo1'].isin(lista_consumos)]['id'].unique())
        si_val_no_dict = [x for x in claves_df_val if x not in claves_dict]
        list_key_info_faltante = list(self.df[self.df['id'].isin(si_val_no_dict)]['clave'].unique())
        df_info_faltante = pd.DataFrame({'clave': list_key_info_faltante})
        df_info_faltante['Observacion'] = 'No encontrado en diccionario'

        ## campos vacíos
        list_key_campos_vacios = list(self.df_dict_clients[(self.df_dict_clients['RUT'].isna())|
                                                           (self.df_dict_clients['ID INFOTECNICA'].isna())]['Clave'])                                           
        df_campos_vacios = pd.DataFrame({'clave': list_key_campos_vacios})
        df_campos_vacios['Observacion'] = 'Contiene uno o más campos vacíos'

        


        ## genera warning
        df_warning = pd.concat([df_duplicados, df_info_faltante, df_campos_vacios])

        ## inserta columna tipo 1 no optimizado##################
        # total_list_key = list_key_duplicados + list_key_info_faltante + list_key_campos_vacios
        # if len(total_list_key)!=0:
        #     print('paso 1')
        #     for i in total_list_key:
        #         print('paso del for')
        #         try:
        #             df_warning.loc[df_warning['clave']==i, 'Tipo'] = self.df[(self.df['Hora_Mensual']==1)&
        #                                                             (self.df['clave']==i)]['tipo1'].copy(deep=True).values[0]
        #         except:
        #             df_warning.loc[df_warning['clave']==i, 'Tipo'] = ''
        #     print('paso 2')

        ## genera warning vacío
        if df_warning.empty:
            df_warning = pd.DataFrame({'No se han generado Warnings'})
        else:
            ## inserta columna tipo 1 optimizado##################
            dff = self.df[self.df['Hora_Mensual']==1].copy(deep=True).reset_index()
            df_warning = df_warning.join(dff.set_index('clave')['tipo1'], on='clave')
        self.warnings['dicc_clientes'] = df_warning

        self.df = self.df.join(self.df_dict_clients[['RUT', 'ID INFOTECNICA']], on='id')
        
        return None

    def add_dictionary_info_gen(self):
        print('añadiendo información diccionario generadores...')
        parametros = self.cfg['PARAMETROS']
        lista_generadores = list(parametros['lista_generadores'])
        ## warnings 2.0

        ## duplicados
        list_key_duplicados = list(self.df_dict_gen[self.df_dict_gen['clave'].duplicated()]['clave'])
        self.df_dict_gen = self.df_dict_gen[~self.df_dict_gen['clave'].duplicated()]
        df_duplicados = pd.DataFrame({'clave': list_key_duplicados})
        df_duplicados['Observacion'] = 'Llave duplicada'

        ## información faltante 
        claves_dict = list(self.df_dict_gen['clave'].unique())
        claves_df_val = list(self.df[self.df['tipo1'].isin(lista_generadores)]['clave'].unique())
        si_val_no_dict = [x for x in claves_df_val if x not in claves_dict]
        list_key_info_faltante = si_val_no_dict
        df_info_faltante = pd.DataFrame({'clave': list_key_info_faltante})
        df_info_faltante['Observacion'] = 'No encontrado en diccionario'

        ## campos distintos a PMGD vacíos
        list_key_campos_vacios = list(self.df_dict_gen[(self.df_dict_gen['ID_Barra_Infotecnica'].isna())|
                                                       (self.df_dict_gen['ID_Unidad_Generadora_Infotecnica'].isna())|
                                                       (self.df_dict_gen['RUT Propietario'].isna())]['clave'])
        df_campos_vacios = pd.DataFrame({'clave': list_key_campos_vacios})
        df_campos_vacios['Observacion'] = 'Contiene uno o más campos vacíos'

        ## campos PMGD vacíos
        list_key_campos_PMGD_vacios = list(self.df_dict_gen[self.df_dict_gen['PMGD'].isna()]['clave'])
        df_campos_PMGD_vacios = pd.DataFrame({'clave': list_key_campos_PMGD_vacios})
        df_campos_PMGD_vacios['Observacion'] = 'Campo PMGD vacío. Se reemplaza por "NO"'


        ## genera warning
        df_warning = pd.concat([df_duplicados, df_info_faltante, df_campos_vacios, df_campos_PMGD_vacios])
        if df_warning.empty:
            df_warning = pd.DataFrame({'No se han generado Warnings'})
        self.warnings['dicc_generadores'] = df_warning

        self.df = self.df.join(self.df_dict_gen.set_index('clave')[['ID_Unidad_Generadora_Infotecnica',
                                                                'ID_Barra_Infotecnica',
                                                                'PMGD',
                                                                'RUT Propietario']], 
                                                                on='clave')
        self.df['PMGD'] = self.df['PMGD'].fillna('No')
        self.df['PMGD']=self.df['PMGD'].str.capitalize()
        self.df['ID_Barra_Infotecnica'] = self.df['ID_Barra_Infotecnica'].fillna(self.df['ID INFOTECNICA'])
        self.df['RUT Propietario'] = self.df['RUT Propietario'].fillna(self.df['RUT'])
        self.df.drop(columns=['RUT', 'ID INFOTECNICA'], inplace=True)
        
        return None

    def add_dictionary_central_info(self):
        print('añadiendo información centrales...')
        parametros = self.cfg['PARAMETROS']
        lista_generadores = list(parametros['lista_generadores'])
        ## warnings 2.0

        ## duplicados
        list_key_duplicados = list(self.df_dict_central[self.df_dict_central['clave'].duplicated()]['clave'])
        self.df_dict_central = self.df_dict_central[~self.df_dict_central['clave'].duplicated()]
        df_duplicados = pd.DataFrame({'clave': list_key_duplicados})
        df_duplicados['Observacion'] = 'Llave duplicada'

        ## información faltante 
        claves_dict_central = list(self.df_dict_central['clave'].unique())
        claves_df_val = list(self.df[self.df['tipo1'].isin(lista_generadores)]['clave'].unique())
        si_val_no_dict = [x for x in claves_df_val if x not in claves_dict_central]
        list_key_info_faltante = si_val_no_dict
        df_info_faltante = pd.DataFrame({'clave': list_key_info_faltante})
        df_info_faltante['Observacion'] = 'No encontrado en diccionario'

        ## campos vacíos
        list_key_campos_vacios = list(self.df_dict_central[self.df_dict_central['Nombre Central Final'].isna()]['clave'])
        df_campos_vacios = pd.DataFrame({'clave': list_key_campos_vacios})
        df_campos_vacios['Observacion'] = 'Contiene uno o más campos vacíos'

        ## genera warning
        df_warning = pd.concat([df_duplicados, df_info_faltante, df_campos_vacios])
        if df_warning.empty:
            df_warning = pd.DataFrame({'No se han generado Warnings'})
        self.warnings['dicc_centrales'] = df_warning
   
        self.df = self.df.join(self.df_dict_central.set_index('clave')['Nombre Central Final'], 
                                                                on='clave')
        return None

    def fill_fag_nan(self):
        self._fill_nan(self.df, 'Factor de Ajuste de Generación por Ampliación (2)', 1)
        self._fill_nan(self.df, 'Factor de Ajuste de Generación (3)', 1)

    def add_clasification(self):
        print('añadiendo asignación...')
       
        self.df['asignación'] = 'no asignado'
        parametros = self.cfg['PARAMETROS']
        lista_tec_ENR = list(parametros['lista_tecnologias_ENR'])
        lista_tec_ER = list(parametros['lista_tecnologias_ER'])
        lista_consumos = list(parametros['lista_consumos'])
        lista_generadores = list(parametros['lista_generadores'])
        if self.cfg['VERSION']['version']=='new':
            self.df.loc[((self.df['Tecnología'].isin(lista_tec_ENR))&
                        (self.df['ERNC']=='No')),'asignación'] = 'AENR'

            self.df.loc[((self.df['Tecnología'].isin(lista_tec_ER))&
                        (self.df['ERNC']=='No')),'asignación'] = 'AER'

            self.df.loc[((self.df['Tecnología'].isin(lista_tec_ER))&
                        (self.df['ERNC']=='Si')),'asignación'] = 'AERNC'

            self.df.loc[((self.df['tipo1'].isin(lista_consumos))&
                        (self.df['MedidaHoraria']>0)),'asignación'] = 'Error'
                
            self.df.loc[((self.df['tipo1'].isin(lista_consumos))&
                        (self.df['MedidaHoraria']<=0)),'asignación'] = 'AC'

            self.df.loc[((self.df['tipo1'].isin(lista_generadores))&
                        (self.df['MedidaHoraria']<0)),'asignación'] = 'ACpropio'

            self.df.loc[((self.df['Tecnología'].isin(lista_tec_ENR))&
                        (self.df['ERNC']=='Si')),'asignación'] = 'mal asignado'
            
        if self.cfg['VERSION']['version']=='old':
            self.df.loc[((self.df['Tecnología'].isin(lista_tec_ER))&
                        (self.df['ERNC']=='No')),'asignación'] = 'AER'

            self.df.loc[((self.df['Tecnología'].isin(lista_tec_ER))&
                        (self.df['ERNC']=='Si')),'asignación'] = 'AERNC'

            self.df.loc[((self.df['tipo1'].isin(lista_consumos))&
                        (self.df['MedidaHoraria']>0)),'asignación'] = 'Error'
                
            self.df.loc[((self.df['tipo1'].isin(lista_consumos))&
                        (self.df['MedidaHoraria']<=0)),'asignación'] = 'AC'

            self.df.loc[((self.df['tipo1'].isin(lista_generadores))&
                        (self.df['MedidaHoraria']<0)),'asignación'] = 'ACpropio'

            self.df.loc[((self.df['tipo1'].isin(lista_generadores))&
                        (self.df['asignación']=='no asignado')),'asignación'] = 'AENR'
        return None

    def get_misallocated(self):
        print('guardando y reasignando casos mal asignados...')
        parametros = self.cfg['PARAMETROS']
        lista_generadores = list(parametros['lista_generadores'])
        
        ## warnings 2.0
        ## mal asignados
        df_campos_mal_asignados = self.df[self.df['asignación']=='mal asignado'][['clave', 'Hora_Mensual']]
        df_campos_mal_asignados.reset_index(drop=True, inplace=True)
        df_campos_mal_asignados['Observacion'] = 'Mal asignado'
        ## no asignados 
        df_campos_no_asignados = self.df[self.df['asignación']=='no asignado'][['clave', 'Hora_Mensual']]
        df_campos_no_asignados.reset_index(drop=True, inplace=True)
        df_campos_no_asignados['Observacion'] = 'No asignado'
        ## error
        df_campos_error = self.df[self.df['asignación']=='Error'][['clave', 'Hora_Mensual']]
        df_campos_error.reset_index(drop=True, inplace=True)
        df_campos_error['Observacion'] = 'Error'
        ## genera warning
        df_warning = pd.concat([df_campos_mal_asignados, df_campos_no_asignados, df_campos_error])
        if df_warning.empty:
            df_warning = pd.DataFrame({'No se han generado Warnings'})
        self.warnings['asignación'] = df_warning

        self.df.loc[((self.df['asignación']=='mal asignado')&
                    (self.df['ERNC']=='Si')), 'asignación'] = 'AERNC'
        self.df.loc[((self.df['tipo1'].isin(lista_generadores))&
                    (self.df['MedidaHoraria']<0)),'asignación'] = 'ACpropio'
        
        return None

    def set_atributes(self):
        def reordering_and_apply_factors():
            print('reordenando y aplicando factores...')
            self.df.set_index(['Hora_Mensual'], drop=False, append=True, inplace=True)
            self.df.sort_index(inplace=True)
            dfa = self.df.pivot(columns='asignación', values='MedidaHoraria')
            dfa[['f1', 'f2']] = self.df[['Factor de Ajuste de Generación por Ampliación (2)',
                                        'Factor de Ajuste de Generación (3)']]
            ## creando columnas NAN para los atributos en caso de no existir                  
            attributes_list = ['AC', 'ACpropio', 'AENR', 'AER', 'AERNC']
            missing_attributes = [x for x in attributes_list if x not in list(dfa.columns)]
            if len(missing_attributes)!=0:
                for x in missing_attributes:
                    dfa[x] = np.nan
            # print(missing_attributes)
            #################
            dfa['aux'] = dfa['AERNC'].copy(deep=True)
            dfa.loc[(dfa['AER'].isnull())&(dfa['AERNC'].notnull())&(dfa['f2']!=1), 'AERNC'] = dfa['aux']*dfa['f2']
            dfa.loc[(dfa['AERNC'].notnull())&(dfa['f2']!=1), 'AER'] = dfa['aux']-dfa['AERNC']
            dfa.drop(['aux'], axis=1, inplace=True)
            #########antiguo############
            # dfa.loc[(dfa['AER'].isnull())&(dfa['AERNC'].notnull())&(dfa['f2']!=1), 'AER'] = dfa['AERNC']*dfa['f2']
            # dfa.loc[(dfa['AERNC'].notnull())&(dfa['f2']!=1), 'AERNC'] = dfa['AERNC']-dfa['AER']
            # dfa.loc[(dfa['AERNC'].notnull())&(dfa['f2']==1), 'AERNC'] = dfa['AERNC']
            ######################################
            
            dfa =  dfa.fillna(0)
            dfa['AC'] = abs(dfa['AC']/1000)
            dfa['AENR'] = abs(dfa['AENR']*(dfa['f1']/1000))
            dfa['AER'] = abs(dfa['AER']*(dfa['f1']/1000))
            dfa['AERNC'] = abs(dfa['AERNC']*(dfa['f1']/1000))
            dfa['ACpropio'] = abs(dfa['ACpropio']/1000)
            dfa.drop(['f1','f2'], axis=1, inplace=True)
            self.df = self.df.join(dfa)
            

        def calculate_loss_attribute():
            
            dff = self.df[['AC','ACpropio','AENR','AER','AERNC','Nombre Central Final','PMGD','tipo1']].copy(deep=True)
            dff['suma_atributos']=-dff['AC']-dff['ACpropio']+dff['AENR']+dff['AER']+dff['AERNC']
            ap_total_hora = dff.groupby(level=1)['suma_atributos'].sum()
            df_ap_total_hora = pd.DataFrame(ap_total_hora)
            df_ap_total_hora.rename(columns = {'suma_atributos':'AP_total_hora'}, inplace=True)
            dff.reset_index(inplace=True)
            dff = dff.join(df_ap_total_hora, on='Hora_Mensual')
            dff.set_index(['key_', 'Hora_Mensual'], inplace=True)
            dff['participa_ap'] = 'NO'
            suma_atributo_central_hora = dff.groupby(['Nombre Central Final', 'Hora_Mensual']).sum()['suma_atributos']
            df_suma_atributo_central_hora = pd.DataFrame(suma_atributo_central_hora)
            df_suma_atributo_central_hora.rename(columns = {'suma_atributos': 'Energia_neta_central_hora'}, inplace=True)
            dff.reset_index(inplace=True)
            dff.set_index(['Nombre Central Final', 'Hora_Mensual'], inplace=True)
            dff = dff.join(df_suma_atributo_central_hora)
            dff.reset_index(inplace=True)
            dff.loc[(dff['tipo1']=='G')&(dff['PMGD']=='No')&(dff['Energia_neta_central_hora']>0)&(dff['Nombre Central Final'].notnull()), 'participa_ap'] = 'SI'
            dff.loc[(dff['participa_ap']=='SI'), 'EN_central_hora'] = dff['Energia_neta_central_hora']
            dff.loc[(dff['participa_ap']=='SI'), 'suma_atributos_participa_ap'] = dff['suma_atributos']
            EN_hora = dff.groupby('Hora_Mensual').sum()['suma_atributos_participa_ap']
            df_EN_hora = pd.DataFrame(EN_hora)
            df_EN_hora.rename(columns={'suma_atributos_participa_ap': 'EN_hora'}, inplace=True)
            dff = dff.join(df_EN_hora, on='Hora_Mensual')
            dff['AP_central_hora'] = dff['AP_total_hora']*dff['EN_central_hora']/dff['EN_hora']
            dff['AP_central_hora'] = dff['AP_central_hora'].fillna(0)
            dff.set_index(['key_', 'Hora_Mensual'], inplace=True)
            dff.loc[(dff['suma_atributos']>=0)&(dff['participa_ap']=='SI'), 'suma_atributo_inyecciones_para_AP']=dff['suma_atributos']
            suma_atributo_inyeccion_central_hora = dff.groupby(['Nombre Central Final', 'Hora_Mensual']).sum()['suma_atributo_inyecciones_para_AP']
            df_suma_atributo_inyeccion_central_hora = pd.DataFrame(suma_atributo_inyeccion_central_hora)
            dff.reset_index(inplace=True)
            dff.set_index(['Nombre Central Final', 'Hora_Mensual'], inplace=True)
            df_suma_atributo_inyeccion_central_hora.rename(columns={'suma_atributo_inyecciones_para_AP':'suma_atributo_inyeccion_central_hora_para_AP'}, inplace=True)
            dff = dff.join(df_suma_atributo_inyeccion_central_hora)
            dff['AP_final']=dff['AP_central_hora']*dff['suma_atributo_inyecciones_para_AP']/dff['suma_atributo_inyeccion_central_hora_para_AP']
            dff['AP_final']=dff['AP_final'].fillna(0)
            dff.reset_index(inplace=True)
            dff.set_index(['key_', 'Hora_Mensual'], inplace=True)
            self.df = self.df.join(dff[['suma_atributos', 'AP_total_hora', 'participa_ap', 'Energia_neta_central_hora',
                                        'EN_central_hora', 'suma_atributos_participa_ap', 'EN_hora', 'AP_central_hora',
                                        'suma_atributo_inyecciones_para_AP', 'suma_atributo_inyeccion_central_hora_para_AP', 'AP_final']])
            # self.df['AP'] = dff['AP_final'] ## renombrar AP_final: AP
            
        def calculate_AC_final():
            '''Calcula AC prorrateado'''
            ## Ai_SN = Atributo Ai sin procesar (sin restar AP y AC)
            dff = self.df[['AC','ACpropio','AENR','AER','AERNC','Nombre Central Final','PMGD','tipo1',
                           'suma_atributos', 'Energia_neta_central_hora', 'suma_atributo_inyecciones_para_AP',
                           'suma_atributo_inyeccion_central_hora_para_AP']].copy(deep=True)
            dff['participa_ac'] = 'NO'
            dff.loc[(dff['tipo1']=='G')&(dff['Nombre Central Final'].notnull()), 'participa_ac'] = 'SI'
            dff.loc[(dff['suma_atributos']<0)&(dff['participa_ac']=='SI') , 'suma_atributo_retiro_generadores']=abs(dff['suma_atributos'])
            AC_central_hora = dff.groupby(['Nombre Central Final', 'Hora_Mensual']).sum()['suma_atributo_retiro_generadores']
            df_AC_central_hora = pd.DataFrame(AC_central_hora)
            df_AC_central_hora.rename(columns={'suma_atributo_retiro_generadores':'AC_central_hora'}, inplace=True)
            dff.reset_index(inplace=True)
            dff.set_index(['Nombre Central Final', 'Hora_Mensual'], inplace=True)
            dff = dff.join(df_AC_central_hora)
            dff.reset_index(inplace=True)
            dff.set_index(['key_', 'Hora_Mensual'], inplace=True)
            dff.loc[(dff['suma_atributos']>=0)&(dff['participa_ac']=='SI'), 'suma_atributo_inyecciones_para_AC']=dff['suma_atributos']

            suma_atributo_inyeccion_central_hora = dff.groupby(['Nombre Central Final', 'Hora_Mensual']).sum()['suma_atributo_inyecciones_para_AC']
            df_suma_atributo_inyeccion_central_hora = pd.DataFrame(suma_atributo_inyeccion_central_hora)
            df_suma_atributo_inyeccion_central_hora.rename(columns={'suma_atributo_inyecciones_para_AC':'suma_atributo_inyeccion_central_hora_para_AC'}, inplace=True)
            dff.reset_index(inplace=True)
            dff.set_index(['Nombre Central Final', 'Hora_Mensual'], inplace=True)
            dff = dff.join(df_suma_atributo_inyeccion_central_hora)

            dff['AC_final'] = dff['AC_central_hora']*dff['suma_atributo_inyecciones_para_AC']/dff['suma_atributo_inyeccion_central_hora_para_AC']
            dff['AC_final'] = dff['AC_final'].fillna(0)
            dff.reset_index(inplace=True)
            dff.set_index(['key_', 'Hora_Mensual'], inplace=True)
            dff['AC_central_hora'] = dff['AC_central_hora'].fillna(0)
            self.df = self.df.join(dff[['participa_ac', 'suma_atributo_retiro_generadores', 'suma_atributo_inyecciones_para_AC',
                                        'suma_atributo_inyeccion_central_hora_para_AC', 'AC_central_hora', 'AC_final']])

        def subtract_AC_and_AP_from_attributes():
        ## Se debe hacer una funcion 
            self.df.rename(columns={'AENR': 'AENR_intermedio', 'AER': 'AER_intermedio', 'AERNC': 'AERNC_intermedio'}, inplace=True)
            self.df['AER'] = self.df['AER_intermedio']-((self.df['AP_final']+self.df['AC_final'])*self.df['AER_intermedio']/self.df['suma_atributos'])
            self.df['AENR'] = self.df['AENR_intermedio']-((self.df['AP_final']+self.df['AC_final'])*self.df['AENR_intermedio']/self.df['suma_atributos'])
            self.df['AERNC'] = self.df['AERNC_intermedio']-((self.df['AP_final']+self.df['AC_final'])*self.df['AERNC_intermedio']/self.df['suma_atributos'])
            self.df.loc[self.df['AER']<0, 'AER'] = 0
            self.df.loc[self.df['AENR']<0, 'AENR'] = 0
            self.df.loc[self.df['AERNC']<0, 'AERNC'] = 0
            self.df['AER'] = self.df['AER'].fillna(0)
            self.df['AENR'] = self.df['AENR'].fillna(0)
            self.df['AERNC'] = self.df['AERNC'].fillna(0)

        def join_AC_ACp():
            self.df['AC']=self.df['AC']+self.df['ACpropio']

        def add_decimal_last_month():
            index_names = list(self.df.index.names)
            index_df = list(self.df.index.get_level_values(index_names[0]).unique())
            index_df_dec = list(self.df_dec.index.get_level_values(index_names[0]).unique())
            si_dec_no_val = [x for x in index_df_dec if x not in index_df]
            si_dec_si_val = [x for x in index_df_dec if x in index_df]
            df_dec2 = self.df_dec.loc[si_dec_si_val]
            self.df[['AC','AENR','AER','AERNC', 'AP_final']]=self.df[['AC','AENR','AER','AERNC', 'AP_final']].copy(deep=True).add(df_dec2, fill_value=0)
            if len(si_dec_no_val)!=0:
                self.df_dec.reset_index(level=1, inplace=True)
                df_dec_no_traspasados = self.df_dec.loc[si_dec_no_val][['AC','AENR','AER','AERNC', 'AP_final']]
                df_dec_no_traspasados.reset_index(inplace=True)
                key = df_dec_no_traspasados['key_'].str.split('_&_', expand=True)
                key.columns = ['clave', 'descripcion', 'propietario', 'nombre_barra']
                df_dec_no_traspasados['clave'] = key['clave']
                df_dec_no_traspasados = df_dec_no_traspasados[['clave', 'AC','AENR','AER','AERNC', 'AP_final']]
            if len(si_dec_no_val)==0:
                df_dec_no_traspasados = pd.DataFrame({'Todos los decimales fueron traspasados': []})
            self.warnings['decimales no traspasados'] = df_dec_no_traspasados
            
        reordering_and_apply_factors()
        calculate_loss_attribute()
        calculate_AC_final() ## Nuevo Requerimiento
        subtract_AC_and_AP_from_attributes() ## Nuevo Requerimiento
        join_AC_ACp()
        add_decimal_last_month()
        


        return None

    def decimal_shift(self):
        print('traspasando decimales...')
        # print(self.df[['AC','AENR','AER','AERNC','AP','Nombre Central Final','PMGD','tipo1']])
        dff_ac, dec_next_month_ac = self._decimal_shift(self.df['AC'])
        dff_aenr, dec_next_month_aenr = self._decimal_shift(self.df['AENR'])
        dff_aer, dec_next_month_aer = self._decimal_shift(self.df['AER'])
        dff_aernc, dec_next_month_aernc = self._decimal_shift(self.df['AERNC'])
        dff_ap, dec_next_month_ap = self._decimal_shift(self.df['AP_final'])

        dec_next_month = {'AC': dec_next_month_ac,
                        'AENR': dec_next_month_aenr,
                        'AER': dec_next_month_aer,
                        'AERNC': dec_next_month_aernc,
                        'AP_final': dec_next_month_ap}
        self.df['AC'] = dff_ac
        self.df['AENR'] = dff_aenr
        self.df['AER'] = dff_aer
        self.df['AERNC'] = dff_aernc
        self.df['AP_final'] = dff_ap
        df_dec_next_month = pd.DataFrame(dec_next_month)
        if not(self.is_test):
            dec_next_month_path = os.path.join(self.results_path, 'decimales_sobrantes_'+self.date+'.xlsx')
            df_dec_next_month.to_excel(dec_next_month_path)
        self.test_df_decimals_next_month = df_dec_next_month.copy(deep=True)
        return None

    def get_and_delete_errors(self):
        print('eliminando casos con asignación error...')
        self.df = self.df[~(self.df['asignación']=='Error')].copy(deep=True)
        return None

    def create_folders_and_results_paths(self):
        print('generando carpeta para resultados...')
        if self.results_path == '':
            self.results_path = self.cfg['RESULTS']['results_path']
        self.results_path = os.path.join(self.results_path, self.date)    
        os.mkdir(self.results_path)
        return None

    def generate_warnings(self):
        print('generando advertencias...')
        
        ## Union Totales #######################3
        self.warnings['Totales'] = pd.concat([self.warnings['dict_sums_atr'], self.warnings['dict_sums_val']])
        self.warnings.pop('dict_sums_atr')
        self.warnings.pop('dict_sums_val')
        ########################################
        warnings_path = os.path.join(self.results_path, 'warnings.xlsx')
        with pd.ExcelWriter(warnings_path) as writer:
            for key in self.warnings:
                # print(key)
                self.warnings[key].to_excel(writer, sheet_name=key)
        return None


    def createCSV(self):
        print('Generando CSV...')
        if not(self.is_test):
            renova_csv_path = os.path.join(self.results_path, 'RenovaCSV.csv')
        
        df_renova = self.df.copy(deep=True)
        df_renova = self.df[((df_renova['AC']!=0)|
                          (df_renova['AENR']!=0)|
                          (df_renova['AER']!=0)|
                          (df_renova['AERNC']!=0)|
                          (df_renova['AP_final']!=0))&
                          (df_renova['MedidaHoraria']!=0)]
        df_renova.loc[df_renova['tipo1']=='G', 'ID_Barra_Infotecnica'] = ''
        ######## suma atributos ##############
        dff = df_renova.copy(deep=True)
        dff_totales = dff[['AP_final', 'AC', 'AER', 'AENR', 'AERNC']].copy(deep=True)
        dff_totales.rename(columns={'AP_final': 'AP'}, inplace=True)
        dff_totales = dff_totales.sum()
        dict_sums_atr = {f'Suma {k}, en [MWh]': dff_totales.loc[k] for k in list(dff_totales.index)}
        self.warnings['dict_sums_atr'] = pd.DataFrame.from_dict(dict_sums_atr, orient='index', columns=['Totales'])
        ######################################
        columns_csv = self.cfg['COLUMNS_CSV']
        columns_names = columns_csv['columns_names']
        columns_renames = columns_csv['columns_renames']
        colums_csv_renova = columns_csv['colums_csv_renova']
        dict_rename = dict(zip(columns_names, columns_renames))
        df_renova.rename(columns=dict_rename, inplace=True)
        self.test_df_csv_final = df_renova[colums_csv_renova].copy(deep=True).reset_index(drop=True)
        if not(self.is_test):
            df_renova[colums_csv_renova].to_csv(renova_csv_path, sep=',', index=False)
        filas_csv = len(df_renova)
        print(f'Cantidad de filas del archivo generado: {filas_csv}')
        return None

    def create_csv_details_renova(self):
        print('Generando CSV detallado Renova incluye decimales del mes anterior en la hora 1...')
        dff = self.df.copy(deep=True)
        self.test_df_csv_intermedio = dff.copy(deep=True)
        if not(self.is_test):
            detail_renova_csv_path = os.path.join(self.results_path, 'DetalleRenovaCSV.csv')
            dff.to_csv(detail_renova_csv_path, index=False) 
            # print(dff.columns)
        return None

    def get_an_delete_rut_misallocated(self):
        ('Obtiene y elimina tipo L, L_D, L_S y R con RUT generador...')
        list_rut_gen = list(self.df_dict_gen[self.df_dict_gen['RUT Propietario'].notnull()]['RUT Propietario'].unique())
        #### implementa la condicion para todos los consumos  #####################
        parametros = self.cfg['PARAMETROS']
        lista_consumos = list(parametros['lista_consumos'])
        list_campos_mal_asignados = list(self.df[(self.df['tipo1'].isin(lista_consumos))&(self.df['RUT Propietario'].isin(list_rut_gen))]['clave'].unique())
        
        ####### implementa la condición para consumo = 'L' ###################
        # list_campos_mal_asignados = list(self.df[(self.df['tipo1']=='L')&(self.df['RUT Propietario'].isin(list_rut_gen))]['clave'].unique())
        
        ######## df_campos_mal_asignados funciona ##############
        # df_campos_mal_asignados = pd.DataFrame({'clave': list_campos_mal_asignados})
        # df_campos_mal_asignados['Observacion'] = 'Tipo1 igual a "L", "L_D", "L_S" o "R", RUT pertenece a generador'
        #######################################################

        ######## df_campos_mal_asignados especificando tipo1 ##############
        df_campos_mal_asignados = self.df[(self.df['clave'].isin(list_campos_mal_asignados))&(self.df['Hora_Mensual']==1)][['clave', 'tipo1']].copy(deep=True)
        df_campos_mal_asignados.reset_index(inplace=True, drop=True)
        df_campos_mal_asignados['Observacion'] = 'Tipo1 igual a "L", "L_D", "L_S" o "R", RUT pertenece a generador'
        #####################################################################

        self.df = self.df[~((self.df['tipo1'].isin(lista_consumos))&(self.df['RUT Propietario'].isin(list_rut_gen)))].copy(deep=True)
        df_warning = df_campos_mal_asignados
        if df_warning.empty:
            df_warning = pd.DataFrame({'No se han generado Warnings'})
        self.warnings['consumo con RUT generador'] = df_warning
        return None

    def valorizado_sums(self):
        dff = self.df.copy(deep=True)
        dff_totales = dff.pivot(columns='tipo1')['MedidaHoraria'].fillna(0).sum()/1000
        dict_sums_val = {f'Suma valorizado, tipo1={k}, en [MWh]': dff_totales.loc[k] for k in list(dff_totales.index)}
        self.warnings['dict_sums_val'] = pd.DataFrame.from_dict(dict_sums_val, orient='index', columns=['Totales']) 

    def execute(self):
        if not(self.is_test):
            self.create_folders_and_results_paths() # Crea carpeta para resultados
        self.set_tipo1_and_operatoria() # Reduce el DataFrame con los tipo1 correspondientes 
        self.valorizado_sums()
        self.add_dictionary_info_clients() # Añade la información del diccionario de clientes
        self.add_dictionary_info_gen() # Añade la información del diccionario de generadores
        self.add_dictionary_central_info() # Añade la información del diccionario de centrales
        self.create_index() # Crea indice único para añadir información del balance ERNC
        self.add_ernc_bal_info() # Añade información balance ERNC
        self.fill_fag_nan() # Completa la información faltante de factores con un 1
        self.add_clasification() # Añade clasificación atributos
        self.get_misallocated() # Trata atributos mal asignados
        self.set_atributes() ## Recalcula atributos según factores, calcula AP y AC a restar, recalcula atributos restando AP y AC, ... 
                             ## ... Añade decimales sobrantes del mes anterior 
        self.create_csv_details_renova() # Genera csv con todas las columnas y valores contienen decimales
        self.decimal_shift() # Desplaza decimales de las columnas de interés, dejando unicamente enteros
        self.get_an_delete_rut_misallocated() # Genera warning y elimina los casos con tipo1 igual a L y rut perteneciente a generador
        self.get_and_delete_errors() # Elimina casos con asignación Error
        self.createCSV() # Genera CSV Renova
        self.test_dict_df_warnings = self.warnings
        if not(self.is_test):
            self.generate_warnings() # Genera archivo Warnings


# df = AccessReadear()
# df = pd.DataFrame()
# o_mod = ModifyDF(df=df)
# o_mod.execute()
