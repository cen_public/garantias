/* Create database*/
CREATE DATABASE IF NOT EXISTS garantias_renova;
/*Create user and roles */

CREATE ROLE IF NOT EXISTS 'service_user'@'%';
GRANT ALL ON  garantias_renova.* TO 'service_user';
CREATE USER IF NOT EXISTS 'user1'@'%' DEFAULT ROLE service_user;
ALTER USER 'user1'@'%' IDENTIFIED BY 'garantias_renova_654321';

/* Create table retiros */
CREATE TABLE IF NOT EXISTS retiros (
    day CHAR(2),
    month CHAR(2),
    hour CHAR(3),
    type CHAR(2),
    busbar NVARCHAR(30),
    supplier NVARCHAR(30),
    key_transfer NVARCHAR(30),
    type_ret CHAR(3), 
    value_kW FLOAT,
    value_MW FLOAT);
ALTER TABLE retiros ADD INDEX (day, month, hour, type);

/* Create table factors */
CREATE TABLE IF NOT EXISTS factors (
    day CHAR(2),
    month CHAR(2),
    hour CHAR(3),
    type CHAR(2),
    sum_plx_lod_MW FLOAT,
    sum_ret_MW FLOAT,
    factor_1 FLOAT,
    factor_2 FLOAT,
    factor_3 FLOAT);
ALTER TABLE factors ADD INDEX (day, month, hour, type);

/* Create table generators */
CREATE TABLE IF NOT EXISTS generators (
    hidrology NVARCHAR(15),
    date_time DATETIME,
    name NVARCHAR(40),
    gen_value FLOAT,
    unit CHAR(4));
ALTER TABLE generators ADD INDEX inx (hidrology, date_time, name);

/* Create table busbars */
CREATE TABLE IF NOT EXISTS busbars (
    hidrology NVARCHAR(15),
    date_time DATETIME,
    name NVARCHAR(40),
    price_value FLOAT,
    unit CHAR(5));
ALTER TABLE busbars ADD INDEX inx (hidrology, date_time, name);