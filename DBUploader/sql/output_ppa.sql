use garantias_renova;

drop table if exists ppa_simulation;
CREATE TABLE IF NOT EXISTS ppa_simulation (
    date_time DATETIME, 
    prefix NVARCHAR(15), 
    hidrology NVARCHAR(15), 
    ppa_id NVARCHAR(150), 
    seller NVARCHAR(40), 
    buyer NVARCHAR(40), 
    rule_type NVARCHAR(40), 
    is_consistent INT, 
    PxQ FLOAT, 
    clave_1 NVARCHAR(40), 
    busbar_1 NVARCHAR(40), 
    price_value_1 FLOAT, 
    value_MWh_1 FLOAT,
    clave_2 NVARCHAR(40), 
    busbar_2 NVARCHAR(40), 
    price_value_2 FLOAT, 
    value_MWh_2 FLOAT, 
    clave_3 NVARCHAR(40), 
    busbar_3 NVARCHAR(40), 
    price_value_3 FLOAT, 
    value_MWh_3 FLOAT, 
    clave_4 NVARCHAR(40), 
    busbar_4 NVARCHAR(40), 
    price_value_4 FLOAT, 
    value_MWh_4 FLOAT);