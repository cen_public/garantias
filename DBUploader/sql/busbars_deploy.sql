use garantias_renova;
drop table if exists busbars;
CREATE TABLE IF NOT EXISTS busbars (
    hidrology NVARCHAR(15),
    date_time DATETIME,
    name NVARCHAR(40),
    price_value FLOAT,
    unit CHAR(5));