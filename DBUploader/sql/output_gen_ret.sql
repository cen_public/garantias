use garantias_renova;

drop table if exists gen_pxq;
CREATE TABLE IF NOT EXISTS gen_pxq (
    prefix NVARCHAR(15),
    hidrology NVARCHAR(15),
    date_time DATETIME,
    month char(3),
    company NVARCHAR(40),
    name NVARCHAR(40),
    value_MWh FLOAT,
    unit CHAR(4),
    busbar NVARCHAR(40),
    price_value FLOAT,
    PxQ FLOAT);


drop table if exists ret_pxq;
CREATE TABLE IF NOT EXISTS ret_pxq (
    prefix NVARCHAR(15),
    date_time DATETIME,
    month char(3),
    type char(4),
    company NVARCHAR(40),
    suministrador NVARCHAR(60),
    clave NVARCHAR(40),
    value_MWh FLOAT,
    busbar NVARCHAR(40),
    price_value FLOAT,
    PxQ FLOAT);