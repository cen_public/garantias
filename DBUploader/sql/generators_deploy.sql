use garantias_renova;
drop table if exists generators;
CREATE TABLE IF NOT EXISTS generators (
    hidrology NVARCHAR(15),
    date_time DATETIME,
    name NVARCHAR(40),
    gen_value FLOAT,
    unit CHAR(4));