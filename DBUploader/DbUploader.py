from dataclasses import dataclass, field
import tempfile, uuid, os
import pandas as pd
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.Database.PickleZip import PickleZip
from configobj import ConfigObj

@dataclass
class FileToDB():
    cfg:ConfigObj = field(init=True)
    db_query:DBQueryMySQL = field(init=True)
    list_csv:list = field(default_factory=list) # A file could be splitted in several csv
   
    def __post_init__(self):
        # Set iter_by_folder and filepath
        self.iter_by_folder = self.cfg.as_bool('iter_by_folder')
        if self.iter_by_folder:
            self.filepath = None
            self.folderpath = self.cfg['folderpath']
        else:
            self.filepath = self.cfg['filepath']
        
        # cols_sorted
        if 'cols_sorted' in self.cfg.keys():
            self.cols_sorted = self.cfg['cols_sorted']
        else:
            self.cols_sorted = None
        
        # boolean_cols
        if 'boolean_cols' in self.cfg.keys():
            bc = self.cfg['boolean_cols']
            self.boolean_cols = [bc] if isinstance(bc, str) else bc
        else:
            self.boolean_cols = []
       
    def upload(self, td:tempfile.TemporaryDirectory):
        self._upload_preparation(td)
        self._insert_file_into_destiny_table()
        self._create_indexes_when_appicable()
    
    def _upload_preparation(self, td:tempfile.TemporaryDirectory):
        self.list_csv = [] # start with a fresh list
        
        if self.iter_by_folder: # If the file is splitted into several files
            self._upload_preparation_multiple(td)
        else:
            self._upload_preparation_single(td)
        
    def _upload_preparation_single(self, td:tempfile.TemporaryDirectory,
                                   add_prefixes:dict={}):
        """ It prepares the file/dataframe to be uploaded from a single file. Mind two things: 
            - A file can be splitted into several csv files to be properly uploaded
            - A temporary file for each csv file should be provided """
        def modify_frame(df: pd.DataFrame):
            if isinstance(add_prefixes, dict) and add_prefixes != {}:
                for k, v in add_prefixes.items():
                    df[k] = v
            return df

        var = self._load_file() # load the file content into memory

        if isinstance(var, pd.DataFrame): # case one frame
            var = modify_frame(var)
            self._set_temp_file(df=var, td=td)
            
        elif isinstance(var, dict): # case multiple frames inside a dictionary
            for key, df_value in var.items():
                df_value['dict_key'] = key # new column to identify the dictionary key
                modify_frame(df_value)
                self._set_temp_file(df=df_value, td=td)
        
    def _upload_preparation_multiple(self, td:tempfile.TemporaryDirectory):
        """ It prepares the file/dataframe to be uploaded from multiple files from a same folder.
            Underneath, it iterates through every file and the call _upload_prepaparation_single method 
            several times """
        def get_prefix_name(name):
            return name.split(self.cfg['prefix_sep'])[0]

        # files iterator
        scan_files = os.scandir(self.folderpath)
        iter_files = (entry for entry in scan_files
                      if entry.is_file() and (entry.name[-4:] == '.zip'
                      or entry.name[-4:] == '.pkl'))

        # Iter over mdb files    
        for file in iter_files:
            self.filepath = os.path.join(self.folderpath, file.name)
            prefix = get_prefix_name(file.name)
            add_prefixes = {'prefix': prefix}
            self._upload_preparation_single(td, add_prefixes=add_prefixes)
   
    def _insert_file_into_destiny_table(self):
        destiny_table = self.cfg['destiny_table']
        print(f'  Insertando archivo en tabla {destiny_table}')
        for csv_file in self.list_csv:
            print(f'    {csv_file}')
            self.db_query.insert_from_file(table=destiny_table,
                                           csv_file=csv_file)
    
    def _create_indexes_when_appicable(self):
        if 'indexes' in self.cfg.keys():
            destiny_table = self.cfg['destiny_table']
            print(f'  Creando indexes en tabla {destiny_table}')
            for k, v in self.cfg['indexes'].items():
                self.db_query.create_index(table=destiny_table,
                                           name=k, columns=tuple(v['columns']))
     
    def _load_file(self):
        var = None
        if self._get_extension() == '.zip':
            var = PickleZip.load(self.filepath)
        elif self._get_extension() == '.pkl':
            var = pd.read_pickle(self.filepath)
        return var
    
    def _get_extension(self):
        foo = self.filepath
        ext = foo[foo.rfind('.') :]
        return ext

    def _set_temp_file(self, df:pd.DataFrame,
                       td:tempfile.TemporaryDirectory):
        def map_boolean_cols():
            map_bc = {'True': 1, 'False': 0}
            for col in self.boolean_cols:
                df[col] = df[col].astype(str).map(map_bc)

        temp_name = f'{uuid.uuid4()}.csv'
        temp_filepath = os.path.join(td, temp_name)
        index_to_csv = self.cfg.as_bool('index_to_csv')
        
        map_boolean_cols()
        
        if self.cols_sorted is not None and self.cols_sorted != []:
            df = df[self.cols_sorted]

        df.to_csv(temp_filepath,
                  index=index_to_csv,
                  sep=self.cfg['sep'])
        self.list_csv.append(temp_filepath)


@dataclass
class DBUploadFilesByCategory():
    """ Class to upload files into DB by category.
        Every category is defined in a cfg file"""
    cfg: dict=field(init=True)
    db_query: DBQueryMySQL=field(init=True)
    temp_dir: str=field(default='')
    
    def __post_init__(self):
        """ Create a list with File objects """
        self.files = [FileToDB(f, self.db_query)
                      for f in self.cfg['files'].values()]
        
        # temp_dir. Set to None if temp_dir is '' (default value for TempDirecroty object)
        if self.temp_dir == '':
            self.temp_dir = None
          
    def deploy(self):
        with open(file=self.cfg['deploy_file'], mode='r') as f:
            deploy_str = f.read()
        self.db_query.execute_query(query='standard',
                                    operation = deploy_str,
                                    params=None,
                                    is_destructive=False) # It's non_destructive
        
    def upload_files(self):
        for f in self.files:
            with tempfile.TemporaryDirectory(dir=self.temp_dir) as td:
                f.upload(td)
