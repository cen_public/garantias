from sys import prefix
import pandas as pd
import os

def divide_frame(df):
    max_rows = 10000
    current_start = 0
    current_end = 10000
    n_rows = len(df)
    flag = True
    results = []
    while flag:
        if current_end > n_rows:
            flag =  False
        df_a = df.iloc[current_start:min(n_rows - 1, current_end)]
        current_start = current_end
        current_end = current_start + max_rows
        results.append(df_a)
    return results

prefix = os.path.join('Output_test', 'RetirosR')


df_rows = pd.read_csv(f'{prefix}.txt', sep='\t', encoding='latin')
df_s = divide_frame(df_rows)
for i, df in enumerate(df_s):
    df.to_csv(f'{prefix}-{i}.txt', sep='\t', index=None, encoding='latin')

df_data = pd.read_csv(f'{prefix}.Data.txt', header=None, sep='\t', encoding='latin')
df_s = divide_frame(df_data)
for i, df in enumerate(df_s):
    df.to_csv(f'{prefix}.Data-{i}.txt', sep='\t', header=None, index=None, encoding='latin')


# PxQ
for sample in [1,2,3]:
    df_data = pd.read_csv(f'{prefix}___PxQ___Sample {sample}.Data.txt', header=None, sep='\t', encoding='latin')
    df_s = divide_frame(df_data)
    for i, df in enumerate(df_s):
        df.to_csv(f'{prefix}___PxQ___Sample {sample}.Data-{i}.txt', sep='\t', header=None, index=None, encoding='latin')


# exec(open('divide_files.py').read())



