from SharedCode.Database.DBQuery import DBQueryMySQL
from DBUploader.DbUploader import DBUploadFilesByCategory
from SharedCode.LoadEnv import load_env
from configobj import ConfigObj

def main():
    load_env(filename='local_dev.env')
    cfg = ConfigObj('DBUploader//db_uploader.cfg')
    db_query = DBQueryMySQL()
    cfg_categories = cfg['Categories']
    temp_dir = cfg['General']['temp_dir']

    for category, values in cfg_categories.items():
        if not values.as_bool('skip_category'):
            print(f'Cargando categoría {category}')
            dbu = DBUploadFilesByCategory(cfg=values, db_query=db_query, temp_dir=temp_dir)
            dbu.deploy()
            dbu.upload_files()

if __name__ == '__main__':
    main()