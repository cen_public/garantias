# cen_garantias_renova
Codigos para el CEN para el sistema de cálculo de garantías y carga de datos a RENOVA

Pasos a ejecutar:
1.- Leer los archivos mdf de retiros con get_access_data.py
2.- Leer los resultados de Plexos con get_plexos.data.py
3.- Crear perfiles de retiros con set_profiles.py
4.- Preparar los vectores fijos de compraventas con ppa_preparation.py
5.- Simular las compraventas con ppa_simulation.py
6.- Calcular los balances neto y de compraventas con calculate_balance.py

