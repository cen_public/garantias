import os, datetime, gc, pickle, logging
import pandas as pd
from configobj import ConfigObj
from PPA.PPASimulation import PPASimulation
from SharedCode.LoadEnv import load_env
from SharedCode.Database.PickleZip import PickleZip
from dateutil.relativedelta import relativedelta
import time

def read_dictionary(file:str, sheet_name:str, cols:list):
    if len(cols) != 2:
        raise ValueError('dimension de col es distinta de 2')
    else:
        df = pd.read_excel(io=file, sheet_name=sheet_name)
        df.index = df[cols[0]].astype(str)
        return df[cols[1]].astype(str).to_dict()

def create_df_ppa(forms_path:str):
    # xlsx files iterator
    iter_xlsx_files = os.scandir(forms_path)
    xlsx_files = (entry for entry in iter_xlsx_files
                    if entry.is_file() and entry.name[-5:] == '.xlsx')

    df_bs = pd.DataFrame({})
    # Iter over xlsx files    
    for xlsx_f in xlsx_files:
        print(f'Revisando {xlsx_f.name}')
        df = df_from_form(file=xlsx_f)  # CEN form
        df_bs = pd.concat([df_bs, df])
    
    df_bs.to_excel('df_ppa.xlsx', index=False)
    return df_bs

def df_from_form(file:str):
    df = pd.DataFrame({})
    try:
        df = pd.read_excel(io=file, sheet_name='Compraventas')
    except Exception as e:
        print(f'{file} no pude ser leido, {e}')
    else:
        declarant = df.iloc[1, 1]
        df = pd.read_excel(io=file, sheet_name='Compraventas',
                            skiprows=6, dtype=str)
        foo = pd.notna(df['[ID Plataforma Contratos]']) & (pd.notna(df['[Id_Contrato FIFC]']))
        df = df[foo]
        df['declarant'] = declarant
        df.drop(columns=['Rut', 'Rut.1', 'Razón Social',
                            'Razón Social.1', '[Nombre Balance]'], inplace=True)
        df.rename(columns={'Unnamed: 12': 'rule',
                           'Nombre Balance ': 'seller',
                           'Nombre Balance .1': 'buyer',
                           '[Barra]': 'busbar',
                           '[Clave de transferencias]': 'clave'}, inplace=True)
        
        # Create a ppa_id
        ppa_id_cols = ['[ID Plataforma Contratos]',
                       '[Id_Contrato FIFC]', 'seller', 'buyer', 'clave']
        df['ppa_id'] = df[ppa_id_cols].agg('_&_'.join, axis=1)

        # Create fixed_id
        fixed_id_cols = ['[ID Plataforma Contratos]', '[Id_Contrato FIFC]']
        df['fixed_id_cols'] = df[fixed_id_cols].agg('_&_'.join, axis=1)

        df.reset_index()
    return df

def create_df_fixed(fixed_path:str, zip_path:str):
    # xlsx files iterator
    iter_xlsx_files = os.scandir(fixed_path)
    xlsx_files = (entry for entry in iter_xlsx_files
                    if entry.is_file() and entry.name[-5:] == '.xlsx')

    df_fv = pd.DataFrame({})
    # Iter over xlsx files    
    for xlsx_f in xlsx_files:
        print(f'Revisando {xlsx_f.name}')
        df = df_from_fixed(file=xlsx_f)  # CEN form
        df_fv = pd.concat([df_fv, df], axis=1)
    
    # split my month
    for m in range(1, 13):
        start = datetime.datetime(year=2022, month=m, day=1)
        end = start + relativedelta(months=1) - relativedelta(hours=1)
        dt = pd.date_range(start=start, end=end, freq='1H')
        df_m = df_fv.loc[df_fv.index.isin(dt)].copy(deep=True)
        df_m.reset_index(drop=False, inplace=True)
        df_m.rename(columns={'index': 'date_time'}, inplace=True)
        mm = "{:02d}".format(m)
        zip_file = f'fixed_vectors_{mm}.zip'
        zip_file = os.path.join(zip_path, zip_file)
        print(f'   Escribiendo {zip_file}')
        PickleZip.dump(varname=df_m, zip_file=zip_file)

def df_from_fixed(file:str):
    n_hours = 8760 
    df = pd.DataFrame({})
    try:
        df = pd.read_excel(io=file, sheet_name='Compraventas', skiprows=1)
        print(f'   Fin lectura')
    except Exception as e:
        print(f'{file} no pude ser leido, {e}')
    else:

        # get new columns names
        empresa = file.name.split('_2022_')[1]
        empresa = empresa.replace('.xlsx', '')
        print(empresa)
        new_cols = (empresa
                    + '_&_' + df['[ID Plataforma Contratos]'].astype(str)
                    + '_&_' + df['Id_Contrato_FIFC'].astype(str)).to_dict()
        
        # Set df_new
        dt_index = df.columns[4 : n_hours + 4]
        df_new = pd.DataFrame({})
        df_new.index = dt_index

        # Get df transposed. Then, fill df_new using join
        print(f'   Transponiendo y haciendo el join')
        df = df.transpose()
        df_new = df_new.join(other=df.iloc[4 : 4 + n_hours, :], how='left')
        df_new.rename(columns=new_cols, inplace=True)

        # Data is in kWh. Convert to MWh. Finally, create a DatetimeIndex
        df_new = df_new / 1000.0
        df_new.index = pd.DatetimeIndex(df_new.index.values)
    return df_new

def main(forms_path:str=None, fixed_path:str=None, ret_path:str=None,
         iny_path:str=None, fix_path:str=None, out_path=None):
    ###################################################
    # For develpoing purposes only
    if forms_path is None:
        forms_path = 'Input_test//Garantias//compraventas//formularios'
    if fixed_path is None:
        fixed_path = 'Input_test//Garantias//compraventas//vectores_fijos'
    if ret_path is None:
        ret_path = 'pkl//output//ret//PxQ'
    if iny_path is None:
        iny_path = 'pkl//output//generators//PxQ'
    if fix_path is None:
        fix_path = 'pkl//intermediate//ppa//fixed'
    if out_path is None:
        out_path = 'pkl//output//ppa'


    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')
    # logging.basicConfig(filename='example.log', encode='utf-8',
    #                     level=logging.DEBUG)
    # logger = logging.getLogger('PPA_simulation')

   
    # Business' rules
    # 1.- Read all forms and fixed vectors. Create a DataFrame with the info
    #     and a PPASimulation object
    
    # for developing purposes
    # df_ppa = create_df_ppa(forms_path=forms_path)
    df_ppa = pd.read_excel('df_ppa.xlsx')
       
    # Diccionarios de generadores, retiros y barras
    xlsx_dict_gen = 'Input_test//Garantias//diccionarios//Centrales_V05.xlsx'
    dict_gen_ck = read_dictionary(file=xlsx_dict_gen,
                                  sheet_name='Clave_CentralPlexos',
                                  cols=['CENTRAL_PLEXOS', 'CLAVE'])
    dict_gen_kb = read_dictionary(file=xlsx_dict_gen,
                                  sheet_name='Clave_BarraPlexos',
                                  cols=['CLAVE', 'BARRA_PLEXOS'])

    xlsx_dict_ret = 'Input_test//Garantias//diccionarios//Retiros_V06.xlsx'
    dict_ret_bus = read_dictionary(file=xlsx_dict_ret,
                                   sheet_name='Retiros',
                                   cols=['CLAVE', 'BARRA_PLEXOS'])
    
    dict_ivt_plx = read_dictionary(file=xlsx_dict_ret,
                                   sheet_name='Barras_IVT-Plexos',
                                   cols=['BARRA_IVT', 'BARRA_PLEXOS'])

    print('main: leyendo informacion de barras Plexos')
    pkl_plx_busbars = 'pkl//input//plx_busbars.zip'
    plx_busbars = PickleZip.load(zip_file=pkl_plx_busbars)

    ppa_simul = PPASimulation(df_ppa=df_ppa, plx_busbars=plx_busbars,
                              dict_gen_ck=dict_gen_ck,
                              dict_gen_kb=dict_gen_kb,
                              dict_ret_bus=dict_ret_bus,
                              dict_ivt_plx=dict_ivt_plx)
    
    # 2.- Process forms (process_df_ppa). Create companies and ppa's
    #     Also, check integrity of the added ppa's
    print('Paso 2')
    time.sleep(5)
    ppa_simul.process_forms()
        
    # 3.- Iter by all hidrologies and months. Simulate all the ppa's
    #     Check the rule application between parties.
    print('Paso 3')
    time.sleep(5)
    hidrologies = ['Sample 1', 'Sample 3', 'Sample 6']
    
    ppa_simul.simulate_ppa_collection(ret_path=ret_path, iny_path=iny_path,
                                      fix_path=fix_path, out_path=out_path,
                                      hidrologies=hidrologies)

    # 5.- From the validated ppa's, create a summary dataframe
    
    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo total de ejecucion: {t_delta}')
    return ppa_simul

