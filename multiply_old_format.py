from distutils import dep_util
import pandas as pd
from SharedCode.Database.PickleZip import PickleZip
import os

type_list = ['L', 'L_D', 'R']
# type_list = ['L', 'L_D']
hidrologies = ['Sample 1', 'Sample 2', 'Sample 3']

dict_cmg = PickleZip.load('pkl//input//plx_busbars.zip')

# ppa
ppa_prefix_ = os.path.join('Input_test', 'Garantias', 'compraventas')
csv = os.path.join(ppa_prefix_, 'Compraventas.txt')
df_ppa_id = pd.read_csv(csv, sep='\t', encoding='latin',
                        engine='python')
flag_ppa = 0

# gen
dict_gen = PickleZip.load('pkl//input//plx_generators.zip')
df_gen_id = pd.read_excel(io='Input_test//Garantias//diccionarios//Diccionario Centrales v.20220928.xlsx',
                          sheet_name='Unidades Generadoras Modeladas', index_col='Central')
map_gen_bus = df_gen_id['Barra Plexos'].to_dict()
flag_gen = 0

csv_output_path = 'Output_test'
ret_path  = 'Output_test'

for type_ in type_list:
    # Retiros
    ret_name_ = os.path.join(ret_path, f'Retiros{type_}')
    df_ret_data = pd.read_csv(f'{ret_name_}.Data.txt', sep='\t', header=None, encoding='latin', engine='python')
    df_ret_id = pd.read_csv(f'{ret_name_}.txt', sep='\t', encoding='latin', engine='python')
    

    print(f'type: {type_}')
    for hidro in hidrologies:
        print(f'  hidro: {hidro}')
        df_h = dict_cmg[hidro].copy(deep=True)
        df_h.drop(columns=['hidrology', 'unit'], inplace=True)
        df_h = df_h.pivot(index='name', columns = '_date', values = 'price_value')
        
        # Create df_ret (start with marginal costs)
        df_ret = df_ret_id['BarraPLP'].to_frame()
        df_ret.reset_index(drop=True, inplace=True)
        df_ret = df_ret.join(other=df_h, on='BarraPLP', how='left')
        df_ret.drop(columns=['BarraPLP'], inplace=True)
        mask = df_ret > 0 
        df_ret = df_ret[mask].fillna(0)
        
        # Multiply with data. Save file
        df_ret_data.columns = df_ret.columns
        df_ret = df_ret.multiply(df_ret_data)
        csv_name = f'Retiros{type_}___PxQ___{hidro}.Data.txt'
        csv = os.path.join(csv_output_path, csv_name)
        df_ret.to_csv(csv, sep='\t', index=False, header=False, decimal='.')

        # Create df_ppa (start with marginal costs)
        if flag_ppa < 3:
            file_ = os.path.join(ppa_prefix_, f'CompraVentas_{hidro}.Data.txt')
            df_ppa_data = pd.read_csv(file_, sep='\t', header=None)
            df_ppa = df_ppa_id['Barra Plexo'].to_frame()
            df_ppa.reset_index(drop=True, inplace=True)
            df_ppa = df_ppa.join(other=df_h, on='Barra Plexo', how='left')
            df_ppa.drop(columns=['Barra Plexo'], inplace=True)
            mask = df_ppa > 0 
            df_ppa = df_ppa[mask].fillna(0)
            
            # Multiply with data
            df_ppa_data.columns = df_ppa.columns
            df_ppa = df_ppa.multiply(df_ppa_data)
            csv_name = f'CompraVentas___PxQ___{hidro}.Data.txt'
            csv = os.path.join(csv_output_path, csv_name)
            df_ppa.to_csv(csv, sep='\t', index=False, header=False, decimal='.')
            flag_ppa += 1
        
        # Create df_gen (start with marginal costs)
        if flag_gen < 3:
            df_gen_data = dict_gen[hidro].drop(columns=['hidrology', 'unit'])
            df_gen_data = df_gen_data.pivot(index='name', columns = '_date', values = 'gen_value')

            df_gen = pd.DataFrame({}, index=df_gen_data.index)
            df_gen['Barra Plexos'] = df_gen.index.map(map_gen_bus)
            df_gen = df_gen.join(other=df_h, on='Barra Plexos', how='left')
            df_gen.drop(columns=['Barra Plexos'], inplace=True)
            mask = df_gen > 0 
            df_gen = df_gen[mask].fillna(0)
            
            # Multiply with data
            df_gen = df_gen.multiply(df_gen_data)
            csv_name = f'Inyecciones___PxQ___{hidro}.Data.txt'
            csv = os.path.join(csv_output_path, csv_name)
            df_gen.to_csv(csv, sep='\t', index=False, header=False, decimal='.')
            
            # Rows
            df_gen_rows = pd.DataFrame({}, index=df_gen_data.index)
            for col in ['Barra Plexos', 'Tipo', 'Empresa']:
                df_gen_rows[col] = df_gen_rows.index.map(df_gen_id[col].to_dict())
            df_gen_rows.reset_index(drop=False, inplace=True)
            df_gen_rows.rename(columns={'name': 'Central'})
            csv_name = f'Inyecciones___PxQ___{hidro}.txt'
            csv = os.path.join(csv_output_path, csv_name)
            df_gen_rows.to_csv(csv, sep='\t', index=False, encoding='latin')
            flag_gen += 1


# exec(open('multiply_old_format.py').read())