import pandas as pd
import unittest
from tests.testRenova.Results import CompareFrames as cf

dict_f1 = {'k1': {'col1': 10.00, 'col2': 30.2,  'col3': 'ABANICO'},
           'k2': {'col1': 20.10, 'col2': 41.3,  'col3': 'EL TORO'},
           'k3': {'col1': 37.65, 'col2':-12.4,  'col3': 'NUEVA RENCA'},
           'k4': {'col1': 17.10, 'col2': 35.3,  'col3': 'COLBUN'},
           'k5': {'col1': 9.999, 'col2': 8.987, 'col3': 'HIDROFEO'}}

f1 = pd.DataFrame.from_dict(dict_f1, orient='index')

dict_f2 = {'k1': {'col1': 10.00, 'col2': 30.2,  'col3': 'ABANICOS'},
           'k2': {'col1': 20.10, 'col2': 41.3,  'col3': 'EL BUEY'},
           'k3': {'col1': 37.65, 'col2': -12.4, 'col3': 'NUEVA RENCA'},
           'k4': {'col1': 17.10, 'col2': 25.3,  'col3': 'COLBUN'},
           'k5': {'col1': 10.00, 'col2': 8.99,  'col3': 'HIDROFEO'}}

f2 = pd.DataFrame.from_dict(dict_f2, orient='index')


class TestCompare(unittest.TestCase):
    def test_by_index_1(self):
        index_values = ['k1', 'k3']
        num_cols = ['col1', 'col2']
        res = cf.compare(f1=f1, f2=f2,
                         index_values=index_values,
                         num_cols=num_cols)
        self.assertTrue(res)
    
    def test_by_index_2(self):
        index_values = ['k1', 'k3']
        num_cols = ['col1', 'col2']
        str_cols = ['col3']
        res = cf.compare(f1=f1, f2=f2,
                         index_values=index_values,
                         num_cols=num_cols,
                         str_cols=str_cols)
        self.assertFalse(res)

    def test_by_index_3(self):
        index_values = ['k1', 'k2']
        str_cols = ['col3']
        res = cf.compare(f1=f1, f2=f2,
                         index_values=index_values,
                         str_cols=str_cols)
        self.assertFalse(res)

    def test_by_index_4(self):
        index_values = ['k1', 'k4']
        num_cols = ['col1', 'col2']
        str_cols = ['col3']
        res = cf.compare(f1=f1, f2=f2,
                         index_values=index_values,
                         num_cols=num_cols,
                         str_cols=str_cols)
        self.assertFalse(res)
    
    def test_by_index_5(self):
        index_values = ['k5']
        num_cols = ['col1', 'col2']
        str_cols = ['col3']
        res = cf.compare(f1=f1, f2=f2,
                         index_values=index_values,
                         num_cols=num_cols,
                         str_cols=str_cols)
        self.assertTrue(res)
   
    def test_by_index_6(self):
        index_col = 'col3'
        index_values = ['NUEVA RENCA', 'HIDROFEO']
        num_cols = ['col1', 'col2']
        res = cf.compare(f1=f1, f2=f2,
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)