import unittest
from tests.testRenova.Results import ExpectedResults, CalculatedResults
from tests.testRenova.Results import CompareFrames as cf


er_old_format = ExpectedResults(date='2021-11')
cr_old_format = CalculatedResults(date='2021-11')
er_new_format = ExpectedResults(date='2021-12')
cr_new_format = CalculatedResults(date='2021-12')

results = [er_old_format, cr_old_format, er_new_format, cr_new_format]

for result in results:
    result.get_results()

class TestRenovaOldFormat(unittest.TestCase):

    def test_2(self):
        """Prueba de normalización tabla valorizado"""
        er = er_old_format.df_csv_intermedio
        cr = cr_old_format.df_csv_intermedio

        index_values = [['K1', 1],
                        ['K2', 1],
                        ['K6', 2],
                        ['K6', 3]]
        index_col = ['clave', 'Hora_Mensual']
        str_cols = ['Operatoria']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         str_cols=str_cols)
        self.assertTrue(res) 
        self.assertFalse('T' in list(cr['tipo1'].unique()))
                
        
    
    def test_3(self):
        """Prueba de información faltante en diccionarios"""
        er = er_old_format.dict_df_warnings
        cr = cr_old_format.dict_df_warnings

        df_dicc_cen_er = er['dicc_centrales']
        df_dicc_cen_cr = cr['dicc_centrales']
        index_values_cen = ['K10']
        index_col_cen = 'clave'
        str_cols_cen = ['Observacion']
        res_cen = cf.compare(df_dicc_cen_er, df_dicc_cen_cr, 
                         index_values=index_values_cen,
                         index_col=index_col_cen,
                         str_cols=str_cols_cen)

        df_dicc_gen_er = er['dicc_generadores']
        df_dicc_gen_cr = cr['dicc_generadores']
        index_values_gen = ['K2', 'K3', 'K4', 'K5', 'K6', 'K10', 'K15']
        index_col_gen = 'clave'
        str_cols_gen = ['Observacion']
        res_gen = cf.compare(df_dicc_gen_er, df_dicc_gen_cr, 
                         index_values=index_values_gen,
                         index_col=index_col_gen,
                         str_cols=str_cols_gen)

        df_dicc_client_er = er['dicc_clientes']
        df_dicc_client_cr = cr['dicc_clientes']
        index_values_client = ['K8', 'K9']
        index_col_client = 'clave'
        str_cols_client = ['Observacion']
        res_client = cf.compare(df_dicc_client_er, df_dicc_client_cr, 
                         index_values=index_values_client,
                         index_col=index_col_client,
                         str_cols=str_cols_client)

        self.assertTrue(res_cen) 
        self.assertTrue(res_gen) 
        self.assertTrue(res_client)
        

    def test_4(self):
        """Prueba de información balance ERNC"""
        er = er_old_format.dict_df_warnings
        cr = cr_old_format.dict_df_warnings

        df_dicc_BERNC_er = er['balance_ERNC']
        df_dicc_BERNC_cr = cr['balance_ERNC']
        index_values_BERNC = ['K5', 'K10', 'K16']
        index_col_BERNC = 'clave'
        str_cols_BERNC = ['Observacion']
        res_BERNC = cf.compare(df_dicc_BERNC_er, df_dicc_BERNC_cr, 
                         index_values=index_values_BERNC,
                         index_col=index_col_BERNC,
                         str_cols=str_cols_BERNC)

        self.assertTrue(res_BERNC) 
    
    def test_5(self):
        """Prueba de asignación de atributos"""
        er = er_old_format.df_csv_intermedio
        cr = cr_old_format.df_csv_intermedio

        index_values = [['K1', 1],
                        ['K2', 1],
                        ['K3', 1],
                        ['K4', 1],
                        ['K5', 1],
                        ['K6', 1],
                        ['K7', 1],
                        ['K8', 1],
                        ['K9', 1],
                        ['K10', 1],
                        ['K15', 1]]
        index_col = ['clave', 'Hora_Mensual']
        str_cols = ['asignación']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         str_cols=str_cols)
         

        er = er_old_format.dict_df_warnings
        cr = cr_old_format.dict_df_warnings

        df_dicc_asig_er = er['asignación']
        df_dicc_asig_cr = cr['asignación']
        
        self.assertTrue(res)
        self.assertTrue(df_dicc_asig_cr[df_dicc_asig_cr['Hora_Mensual']==1].empty)
        self.assertTrue(df_dicc_asig_er[df_dicc_asig_er['Hora_Mensual']==1].empty)

    def test_6(self):
        """Prueba de cálculo de atributos"""
        er = er_old_format.df_csv_intermedio
        cr = cr_old_format.df_csv_intermedio

        index_values = [['K1', 1],
                        ['K2', 1],
                        ['K3', 1],
                        ['K4', 1],
                        ['K5', 1],
                        ['K6', 1],
                        ['K7', 1],
                        ['K8', 1],
                        ['K9', 1],
                        ['K10', 1],
                        ['K15', 1]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['ACpropio', 
                    'AC', 
                    'AENR_intermedio', 
                    'AER_intermedio', 
                    'AERNC_intermedio']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_7(self):
        """Prueba de cálculo de atributos de pérdida"""
        er = er_old_format.df_csv_intermedio
        cr = cr_old_format.df_csv_intermedio

        index_values = [['K1', 2],
                        ['K2', 2],
                        ['K3', 2],
                        ['K4', 2],
                        ['K5', 2],
                        ['K6', 2],
                        ['K7', 2],
                        ['K8', 2],
                        ['K9', 2],
                        ['K10', 2],
                        ['K15', 2]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['AP_central_hora', 'AP_final']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)


    def test_8(self):
        """Prueba de cálculo de atributos de consumo"""
        er = er_old_format.df_csv_intermedio
        cr = cr_old_format.df_csv_intermedio

        index_values = [['K1', 2],
                        ['K2', 2],
                        ['K3', 2],
                        ['K4', 2],
                        ['K5', 2],
                        ['K6', 2],
                        ['K7', 2],
                        ['K8', 2],
                        ['K9', 2],
                        ['K10', 2],
                        ['K15', 2]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['AC_central_hora', 'AC_final']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_9(self):
        """Prueba de recálculo de atributos AER, AENR, AERNC y AC"""
        er = er_old_format.df_csv_intermedio
        cr = cr_old_format.df_csv_intermedio

        index_values = [['K1', 2],
                        ['K2', 2],
                        ['K3', 2],
                        ['K4', 2],
                        ['K5', 2],
                        ['K6', 2],
                        ['K7', 2],
                        ['K8', 2],
                        ['K9', 2],
                        ['K10', 2],
                        ['K15', 2]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['AER', 'AENR', 'AERNC', 'AC']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_10(self):
        """Prueba de traspaso de decimales"""
        er = er_old_format.df_decimals_next_month
        cr = cr_old_format.df_decimals_next_month
        index_values = list(er.index)
        num_cols = ['AC', 
                    'AENR', 
                    'AER', 
                    'AERNC', 
                    'AP_final']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_11(self):
        """Prueba normalización CSV"""
        er = er_old_format.df_csv_final
        cr = cr_old_format.df_csv_final
        index_values = list(er.index)
        num_cols = ['AERC_MWh', 
                    'AERNC_MWh', 
                    'AENR_MWh', 
                    'AC_MWh', 
                    'AP_MWh']
        res = cf.compare(er, cr, 
                        index_values=index_values,
                        num_cols=num_cols)  
        
        self.assertEqual(list(cr[cr['Tipo']=='G']['ID_Barra'].unique()), [''])
        self.assertEqual(list(er.columns), list(cr.columns))
        self.assertTrue(res)


class TestRenovaNewFormat(unittest.TestCase):
    def test_2(self):
        """Prueba de normalización tabla valorizado"""
        er = er_new_format.df_csv_intermedio
        cr = cr_new_format.df_csv_intermedio

        index_values = [['K1', 1],
                        ['K2', 1],
                        ['K6', 2],
                        ['K6', 3]]
        index_col = ['clave', 'Hora_Mensual']
        str_cols = ['Operatoria']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         str_cols=str_cols)
        self.assertTrue(res) 
        self.assertFalse('T' in list(cr['tipo1'].unique()))
    
    def test_3(self):
        """Prueba de información faltante en diccionarios"""
        er = er_new_format.dict_df_warnings
        cr = cr_new_format.dict_df_warnings

        df_dicc_cen_er = er['dicc_centrales']
        df_dicc_cen_cr = cr['dicc_centrales']
        

        df_dicc_gen_er = er['dicc_generadores']
        df_dicc_gen_cr = cr['dicc_generadores']
        index_values_gen = ['K2', 'K3', 'K5', 'K6', 'K10', 'K12']
        index_col_gen = 'clave'
        str_cols_gen = ['Observacion']
        res_gen = cf.compare(df_dicc_gen_er, df_dicc_gen_cr, 
                         index_values=index_values_gen,
                         index_col=index_col_gen,
                         str_cols=str_cols_gen)

        df_dicc_client_er = er['dicc_clientes']
        df_dicc_client_cr = cr['dicc_clientes']
        index_values_client = ['K8', 'K13']
        index_col_client = 'clave'
        str_cols_client = ['Observacion']
        res_client = cf.compare(df_dicc_client_er, df_dicc_client_cr, 
                         index_values=index_values_client,
                         index_col=index_col_client,
                         str_cols=str_cols_client)

        self.assertEqual(len(df_dicc_cen_er), len(df_dicc_cen_cr))
        self.assertTrue(res_gen) 
        self.assertTrue(res_client)

    def test_4(self):
        """Prueba de información balance ERNC"""
        er = er_new_format.dict_df_warnings
        cr = cr_new_format.dict_df_warnings

        df_dicc_BERNC_er = er['balance_ERNC']
        df_dicc_BERNC_cr = cr['balance_ERNC']
        index_values_BERNC = ['K5', 'K10', 'K4']
        index_col_BERNC = 'clave'
        str_cols_BERNC = ['Observacion']
        res_BERNC = cf.compare(df_dicc_BERNC_er, df_dicc_BERNC_cr, 
                         index_values=index_values_BERNC,
                         index_col=index_col_BERNC,
                         str_cols=str_cols_BERNC)

        self.assertTrue(res_BERNC) 
    
    def test_5(self):
        """Prueba de asignación de atributos"""
        er = er_new_format.df_csv_intermedio
        cr = cr_new_format.df_csv_intermedio

        index_values = [['K1', 1],
                        ['K2', 1],
                        ['K3', 1],
                        ['K6', 1],
                        ['K8', 1],
                        ['K9', 1],
                        ['K10', 1],
                        ['K12', 1],
                        ['K13', 1],
                        ['K17', 1]]
        index_col = ['clave', 'Hora_Mensual']
        str_cols = ['asignación']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         str_cols=str_cols)
         

        er = er_new_format.dict_df_warnings
        cr = cr_new_format.dict_df_warnings

        df_dicc_asig_er = er['asignación']
        df_dicc_asig_cr = cr['asignación']
        index_values = [['K10', 1],
                        ['K13', 1],
                        ['K17', 1]]
        index_col = ['clave', 'Hora_Mensual']
        str_cols = ['Observacion']
        res_asig = cf.compare(df_dicc_asig_er, df_dicc_asig_cr, 
                              index_values=index_values,
                              index_col=index_col,
                              str_cols=str_cols)


        self.assertTrue(res)
        self.assertTrue(res_asig)


    def test_6(self):
        """Prueba de cálculo de atributos"""
        er = er_new_format.df_csv_intermedio
        cr = cr_new_format.df_csv_intermedio

        index_values = [['K1', 1],
                        ['K2', 1],
                        ['K3', 1],
                        ['K6', 1],
                        ['K8', 1],
                        ['K9', 1],
                        ['K10', 1],
                        ['K12', 1],
                        ['K13', 1],
                        ['K17', 1]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['ACpropio', 
                    'AC', 
                    'AENR_intermedio', 
                    'AER_intermedio', 
                    'AERNC_intermedio']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_7(self):
        """Prueba de cálculo de atributos de pérdida"""
        er = er_new_format.df_csv_intermedio
        cr = cr_new_format.df_csv_intermedio

        index_values = [['K1', 2],
                        ['K2', 2],
                        ['K3', 2],
                        ['K6', 2],
                        ['K8', 2],
                        ['K9', 2],
                        ['K10', 2],
                        ['K12', 2],
                        ['K13', 2],
                        ['K17', 2]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['AP_central_hora', 'AP_final']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_8(self):
        """Prueba de cálculo de atributos de consumo"""
        er = er_new_format.df_csv_intermedio
        cr = cr_new_format.df_csv_intermedio

        index_values = [['K1', 2],
                        ['K2', 2],
                        ['K3', 2],
                        ['K6', 2],
                        ['K8', 2],
                        ['K9', 2],
                        ['K10', 2],
                        ['K12', 2],
                        ['K13', 2],
                        ['K17', 2]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['AC_central_hora', 'AC_final']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_9(self):
        """Prueba de recálculo de atributos AER, AENR, AERNC y AC"""
        er = er_new_format.df_csv_intermedio
        cr = cr_new_format.df_csv_intermedio

        index_values = [['K1', 2],
                        ['K2', 2],
                        ['K3', 2],
                        ['K6', 2],
                        ['K8', 2],
                        ['K9', 2],
                        ['K10', 2],
                        ['K12', 2],
                        ['K13', 2],
                        ['K17', 2]]
        index_col = ['clave', 'Hora_Mensual']
        num_cols = ['AER', 'AENR', 'AERNC', 'AC']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         index_col=index_col,
                         num_cols=num_cols)
        self.assertTrue(res)

    def test_10(self):
        """Prueba de traspaso de decimales"""
        er = er_new_format.df_decimals_next_month
        cr = cr_new_format.df_decimals_next_month
        index_values = list(er.index)
        num_cols = ['AC', 
                    'AENR', 
                    'AER', 
                    'AERNC', 
                    'AP_final']
        res = cf.compare(er, cr, 
                         index_values=index_values,
                         num_cols=num_cols)
        

        er = er_new_format.dict_df_warnings
        cr = cr_new_format.dict_df_warnings
        df_dicc_dec_er = er['decimales no traspasados']
        df_dicc_dec_cr = cr['decimales no traspasados']
        index_values = ['K4']
        index_col = 'clave'
        num_cols = ['AC', 
                    'AENR', 
                    'AER', 
                    'AERNC', 
                    'AP_final']
        res_warning = cf.compare(df_dicc_dec_er, df_dicc_dec_cr, 
                                 index_values=index_values,
                                 index_col=index_col,
                                 num_cols=num_cols)  

        self.assertTrue(res)
        self.assertTrue(res_warning)  

    def test_11(self):
        """Prueba normalización CSV"""
        er = er_new_format.df_csv_final
        cr = cr_new_format.df_csv_final
        index_values = list(er.index)
        num_cols = ['AERC_MWh', 
                    'AERNC_MWh', 
                    'AENR_MWh', 
                    'AC_MWh', 
                    'AP_MWh']
        res = cf.compare(er, cr, 
                        index_values=index_values,
                        num_cols=num_cols)  

        er_dict = er_new_format.dict_df_warnings
        cr_dict = cr_new_format.dict_df_warnings
        df_dicc_RUT_er = er_dict['consumo con RUT generador']
        df_dicc_RUT_cr = cr_dict['consumo con RUT generador']
        index_values = ['K17']
        index_col = 'clave'
        str_cols = ['Observacion']
        res_warning = cf.compare(df_dicc_RUT_er, df_dicc_RUT_cr, 
                        index_values=index_values,
                        index_col=index_col,
                        str_cols=str_cols)  

        self.assertEqual(list(cr[cr['Tipo']=='G']['ID_Barra'].unique()), [''])
        self.assertEqual(list(er.columns), list(cr.columns))
        self.assertTrue(res)
        self.assertTrue(res_warning)

    # def test_12(self):
    #     """Prueba comprobación cálculos"""
    #     er = er_new_format.df_csv_intermedio
    #     cr = cr_new_format.df_csv_intermedio

