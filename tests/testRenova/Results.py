from dataclasses import dataclass, field
from configobj import ConfigObj
from Renova.RenovaCSV import RenovaCSVCreation as RC
import pandas as pd
import os

## Crear archivo config con las rutas para los datos 11-2021 y 12-2021, y los resultados esperados 11-2021 y 12-2021 /listo
#### Test para 2 periodos, menor o igual a 11-2021 y mayor a 11-2021  
 
## obtener dataframes de la ejecución del programa /listo
## obtener dataframes de resultados esperados /listo

############ dataframes necesarios:
## dataframe CSV intermedio
## dataframe CSV final
## dict dataframes warnings: {df_dict_centrales:DF, 
#                             df_dict_generadores:DF,
#                             df_dict_clientes:DF, 
#                             df_BERNC:DF, 
#                             df_asignacion:DF, 
#                             df_decimales_no_traspasados:DF}
## dataframes decimales sobrantes


def empty_frame():
    return pd.DataFrame({})

@dataclass
class Results():
    date:str = field(init=True)
    df_csv_intermedio:pd.DataFrame = field(default_factory=empty_frame)
    df_csv_final:pd.DataFrame = field(default_factory=empty_frame)
    df_decimals_next_month:pd.DataFrame = field(default_factory=empty_frame)
    dict_df_warnings:dict = field(default_factory=dict)

    def __post_init__(self):
        cfg_test_path = os.path.join('tests', 'testRenova', 'config_test.cfg')
        self.cfg = ConfigObj(cfg_test_path)


class ExpectedResults(Results):
    """Clase que permite obtener los resultados esperados"""
   
    def get_results(self):
        """"Leer resultados esperados"""
        
        paths_date = self.cfg['PATHS']['RESULTS'][self.date]
        prefix = self.cfg['PATHS']['RESULTS']['output_path']
        paths_date = {k: os.path.join(prefix, v) for k, v in paths_date.items()}
        
        self.df_csv_final = pd.read_csv(paths_date['CSV_final']) 
        self.df_csv_intermedio = pd.read_csv(paths_date['CSV_intermedio'])
        warnings_sheet = ['dicc_clientes',
                          'dicc_generadores', 
                          'dicc_centrales', 
                          'balance_ERNC', 
                          'asignación', 
                          'decimales no traspasados',
                          'consumo con RUT generador']

        for sheet in warnings_sheet:
            self.dict_df_warnings[sheet] = pd.read_excel(paths_date['warnings'],
                                                         sheet_name=sheet,
                                                         index_col=0)
                                                         
        self.df_decimals_next_month = pd.read_excel(paths_date['decimales_sobrantes'],
                                                    index_col=0)


class CalculatedResults(Results):
    """Clase que permite obtener los resultados de la ejecución del programa"""
    
    def get_results(self):
        """"Ejecuta el programa y obtiene resultados"""
        
        paths_date = self.cfg['PATHS']['DATA'][self.date]
        prefix = self.cfg['PATHS']['DATA']['input_path']
        paths_date = {k: os.path.join(prefix, v) for k, v in paths_date.items()}
        print(self.date)
        h = RC(date=self.date, is_test=True,
               cfg='config.cfg', **paths_date)
        if self.date == '2021-11':
            h._RenovaCSVCreation__cfg['VALORIZADO']['origin'] = 'MDB'
        if self.date == '2021-12':
            h._RenovaCSVCreation__cfg['VALORIZADO']['origin'] = 'CSV'
        print(type(h._RenovaCSVCreation__cfg))
        h.run_csv_creation()
        self.df_csv_final = h.test_df_csv_final
        self.df_csv_intermedio = h.test_df_csv_intermedio
        self.df_csv_intermedio.reset_index(drop=True, inplace=True)
        self.dict_df_warnings = h.test_dict_df_warnings
        self.df_decimals_next_month = h.test_df_decimals_next_month


class CompareFrames():
    def compare(f1, f2, index_values:list, index_col=None, num_cols:list=[],
                str_cols:list=[]):
        res = True
        f1a = f1.copy(deep=True)
        f2a = f2.copy(deep=True)

        try:
            if index_col is not None:
                f1a.reset_index(drop=True, inplace=True)
                f1a.reset_index(drop=True, inplace=True)
                f1a.index = f1[index_col]
                f2a.index = f1[index_col]
            
            df1 = f1a.loc[index_values][num_cols + str_cols]
            df2 = f2a.loc[index_values][num_cols + str_cols]

            for col in num_cols:
                col1 = df1[col].astype(float)
                col2 = df2[col].astype(float)
                foo = abs(col1 - col2 ) < 0.2
                res = res and foo.all()

            for col in str_cols:
                col1 = df1[col].astype(str)
                col2 = df2[col].astype(str)
                res = res and col1.equals(col2)
        
        except:
            res = False

        finally:
            return res