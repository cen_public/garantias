import unittest
from SharedCode.PPA.Rule import Rule
from SharedCode.exceptions import PowerPurchaseError
import pandas as pd
import numpy as np


class TestRule(unittest.TestCase):

    def test_new_rule(self):
        original_format = '0.3 * ret["CGELADRCGEA"]'
        year = 2022
        key_column = 'clave_dif'
        rule = Rule(original_format=original_format,
                    year=year,
                    key_column=key_column)
    
        self.assertEqual(original_format, rule.original_format)
        self.assertEqual(year, rule.year)
        self.assertEqual(key_column, rule.key_column)
    
    # def test_test_rule(self):
    #     rule_str = '0.3 * ret[ret["clave_dif"] == "CGELADRCGEA"][["value_MWh", "PxQ"]]'
    #     rule = Rule()
    #     rule.key_column = 'clave_dif'
    #     rule.value_column = 'PxQ'
    #     self.assertTrue(rule._test_rule(rule=rule_str, key_='CGELADRCGEA',
    #                     iny_or_ret='ret'))
    #     # bad rule
    #     rule_str = '0.3 * ret[ret["clave_dif"] == "CGELADRCGEA"]'
    #     self.assertFalse(rule._test_rule(rule=rule_str, key_='CGELADRCGEA',
    #                      iny_or_ret='ret'))

    def test_split_rule(self):
        d1 = {0: '0.3 ', 1: ' ret["CGELADRCGEA"]'}
        d2 = {0: '0.3', 1: 'ret["CGELADRCGEA"]'}
        d3 = {0: 'iny["CGELADRCGEA"] ', 1: ' 0.3'}
        d_check = {'0.3 * ret["CGELADRCGEA"]': d1,
                   '0.3*ret["CGELADRCGEA"]': d2, 
                   'iny["CGELADRCGEA"] / 0.3': d3}
        
        year = 2022
        key_column = 'clave_dif'
        for original_f, split_f in d_check.items():
            rule = Rule(original_format=original_f,
                        year=year,
                        key_column=key_column)
            
            self.assertEqual(rule._split_rule(), split_f)
            

    def test_standard_rule(self):
         # For testing purposes: test without blanks
        d_check = {'0.3*ret["CGELADRCGEA"]':
                   ['0.3*ret[ret["clave_dif"] == "CGELADRCGEA"][["value_MWh", "PxQ"]]'],
                   'iny["CGELADRCGEA"]*0.3':
                   ['iny[iny["clave_dif"] == "CGELADRCGEA"][["value_MWh", "PxQ"]]*0.3'],
                   'iny["G_CURUROS"]+iny["C_CURUROS"]-ret["DGSA"]':
                   ['iny[iny["clave_dif"] == "G_CURUROS"][["value_MWh", "PxQ"]]+iny[iny["clave_dif"] == "C_CURUROS"][["value_MWh", "PxQ"]]-ret[ret["clave_dif"] == "DGSA"][["value_MWh", "PxQ"]]']}
        
        key_column = 'clave_dif'
        year= 2022
        self._internal_test(d_check=d_check, key_column='clave_dif', year=year)    

        # Bad Rule
        rule =  Rule(original_format='0.3 * ret["CGELADRCGEA]',
                     year=year, key_column=key_column)
        with self.assertRaises(PowerPurchaseError):
            rule.to_pandas_format()
    
    def test_min_max_rule(self):
        d_check = {'max(0,iny["22040700000IF"]+iny["22040700000RF"])':
                   ['np.maximum(0,iny[iny["clave_dif"] == "22040700000IF"]["value_MWh"]+iny[iny["clave_dif"] == "22040700000RF"]["value_MWh"])',
                    'foo.to_frame().join(other=df_bus["price_value"],how="left",on=["date_time","busbar"])',
                    'foo["value_MWh"]*foo["price_value"]'],
                   'min(ret["WATT_BB"]-iny["G_CUMBRES"],0)':
                    ['np.minimum(ret[ret["clave_dif"] == "WATT_BB"]["value_MWh"]-iny[iny["clave_dif"] == "G_CUMBRES"]["value_MWh"],0)',
                    'foo.to_frame().join(other=df_bus["price_value"],how="left",on=["date_time","busbar"])',
                    'foo["value_MWh"]*foo["price_value"]']}
        
        key_column = 'clave_dif'
        year= 2022
        self._internal_test(d_check=d_check, key_column='clave_dif', year=year)   
    
    def test_monthly_factors_rule(self):
        r1 = '{"clave":iny["G_CTRL_EO_CABO_LEONES_I"],"factores":["0.2576,0.2576,0.2576,0.2576,0.2576,0.2576,0.255,0.255,0.255,0.255,0.255,0.255"]}'
        # factors: chose "July, i.e, factores[6]
        d_check = {r1: '0.255*iny[iny["clave_dif"] == "G_CTRL_EO_CABO_LEONES_I"][["value_MWh", "PxQ"]]'}

        year = 2022
        key_column = 'clave_dif'
        
        for original_f, pandas_f in d_check.items():
            rule = Rule(original_format=original_f,
                        year=year,
                        key_column=key_column)
            rule.month = 6
            rule.to_pandas_format()
            self.assertEqual(rule.pandas_format, pandas_f)
    
    def _internal_test(self, d_check:dict, key_column:str, year:int):
        # See df_validation at the beginnign Rule class
        expected_values = 0.3 * np.array([40]) 
        
        for original_f, pandas_f in d_check.items():
            rule = Rule(original_format=original_f,
                        year=year,
                        key_column=key_column)
            rule.to_pandas_format()
            self.assertEqual(rule.pandas_format, pandas_f)
            # self.assertEqual(rule.test_eval.values, expected_values) 
