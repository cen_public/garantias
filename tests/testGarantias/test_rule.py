import unittest
from PPA.Rule import Rule
from SharedCode.exceptions import PowerPurchaseError

class TestRule(unittest.TestCase):
    maxDiff = None

    def test_new_rule(self):
        original_format = '0.3 * ret["CGELADRCGEA"]'
        year = 2022
        key_column = 'clave_dif'
        rule = Rule(original_format=original_format,
                    year=year,
                    key_column=key_column)
    
        self.assertEqual(original_format, rule.original_format)
        self.assertEqual(year, rule.year)
        self.assertEqual(key_column, rule.key_column)
    
    def test_split_rule(self):
        d1 = {0: '0.3 ', 1: ' ret["CGELADRCGEA"]'}
        d2 = {0: '0.3', 1: 'ret["CGELADRCGEA"]'}
        d3 = {0: 'iny["CGELADRCGEA"] ', 1: ' 0.3'}
        d_check = {'0.3 * ret["CGELADRCGEA"]': d1,
                   '0.3*ret["CGELADRCGEA"]': d2, 
                   'iny["CGELADRCGEA"] / 0.3': d3}
        
        year = 2022
        key_column = 'clave_dif'
        for original_f, split_f in d_check.items():
            rule = Rule(original_format=original_f,
                        year=year,
                        key_column=key_column)
            
            self.assertEqual(rule._split_rule(), split_f)
            

    def test_standard_rule(self):
         # For testing purposes: test without blanks
        d_check = {'0.3*ret["CGELADRCGEA"]':
                   [[['CGELADRCGEA',
                      'ret[ret["clave_dif"] == "CGELADRCGEA"]["busbar"]',
                      'ret[ret["clave_dif"] == "CGELADRCGEA"]["price_value"]',
                      'ret[ret["clave_dif"] == "CGELADRCGEA"]["value_MWh"]',
                      'ret[ret["clave_dif"] == "CGELADRCGEA"]["price_value"]*ret[ret["clave_dif"] == "CGELADRCGEA"]["value_MWh"]']],
                    '0.3*d_pxq[0]'],
                     
                   'iny["CGELADRCGEA"]*0.3':
                   [[['CGELADRCGEA',
                      'iny[iny["clave_dif"] == "CGELADRCGEA"]["busbar"]',
                      'iny[iny["clave_dif"] == "CGELADRCGEA"]["price_value"]',
                      'iny[iny["clave_dif"] == "CGELADRCGEA"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "CGELADRCGEA"]["price_value"]*iny[iny["clave_dif"] == "CGELADRCGEA"]["value_MWh"]']],
                    'd_pxq[0]*0.3'],
                      
                      '0.3*iny["G_CURUROS"]+iny["C_CURUROS"]-ret["DGSA"]/2':
                   [[['G_CURUROS',
                      'iny[iny["clave_dif"] == "G_CURUROS"]["busbar"]',
                      'iny[iny["clave_dif"] == "G_CURUROS"]["price_value"]',
                      'iny[iny["clave_dif"] == "G_CURUROS"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "G_CURUROS"]["price_value"]*iny[iny["clave_dif"] == "G_CURUROS"]["value_MWh"]'],
                     ['C_CURUROS',
                      'iny[iny["clave_dif"] == "C_CURUROS"]["busbar"]',
                      'iny[iny["clave_dif"] == "C_CURUROS"]["price_value"]',
                      'iny[iny["clave_dif"] == "C_CURUROS"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "C_CURUROS"]["price_value"]*iny[iny["clave_dif"] == "C_CURUROS"]["value_MWh"]'],
                     ['DGSA',
                      'ret[ret["clave_dif"] == "DGSA"]["busbar"]',
                      'ret[ret["clave_dif"] == "DGSA"]["price_value"]',
                      'ret[ret["clave_dif"] == "DGSA"]["value_MWh"]',
                      'ret[ret["clave_dif"] == "DGSA"]["price_value"]*ret[ret["clave_dif"] == "DGSA"]["value_MWh"]']], 
                    '0.3*d_pxq[0]+d_pxq[1]-d_pxq[2]/2']}
        
        key_column = 'clave_dif'
        year= 2022
        self._internal_test(d_check=d_check, key_column='clave_dif', year=year)    

        # Bad Rule
        rule =  Rule(original_format='0.3 * ret["CGELADRCGEA]',
                     year=year, key_column=key_column)
        with self.assertRaises(PowerPurchaseError):
            rule.to_pandas_format()
    
    def test_min_max_rule(self):
        d_check = {'max(0,iny["22040700000IF"]+iny["22040700000RF"])':
                   [[['22040700000IF',
                      'iny[iny["clave_dif"] == "22040700000IF"]["busbar"]',
                      'iny[iny["clave_dif"] == "22040700000IF"]["price_value"]',
                      'iny[iny["clave_dif"] == "22040700000IF"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "22040700000IF"]["price_value"]*iny[iny["clave_dif"] == "22040700000IF"]["value_MWh"]'],
                    ['22040700000RF',
                      'iny[iny["clave_dif"] == "22040700000RF"]["busbar"]',
                      'iny[iny["clave_dif"] == "22040700000RF"]["price_value"]',
                      'iny[iny["clave_dif"] == "22040700000RF"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "22040700000RF"]["price_value"]*iny[iny["clave_dif"] == "22040700000RF"]["value_MWh"]']],
                   'np.maximum(0,d_mwh[0]+d_mwh[1])*(d_price[0]+d_price[1]).mean()'],
                   'min(ret["WATT_BB"]-iny["G_CUMBRES"],0)':
                   [[['WATT_BB',
                      'ret[ret["clave_dif"] == "WATT_BB"]["busbar"]',
                      'ret[ret["clave_dif"] == "WATT_BB"]["price_value"]',
                      'ret[ret["clave_dif"] == "WATT_BB"]["value_MWh"]',
                      'ret[ret["clave_dif"] == "WATT_BB"]["price_value"]*ret[ret["clave_dif"] == "WATT_BB"]["value_MWh"]'],
                     ['G_CUMBRES',
                      'iny[iny["clave_dif"] == "G_CUMBRES"]["busbar"]',
                      'iny[iny["clave_dif"] == "G_CUMBRES"]["price_value"]',
                      'iny[iny["clave_dif"] == "G_CUMBRES"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "G_CUMBRES"]["price_value"]*iny[iny["clave_dif"] == "G_CUMBRES"]["value_MWh"]']],
                    'np.minimum(d_mwh[0]-d_mwh[1],0)*(d_price[0]+d_price[1]).mean()']}

        key_column = 'clave_dif'
        year= 2022
        self._internal_test(d_check=d_check, key_column='clave_dif', year=year)   
    
    def test_monthly_factors_rule(self):
        r1 = ('{"clave":iny["G_CTRL_EO_CABO_LEONES_I"],"factores":['
               '"0.2576,0.2576,0.2576,0.2576,0.2576,0.2576,'
               '0.255,0.255,0.255,0.255,0.255,0.255"]}')
        # factors: chose "July, i.e, factores[6]
        d_check = {r1:
                   [[['G_CTRL_EO_CABO_LEONES_I',
                      'iny[iny["clave_dif"] == "G_CTRL_EO_CABO_LEONES_I"]["busbar"]',
                      'iny[iny["clave_dif"] == "G_CTRL_EO_CABO_LEONES_I"]["price_value"]',
                      'iny[iny["clave_dif"] == "G_CTRL_EO_CABO_LEONES_I"]["value_MWh"]',
                      'iny[iny["clave_dif"] == "G_CTRL_EO_CABO_LEONES_I"]["price_value"]*iny[iny["clave_dif"] == "G_CTRL_EO_CABO_LEONES_I"]["value_MWh"]']],  
                      '0.255*d_pxq[0]']}

        year = 2022
        key_column = 'clave_dif'
        
        for original_f, pandas_f in d_check.items():
            rule = Rule(original_format=original_f,
                        year=year,
                        key_column=key_column)
            rule.month = 6
            rule.to_pandas_format()
            self.assertEqual(rule.pandas_format, pandas_f)
       
    def _internal_test(self, d_check:dict, key_column:str, year:int):
      for original_f, pandas_f in d_check.items():
            rule = Rule(original_format=original_f,
                        year=year,
                        key_column=key_column)
            rule.to_pandas_format()
            self.assertEqual(rule.pandas_format, pandas_f)
  
