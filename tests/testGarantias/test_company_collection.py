import unittest
from SharedCode.PPA.PPA_parties import PPA_parties
from SharedCode.PPA.PPA import PPA
from SharedCode.PPA.Company import Company
from SharedCode.PPA.CompanyPPACollection import CompanyPPACollection
from SharedCode.exceptions import PowerPurchaseError

class TestCompanyCollection(unittest.TestCase):
    kwargs_parties_1 = {'declarant': 'endesa',
                        'counterpart': None,
                        'buyer': 'codelco',
                        'seller': 'endesa'}
    kwargs_parties_2 = {'declarant': 'codelco',
                        'counterpart': None,
                        'buyer': 'codelco',
                        'seller': 'endesa'}
                            
    def test_identify_counterpart(self):
        parties_1 = PPA_parties(**self.kwargs_parties_1)
        parties_2 = PPA_parties(**self.kwargs_parties_2)

        counterp_1 = CompanyPPACollection()._identify_counterpart(parties_1)
        counterp_2 = CompanyPPACollection()._identify_counterpart(parties_2)
        self.assertEqual(counterp_1, 'codelco')
        self.assertEqual(counterp_2, 'endesa')

    def test_is_declarant_ok(self):
        parties_1 = PPA_parties(**self.kwargs_parties_1)
        ppa = PPA(id='id_1', parties=parties_1)
        self.assertIsNone(
            CompanyPPACollection()._is_declarant_ok('endesa', ppa))
        
        with self.assertRaises(PowerPurchaseError):
            CompanyPPACollection()._is_declarant_ok('foo', ppa)
        
    def test_does_counterpart_exist(self):    
        parties_1 = PPA_parties(**self.kwargs_parties_1)
        parties_2 = PPA_parties(**self.kwargs_parties_2)

        ppa_1 = PPA(id='id_1', parties=parties_1)
                
        # self.assertEqual(counterpart, 'codelco')


    # def test_add_ppa(self):
    #     kwargs_parties = {'declarant': 'endesa',
    #                       'counterpart': 'codelco',
    #                       'buyer': 'codelco',
    #                       'seller': 'endesa'}
    #     parties = PPA_parties(**kwargs_parties)

    #     kwargs = {'id': 'complex@id__123',
    #               'parties': parties,
    #               'rule': '{"key": string en forma de diccionario}'}
    #     self.assertIsInstance(PPA(**kwargs), PPA)
        