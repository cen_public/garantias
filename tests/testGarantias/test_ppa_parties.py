import unittest
from SharedCode.PPA.PPA_parties import PPA_parties
from SharedCode.exceptions import PowerPurchaseError

class TestPPAParties(unittest.TestCase):

    def test_create_ppa_parties(self):
        kwargs = {'declarant': 'endesa',
                  'counterpart': 'codelco',
                  'buyer': 'codelco',
                  'seller': 'endesa'}
        self.assertIsInstance(PPA_parties(**kwargs), PPA_parties)
        
    def test_create_ppa_parties_wrong_field(self):
        kwargs = {'declarant': 'endesa',
                  'counterpart': 'codelco',
                  'comprador': 'codelco',
                  'seller': 'endesa'}
        with self.assertRaises(TypeError):
            foo = PPA_parties(**kwargs)
    
    def test_is_mirror_equal_objects(self):
        kwargs = {'declarant': 'endesa',
                  'counterpart': 'codelco',
                  'buyer': 'codelco',
                  'seller': 'endesa'}
        obj_1 = PPA_parties(**kwargs)
        obj_2 = PPA_parties(**kwargs)
        with self.assertRaises(PowerPurchaseError):
            obj_1.is_mirror_party(other=obj_2)

    def test_is_mirror_non_mirror_objects1(self):
        kwargs1 = {'declarant': 'endesa',
                   'counterpart': 'codelco',
                   'buyer': 'endesa',
                   'seller': 'codelco'}

        kwargs2 = {'declarant': 'codelco',
                   'counterpart': 'endesa',
                   'buyer': 'codelco',
                   'seller': 'endesa'}

        obj_1 = PPA_parties(**kwargs1)
        obj_2 = PPA_parties(**kwargs2)
        with self.assertRaises(PowerPurchaseError):
            obj_1.is_mirror_party(other=obj_2)
    
    def test_is_mirror_non_mirror_objects2(self):
        kwargs1 = {'declarant': 'endesa',
                   'counterpart': 'codelco',
                   'buyer': 'codelco',
                   'seller': 'endesa'}

        kwargs2 = {'declarant': 'codelco',
                   'counterpart': 'endesa',
                   'buyer': 'endesa',
                   'seller': 'codelco'}

        obj_1 = PPA_parties(**kwargs1)
        obj_2 = PPA_parties(**kwargs2)
        with self.assertRaises(PowerPurchaseError):
            obj_1.is_mirror_party(other=obj_2)
    
    def test_is_mirror_ok(self):
        kwargs1 = {'declarant': 'endesa',
                   'counterpart': 'codelco',
                   'buyer': 'codelco',
                   'seller': 'endesa'}

        kwargs2 = {'declarant': 'codelco',
                   'counterpart': 'endesa',
                   'buyer': 'codelco',
                   'seller': 'endesa'}

        obj_1 = PPA_parties(**kwargs1)
        obj_2 = PPA_parties(**kwargs2)
        obj_1.is_mirror_party(other=obj_2) # nothing raises
