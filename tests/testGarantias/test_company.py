import unittest
from SharedCode.PPA.PPA_parties import PPA_parties
from SharedCode.PPA.PPA import PPA
from SharedCode.PPA.Company import Company
from SharedCode.exceptions import PowerPurchaseError

class TestCompany(unittest.TestCase):

    def test_create_company(self):
        self.assertIsInstance(Company(name='endesa'), Company)
        
    def test_create_ppa_wrong_field(self):
        with self.assertRaises(TypeError):
            Company(name='endesa', wrong_field = 'foo')

    def test_add_ppa(self):
        kwargs_parties = {'declarant': 'endesa',
                          'counterpart': 'codelco',
                          'buyer': 'codelco',
                          'seller': 'endesa'}
        ppa_parties = PPA_parties(**kwargs_parties)

        kwargs_ppa = {'id': 'complex@id__123',
                      'parties': ppa_parties,
                      'rule': '{"key": string en forma de diccionario}'}
        ppa = PPA(**kwargs_ppa)
        co = Company(name='endesa')
        co.add_ppa(ppa)
        d1 = {kwargs_ppa['id']: ppa}
        d2 = co.ppa_dict
        self.assertDictEqual(d1=d1, d2=d2)

        # add a similar ppa to collection
        co.add_ppa(ppa)
        d1 = {}
        d2 = co.ppa_dict
        self.assertDictEqual(d1=d1, d2=d2)


