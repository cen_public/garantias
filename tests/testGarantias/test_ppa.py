import unittest
from SharedCode.PPA.PPA_parties import PPA_parties
from SharedCode.PPA.PPA import PPA
from SharedCode.exceptions import PowerPurchaseError

class TestPPA(unittest.TestCase):

    def test_create_ppa(self):
        kwargs_parties = {'declarant': 'endesa',
                          'counterpart': 'codelco',
                          'buyer': 'codelco',
                          'seller': 'endesa'}
        parties = PPA_parties(**kwargs_parties)

        kwargs = {'id': 'complex@id__123',
                  'parties': parties,
                  'rule': '{"key": string en forma de diccionario}'}
        self.assertIsInstance(PPA(**kwargs), PPA)
        
    def test_create_ppa_wrong_field(self):
        kwargs = {'id': 'complex@id__123',
                  'partiesss': None,
                  'rule': '{"key": string en forma de diccionario}'}

        with self.assertRaises(TypeError):
            foo = PPA(**kwargs)
