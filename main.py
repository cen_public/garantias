import pandas as pd
import numpy as np
import os

def main(path1: str, path2:str, path3:str):
    def precheck_folders(folders:dict):
        """ folders items:
            key: some id for the hidrology
            folder: path with the input files associated to the hidrology """
        results = {}
        n_folders = len(folders)
        for key, folder in folders.items():
            in_files = {'cmg': None, 'iny': None, 'ret': None}
            folder_content = os.scandir(folder)
            files = (entry for entry in folder_content if entry.is_file())
            
            check_counter = 0
            for file in files:
                print(file.name)
                filename = file.name.lower()
                if filename.find('cmg') > 0 and  file.find('.csv') > 0:
                    check_counter += 1
                    in_files.update({'cmg': filename})
                if filename.find('iny') > 0 and  file.find('.csv') > 0:
                    check_counter += 1
                    in_files.update({'iny': filename})
                if filename.find('ret') > 0 and  file.find('.csv') > 0:
                    check_counter += 1
                    in_files.update({'ret': filename})
            
            if check_counter == 3:
                print(f'Carpeta ok: {folder}')
            else:
                print(f'Carpeta con problemas: {folder}')
            
            results.update({'input_files': {key: in_files}})

        status = True if check_counter == 3 * n_folders else False
        return results, input_files
    
    def set_input_files():
        pass

    input_folders = {1: path1}
    n_hidro = len(input_folders)

    boo = {'pq_iny': None, 'pq_ret': None}
    out_df = {i: boo.copy for i in range(1, n_hidro + 1)}
    temp_df = {i: boo.copy for i in range(1, n_hidro + 1)}

    # Precheck
    status, input_files = precheck_folders(input_folders)
    # Set input_files dictionary

    # PxQ
    for k, v in input_files.items():
        df_cmg = pd.read_csv(filepath=v['cmg'])
        df_iny = pd.read_csv(filepath=v['iny'])
        df_ret = pd.read_csv(filepath=v['ret'])

        temp_df[k]['cmg'] = df_cmg
        temp_df[k]['iny'] = df_iny
        temp_df[k]['ret'] = df_ret
    
    return temp_df