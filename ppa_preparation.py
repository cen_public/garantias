import os, datetime, gc, pickle, logging
import pandas as pd
from configobj import ConfigObj
from PPA.FixedVectorsFrame import FixedVectorsFrame
from SharedCode.LoadEnv import load_env
from SharedCode.Database.PickleZip import PickleZip
from dateutil.relativedelta import relativedelta
import time

def read_dictionary(file:str, sheet_name:str, cols:list):
    if len(cols) != 2:
        raise ValueError('dimension de col es distinta de 2')
    else:
        df = pd.read_excel(io=file, sheet_name=sheet_name)
        df.index = df[cols[0]].astype(str)
        return df[cols[1]].astype(str).to_dict()

def create_df_ppa(forms_path:str):
    # xlsx files iterator
    iter_xlsx_files = os.scandir(forms_path)
    xlsx_files = (entry for entry in iter_xlsx_files
                    if entry.is_file() and entry.name[-5:] == '.xlsx')

    df_bs = pd.DataFrame({})
    # Iter over xlsx files    
    for xlsx_f in xlsx_files:
        print(f'Revisando {xlsx_f.name}')
        df_a = df_from_form(file=xlsx_f, sheet_name='Compraventas')  # CEN form, sheet "Compraventas"
        df_b = df_from_form(file=xlsx_f, sheet_name='Compraventas Cliente regulado')  # CEN form, sheet "Compraventas regulados"
        df_bs = pd.concat([df_bs, df_a, df_b])
    
    df_bs.to_excel('df_ppa.xlsx', index=False)
    return df_bs

def df_from_form(file:str, sheet_name:str):
    df = pd.DataFrame({})
    try:
        df = pd.read_excel(io=file, sheet_name=sheet_name)
    except Exception as e:
        print(f'{file}, hoja {sheet_name} no pude ser leido, {e}')
    else:
        declarant = df.iloc[1, 1]
        df = pd.read_excel(io=file, sheet_name=sheet_name,
                            skiprows=6, dtype=str)
        foo = pd.notna(df['[ID Plataforma Contratos]']) & (pd.notna(df['[Id_Contrato FIFC]']))
        df = df[foo]
        df['declarant'] = declarant
        df.drop(columns=['Rut', 'Rut.1', 'Razón Social',
                            'Razón Social.1', '[Nombre Balance]'], inplace=True)
        df.rename(columns={'Unnamed: 12': 'rule',
                           'Nombre Balance ': 'seller',
                           'Nombre Balance .1': 'buyer',
                           '[Barra]': 'busbar_ivt',
                           '[Clave de transferencias]': 'clave'}, inplace=True)
        
        # solucionar las fechas inicio y fin con distinto formato
        try:
            df.rename(columns={'fecha_inicio': 'Initial_Date'}, inplace=True)
        except:
            pass
        try:
            df.rename(columns={'Fecha_Inicio': 'Initial_Date'}, inplace=True)
        except:
            pass
        try:
            df.rename(columns={'fecha_fin': 'Final_Date'}, inplace=True)
        except:
            pass
        try:
            df.rename(columns={'Fecha_Fin': 'Final_Date'}, inplace=True)
        except:
            pass
            
            
        
        # Create a ppa_id
        ppa_id_cols = ['[ID Plataforma Contratos]',
                       '[Id_Contrato FIFC]', 'seller', 'buyer', 'clave']
        df['ppa_id'] = df[ppa_id_cols].astype(str).agg('_&_'.join, axis=1)

        # Create fixed_id
        fixed_id_cols = ['declarant', '[ID Plataforma Contratos]',
                         '[Id_Contrato FIFC]']
        df['fixed_id'] = df[fixed_id_cols].agg('_&_'.join, axis=1)

        df.reset_index()
    return df

def main(forms_path:str=None, fixed_path:str=None, ret_path:str=None,
         iny_path:str=None, zip_fix_path:str=None, out_path=None):
    ###################################################
    # For develpoing purposes only
    if forms_path is None:
        forms_path = 'Input_test//Garantias//compraventas//formularios'
    if fixed_path is None:
        fixed_path = 'Input_test//Garantias//compraventas//vectores_fijos'
    if ret_path is None:
        ret_path = 'pkl//output//ret//PxQ'
    if iny_path is None:
        iny_path = 'pkl//output//generators//PxQ'
    if zip_fix_path is None:
        zip_fix_path = 'pkl//intermediate//ppa//fixed'
    if out_path is None:
        out_path = 'pkl//output//ppa'


    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')

   
    # Business' rules
    # 1.- Read all forms and fixed vectors. Create a DataFrame with the info
    #     and a PPASimulation object
    
    # for developing purposes
    df_ppa = create_df_ppa(forms_path=forms_path)
    df_ppa = pd.read_excel('df_ppa.xlsx')
       
    # Diccionarios de generadores y retiros
    xlsx_dict_gen = 'Input_test//Garantias//diccionarios//Centrales_V05.xlsx'
    
    xlsx_dict_ret = 'Input_test//Garantias//diccionarios//Retiros_V06.xlsx'
    
    dict_bus_bus = read_dictionary(file=xlsx_dict_ret,
                                   sheet_name='Barras_IVT-Plexos',
                                   cols=['BARRA_IVT', 'BARRA_PLEXOS'])

    print('main: leyendo informacion de barras Plexos')
    pkl_plx_busbars = 'pkl//input//plx_busbars.zip'
    plx_busbars = PickleZip.load(zip_file=pkl_plx_busbars)

    # Prepare a copy of df_ppa with fixed vector rules only. Also, check if
    # there are duplicated values
    foo = df_ppa['rule'].str.contains('_fijo')
    df_ppa_fixed = df_ppa[foo].copy(deep=True)
    df_ppa_fixed.set_index(keys='fixed_id', drop=True, inplace=True)

    # get duplicates
    idx = df_ppa_fixed.index.duplicated(keep='first')
    duplicated_idx = df_ppa_fixed.index[idx]
    
    # drop duplicates
    idx = df_ppa_fixed.index.drop_duplicates(keep=False)
    df_ppa_fixed = df_ppa_fixed.loc[idx]

    fixed = FixedVectorsFrame(fixed_path=fixed_path, zip_path=zip_fix_path,
                              df_form=df_ppa_fixed, plx_busbars=plx_busbars,
                              dict_bus=dict_bus_bus, year=2022)
    
    # prepare fixed vectors by hidrology
    hidrologies = ['Sample 1', 'Sample 3', 'Sample 6']
    fixed.set_df_from_files(hidrologies=hidrologies)
    
    # Report those row with Nan in price_value
    foo = f'{hidrologies[0]}___fixed_vectors_01.zip'
    file_to_check = os.path.join(zip_fix_path,foo)
    df_check = PickleZip.load(file_to_check)
    df_check = df_check[df_check['price_value'].isna()]
    print('Contratos con problemas de Nan en sus costos marginales')
    print(df_check['ppa_id'].unique())
    
    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo total de ejecucion: {t_delta}')
 

