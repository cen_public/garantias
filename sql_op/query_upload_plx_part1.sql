use garantias_renova;
DROP TABLE IF EXISTS table_name;
CREATE TABLE IF NOT EXISTS table_name (
	model_name CHAR(40),
    parent_class_id CHAR(4),
    parent_id CHAR(7),
    parent_name CHAR(40),
    collection_id CHAR(20),
    collection_name CHAR(40),
    category_id CHAR(7),
    category_name CHAR(40),
    category_rank CHAR(40),
    child_id CHAR(7),
    child_name CHAR(40),
    property_id CHAR(40),
    property_name CHAR(40),
    band_id CHAR(40),
    timeslice_name CHAR(40),
    sample_name CHAR(40),
    _date CHAR(40),
    period_id CHAR(7),
    value NVARCHAR(40),
    unit_id CHAR(10),
    unit_name CHAR(10),
    interval_id CHAR(40),
    phase_id CHAR(40),
    phase_name CHAR(40)
    );