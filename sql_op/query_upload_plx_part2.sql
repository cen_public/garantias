ALTER TABLE table_name 
	DROP COLUMN model_name,
    DROP COLUMN parent_class_id,
    DROP COLUMN parent_id,
    DROP COLUMN parent_name,
    DROP COLUMN collection_id,
    DROP COLUMN collection_name,
    DROP COLUMN category_id,
    DROP COLUMN category_name,
    DROP COLUMN category_rank,
    DROP COLUMN child_id,
    DROP COLUMN property_id,
    DROP COLUMN property_name,
    DROP COLUMN band_id,
    DROP COLUMN period_id,
    DROP COLUMN interval_id,
    DROP COLUMN timeslice_name,
    DROP COLUMN unit_id,
    DROP COLUMN phase_id,
    DROP COLUMN phase_name;

ALTER TABLE table_name
	RENAME COLUMN sample_name TO hidrology,
	RENAME COLUMN child_name TO name,
    RENAME COLUMN _date TO date_time,
    RENAME COLUMN unit_name TO unit;

UPDATE table_name set hidrology = replace(hidrology, '"', '');
UPDATE table_name set name = replace(name, '"', '');
UPDATE table_name set value = replace(value, '"', '');
UPDATE table_name set value = replace(value, ',', '.');
UPDATE table_name set unit = replace(unit, '"', '');
UPDATE table_name set date_time = replace(date_time, '"', '');

UPDATE table_name SET date_time = STR_TO_DATE(date_time,'%d-%m-%Y %k:%i:%S');
ALTER TABLE table_name MODIFY COLUMN value FLOAT(30);
ALTER TABLE table_name
	DROP COLUMN value1,
    DROP COLUMN value2;
