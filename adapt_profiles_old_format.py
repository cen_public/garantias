from SharedCode.Database.PickleZip import PickleZip
import pandas as pd
import os, gc
import _pickle

base_path = 'pkl//intermediate//new_profiles'


type_ = {'R': 'R',
         'L': 'L',
         'LD': 'L_D'}

months = months = [f"{i:02d}" for i in range(1, 13)]

for kt, vt in type_.items():
    df_oldformat = pd.DataFrame({})
    for mm in months:
        print(f'Iterando en mes {mm}')
        file_month = f'RETIROS_23{mm}.pkl'
        file = os.path.join(base_path, file_month)
        print(f' {file}')
        try:
            df = pd.read_pickle(file)
        except FileNotFoundError:
            df = pd.read_csv(file.replace('.pkl', '.csv'), encoding='latin')
            df['date_time'] = df['date_time'].astype('datetime64')
        except _pickle.UnpicklingError:
            df = pd.read_csv(file.replace('.pkl', '.csv'), encoding='latin')
            df['date_time'] = df['date_time'].astype('datetime64')
        
        df = df[df['type'] == kt]
        df['key'] = df['suministrador'] + "_&_" +  df['clave']
        df.drop(columns=['f1', 'f2', 'f3', 'month', 'avg_MWh'], inplace=True)
        df.sort_values(by=['key', 'date_time'], inplace=True)
        df.reset_index(drop=True, inplace=True)
        print(f"  N claves_suministrador: {len(df['key'].unique())}")

        # Reshaped frame
        df_new = df.pivot(index='key', columns='date_time', values='value_MWh')
            
        # df_olformat
        if len(df_oldformat) == 0: # for the first iter
            df_oldformat.index = df_new.index 

        df_oldformat = df_oldformat.join(other=df_new, how='outer') 

    # Final make up
    df_base = PickleZip.load('pkl//input//RETIROS_2212.zip')

    df_oldformat.sort_index(axis=0, inplace=True)
    cols = df_oldformat.columns.sort_values()
    df_oldformat = df_oldformat[cols]

    # df index
    a = zip(df_base['suministrador_clave'], df_base['suministrador'])
    suministrador_d = {ai[0]: ai[1] for ai in list(a)}

    a = zip(df_base['suministrador_clave'], df_base['retiro'])
    retiro_d = {ai[0]: ai[1] for ai in list(a)}

    a = zip(df_base['suministrador_clave'], df_base['clave'])
    clave_d = {ai[0]: ai[1] for ai in list(a)}

    a = zip(df_base['suministrador_clave'], df_base['barra'])
    barra_d = {ai[0]: ai[1] for ai in list(a)} 

    d_b_plexos = pd.read_excel(io='Input_test//Garantias//diccionarios//20220825_Diccionario_Retiros_V2.xlsx',
                            sheet_name='Barras PLP')
    d_b_plexos.set_index(keys=['BARRA_IVT'], drop=True, inplace=True)
    barraplexos_d = d_b_plexos['BARRA_PLEXOS'].to_dict()

    map_d = {'Suministrador': suministrador_d,
            'Retiro': retiro_d,
            'clave': clave_d,
            'Barra': barra_d}

    df_oldformat_index = pd.DataFrame({}, columns=list(map_d.keys()))
    for k, v in map_d.items():
        df_oldformat_index[k] = df_oldformat.index.map(v).values

    df_oldformat_index['BarraPLP'] = df_oldformat_index['Barra'].map(barraplexos_d)
    df_oldformat_index['Tipo'] = vt

    output_name = os.path.join('Output_test', f'Retiros{vt}')
    df_oldformat.to_csv(f'{output_name}.Data.txt', sep='\t', index=False, header=False, decimal='.')
    df_oldformat_index['Retiro'] = df_oldformat_index['Retiro'].str.replace('\u017d','')
    df_oldformat_index.to_csv(f'{output_name}.txt', sep='\t', index=False, header=True, encoding='latin') 
    del df_oldformat
    del df_oldformat_index
    gc.collect()

# exec(open('adapt_profiles_old_format.py').read())