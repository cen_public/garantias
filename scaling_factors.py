import os, datetime, gc
import pandas as pd
from configobj import ConfigObj
from SharedCode.ValReader import AccessReader
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.LoadEnv import load_env


class Dict(dict):
    def __missing__(self, key):
        return key

def main(mdb_path:str, plexos_load_path:str):
    
    def create_df_plx_lod_lod(csv_path):
        # Creata a DataFrame from Plexos load file
        df = pd.read_csv(csv_path)
        
        # Get type of load using split in "NAME": There are some names with two "_"
        # (the last one should be used)
        bar = df['NAME'].str.split('_', n=2, expand=True)
        if len(bar.columns) == 2:
            df['type'] = bar[1]
        elif len(bar.columns) == 3:
            bar[2].fillna(bar[1], inplace=True)
            df['type'] = bar[2]

        df['PATTERN'] = df['PATTERN'].str.replace('_', ',')
        foo = df['PATTERN'].str.split(',', expand=True)
        foo['type'] = df['type']
        foo.rename(columns={0:"day", 1:"month", 2:"hour"}, inplace=True)
        df.index = pd.MultiIndex.from_frame(foo)
        df.drop(columns=['type'], inplace=True)
        df.sort_index(inplace=True)
        return df
    
    def create_df_plx_lod_esc(csv_path):
        # Creata a DataFrame from Plexos load file
        df = pd.read_csv(csv_path)
        df.rename(columns={'Growth_R': 'R', 'Growth_L': 'L', 'Growth_LD': 'LD'})
        df['date_time'] = df[['YEAR', 'MONTH', 'DAY']].apply(pd.to_datetime)

    def create_df_ret(mdb_path, cfg):
        # Create a DataFrame from MS Access DB
        cfg_db = cfg['GARANTIAS']['RETIROS_DB']
        a_reader = AccessReader(cfg=cfg_db)
        df_ret = a_reader.read_db(db_path=mdb_path)
        
        # Agregar una columna datetime
        cols = df_ret.columns.to_list()
        cols[0] = cols[0].replace('\ufeff', '')  # weird char
        cols[0] = cols[0].replace('Ã±', 'ñ')  # weird char
        df_ret.columns = cols
        df_ret['datetime'] = (pd.to_datetime(df_ret['Clave Año_Mes'], format='%y%m')
                              + (df_ret['Hora Mensual']*60 - 1).astype('timedelta64[m]'))
        
        # Convert datetime to string with the format "%w-%m-%H"
        map_day = Dict()
        map_hour = Dict()
        map_day.update(cfg['GARANTIAS']['PLEXOS_DATE']['DAYS'])
        map_hour.update(cfg['GARANTIAS']['PLEXOS_DATE']['HOURS'])
        foo = df_ret['datetime'].dt.strftime('%d-D%w-%m-H%H')
        foo = foo.str.split('-', expand=True)
        df_ret['day_month'] = foo[0]
        foo[1] = foo[1].map(map_day) #day
        foo[3] = foo[3].map(map_hour) #hour
        foo[4] = df_ret['Tipo'].str.replace('L_D', 'LD') # type
        foo[4] = foo[4].replace('L_S', 'L') # type (dejarlos fuera)
        foo.rename(columns={1:"day", 2:"month", 3:"hour", 4:'type'}, inplace=True)
        df_ret.index = pd.MultiIndex.from_frame(foo[['day', 'month', 'hour', 'type']])    
        df_ret.sort_index(inplace=True)
        return df_ret

    def create_df_factors(mdb_path):
        df_factors = pd.DataFrame({})
        df_factors.index = df_plx_lod_lod.index.unique()
        df_factors.insert(0, 'sum_plx_lod_MW', 0)
        df_factors.insert(0, 'sum_ret_MW', 0)
        df_factors.insert(0, 'factor_1', 0)
        df_factors.insert(0, 'factor_2', 0)
        df_factors.insert(0, 'factor_3', 0)
        df_factors.sort_index(inplace=True)
        
        iter_mdb_files = os.scandir(mdb_path)
        mdb_files = (entry for entry in iter_mdb_files if entry.is_file() and entry.name[-4:] == '.mdb')
            
        for mdb_f in mdb_files:
            print(f'Revisando {mdb_f.name}')
            mdb_abspath= os.path.join(mdb_path, mdb_f.name)
            df_ret = create_df_ret(mdb_path=mdb_abspath, cfg=cfg)
            print(f'Fin lectura de {mdb_f.name}. Comienzo de cálculo de factores')

            for inx in df_ret.index.unique():
                df_factors.at[inx, 'sum_plx_lod_MW'] = df_plx_lod_lod.loc[inx]['VALUE'].sum()
                foo = df_ret.loc[inx].copy(deep=True)
                foo = foo.groupby('day_month').sum()
                df_factors.at[inx, 'sum_ret_MW'] = foo['Medida_kWh'].mean() * -1E-03
            
            df_factors['factor_1'] = df_factors['sum_plx_lod_MW'] / df_factors['sum_ret_MW']
            del df_ret
            gc.collect()
        
        return df_factors

    def write_into_db():
        # Send values to Database    
        obj_db = DBQueryMySQL()
        try:
            obj_db.insert_df(table='factors', df=df_factors, if_exists='append',
                            index=True, index_label=['day', 'month', 'hour', 'type'])
        except:
            print(f'No se pudo insertar el dataframe')
    
    ###################################################
    # For develpoing purposes only
    if mdb_path is None:
        cwd = os.cwd()
        mdb_path = os.path.join(cwd, 'Input_test', 'Garantias', 'retiros')
    
    if plexos_load_path is None:
        cwd = os.getcwd()
        plexos_load_path = os.path.join(plexos_load_path, 'Input_test',
                                        'Garantias', 'plexos', 'load')

        csv_lod_lod = 'load_load.csv'
        csv_lod_esc = 'load_escale.csv'
        csv_path_lod_lod = os.path.join(plexos_load_path, 'Input_test', 'Garantias',
                                        'plexos', 'load', csv_lod_lod)
        csv_path_lod_esc = os.path.join(plexos_load_path, 'Input_test', 'Garantias',
                                        'plexos', 'load', csv_lod_esc)

    ###################################################
    ## Main
    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')
    df_plx_lod_lod = create_df_plx_lod_lod(csv_path=csv_path_lod_lod)
    df_plx_lod_esc = create_df_plx_lod_esc(csv_path=csv_path_lod_esc)
    df_factors = create_df_factors(mdb_path=mdb_path)
    write_into_db()
    ################################################### 
       
    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo de ejecucion, {t_delta}')
    return df_factors, df_plx_lod_lod, df_plx_lod_esc

# idx = pd.IndexSlice
# df.loc[idx[:, '01', 'H1', :]]