import os
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.LoadEnv import load_env
import pandas as pd

def main():
    load_env(filename='local_dev.env')
    drop_tables = True # Pedirlo con confirmacion
    
    # PEDIR CONFIRMACION EXPLICITA EN LA CONSULTA DE DONDE SE ESTÁ TRABAJANDO
    # deploy_str = None
    # drop_tables_str = None  
    # slot = os.environ.get('SLOT', 'None')
    slot = 'local'
    if slot == 'local':
        with open(file='deployment_mysql_local_str.sql', mode='r') as f:
            deploy_str = f.read()
        # with open(file='drop_tables.sql', mode='r') as f:
        #     drop_tables_str = f.read()
    
    obj_mysql = DBQueryMySQL()
    foo = obj_mysql.set_cnxn(username='admin', password='cug8fGAD$$')
    obj_mysql.set_cursor()
    
    if obj_mysql.cursor is not None and deploy_str is not None:
        # obj_mysql.cursor.execute(drop_tables_str) if drop_tables else lambda x:None
        print(deploy_str)
        obj_mysql.cursor.execute(deploy_str)
        obj_mysql.cursor.fetchall()
        obj_mysql.cursor.close()
        obj_mysql.cnxn.close()
    else:
        print('No se pudo hacer el despliegue')

if __name__ == '__main__':
    main()