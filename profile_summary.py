import os, datetime
import pandas as pd
from SharedCode.Database.PickleZip import PickleZip
from calculate_balance import print_msg

input_folder = 'pkl//intermediate//new_profiles'

# Iter over pkl files with profiles
iter_profiles = os.scandir(input_folder)
profiles_files = (entry for entry in iter_profiles
                    if entry.is_file() and entry.name[-4:] == '.pkl')
dict_res = {}

for profile in profiles_files:
    print_msg(f'   Revisando {profile.name}')
    profile_abspath= os.path.join(input_folder, profile.name)
    df = pd.read_pickle(profile_abspath)

    foo = {}
    for type_ in ['R', 'L', 'LD']:
        foo.update({type_: df[df['type'] == type_]['value_MWh'].sum() / 1000})
    
    dict_res.update({profile.name: foo})

df_res = pd.DataFrame.from_dict(dict_res, orient='index')
