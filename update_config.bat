@echo off
setlocal enabledelayedexpansion
rem delayed variable expansion, allowing variables to be expanded at execution time.

set "search=<RENOVAPATH>"
set "replace=%1"
set "inputFile=config_base.cfg"
set "outputFile=config.cfg"

(for /f "tokens=*" %%a in ('type "%inputFile%" ^| findstr /n "^"') do (
    set "line=%%a"
    set "line=!line:*:=!"

    if defined line (
        set "line=!line:%search%=%replace%!"
        echo(!line!
    ) else echo.
)) > "%outputFile%"


set "search=<NAVIGATOR>"
set "replace=%2"
set "inputFile=exec_gr_prod_base.bat"
set "outputFile=exec_gr_prod.bat"

(for /f "tokens=*" %%a in ('type "%inputFile%" ^| findstr /n "^"') do (
    set "line=%%a"
    set "line=!line:*:=!"

    if defined line (
        set "line=!line:%search%=%replace%!"
        echo(!line!
    ) else echo.
)) > "%outputFile%"


endlocal