from json import load
import pickle, gc, os
import pandas as pd
from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.Database.PickleZip import PickleZip
from SharedCode.LoadEnv import load_env
from SharedCode.PlexosReader import PlexosReader
import datetime


def print_msg(text:str):
    now = datetime.datetime.now()
    print(f'{text} {now}')

def load_pickle(pkl_file:str):
    print_msg(f'load_pickle: Leyendo {pkl_file}')
    with open(pkl_file, 'rb') as f:
        varname = pickle.load(f)
    return varname

def dump_pickle(varname, pickle_file):
    with open(file=pickle_file, mode='wb') as f:
        pickle.dump(varname, f)

def read_dictionary(file:str, sheet_name:str):
    return pd.read_excel(io=file, sheet_name=sheet_name)

def calculate_iny(dict_gen_bus:dict, plx_generators:dict, plx_busbars:dict,
                  output_folder:str, hidrologies:list):            
    """ P x Q generators. It dumps a pkl file"""
    dict_gen_bus.index = dict_gen_bus['CENTRAL_PLEXOS'].astype(str)
    map_bus = dict_gen_bus['BARRA_PLEXOS'].to_dict() # {'Central': 'Barra'}
    map_company = dict_gen_bus['EMPRESA'].to_dict() # {'Central': 'Empresa'}

    for hidro in hidrologies:
        print_msg(f'calculate_iny: calculando para {hidro}')
        
        # Prepare df_bus
        df_bus = plx_busbars[hidro]
        df_bus.index = pd.MultiIndex.from_frame(
            df_bus[['name', 'date_time']])
        df_bus.sort_index(inplace=True)

        # Prepare df_gen. If price is <0, replace by zero.
        df_gen = plx_generators[hidro]
        df_gen['busbar'] = df_gen['name'].astype(str).map(map_bus)
        df_gen['company'] = df_gen['name'].astype(str).map(map_company)
        df_gen = df_gen.join(other=df_bus[['price_value']],
                             on=['busbar', 'date_time'], how='left')
        df_gen['PxQ'] = df_gen['price_value'] * df_gen['gen_value']
        # df_gen.rename(columns={'busbar': 'busbars_plexos'}, inplace=True)
        df_gen.rename(columns={'gen_value': 'value_MWh'}, inplace=True)
        inx_pos = df_gen['PxQ'] >= 0 # boolean array
        inx_pos = inx_pos.astype(int) # 1 if ¡hourly_expend.' > 0
        df_gen['PxQ'] = df_gen['PxQ'] * inx_pos
        df_gen['month'] = df_gen['date_time'].dt.strftime('%m') # add month

        # Write pickle file
        print_msg(f'   escribiendo zip file')
        output_file = hidro + '___' + 'generators.zip'
        output_abspath = os.path.join(output_folder, output_file)
        PickleZip.dump(varname=df_gen, zip_file=output_abspath)
        
        # Release memory
        del df_gen
        gc.collect()

def calculate_ret(dict_ret_bus:dict, input_folder:str, output_folder:str,
                  ppa_intermediate_folder:str, plx_busbars:dict,
                  hidrologies:list):
    """ P x Q loads. It dumps a pkl file"""
    # working with ID as key (df_ret)
    dict_id = dict_ret_bus.copy(deep=True)
    dict_id.index = dict_id['ID'].astype(str) # ID es concatenacion "suministrador-clave"
    map_id_bus = dict_id['BARRA_PLEXOS'].to_dict() # {'clave': 'Barra'}
    map_id_company = dict_id['SUMINISTRADOR'].to_dict() # {'clave: 'Empresa'}

    # working with CLAVE as key (df_ret_ppa)
    dict_clave = dict_ret_bus.copy(deep=True)
    dict_clave.index = dict_clave['CLAVE'].astype(str)
    map_clave_bus = dict_clave['BARRA_PLEXOS'].to_dict() # {'clave': 'Barra'}
    map_clave_company = dict_clave['SUMINISTRADOR'].to_dict() # {'clave: 'Empresa'}

    for hidro in hidrologies:
        print_msg(f'calculate_ret: calculando para {hidro}')
      
        # Prepare df_bus
        df_bus = plx_busbars[hidro] # df with busbars and their Marg. Cost
        df_bus.index = pd.MultiIndex.from_frame(
            df_bus[['name', 'date_time']])
        df_bus.sort_index(inplace=True)

        # Iter over pkl files with profiles
        iter_profiles = os.scandir(input_folder)
        profiles_files = (entry for entry in iter_profiles
                         if entry.is_file() and entry.name[-4:] == '.pkl')
        
        for profile in profiles_files:
            print_msg(f'   Revisando {profile.name}')
            profile_abspath= os.path.join(input_folder, profile.name)
            
            # Prepare df_ret to obtain the PxQ (hourly expenditure)
            df_ret = pd.read_pickle(profile_abspath)
            df_ret['ID'] = (df_ret['suministrador'].astype(str) + '-'
                            + df_ret['clave'].astype(str))
            try:
                df_ret.drop(columns=['suministrador_clave'], inplace=True)
            except:
                print('   columna no encontrada. Eliminar este try en el futuro')
            df_ret['busbar'] = df_ret['ID'].astype(str).map(map_id_bus)
            df_ret['company'] = df_ret['ID'].astype(str).map(map_id_company)
            df_ret.drop(columns=['avg_MWh', 'f1', 'f2', 'f3', 'ID'], inplace=True)
            df_ret = df_ret.join(other=df_bus[['price_value']],
                                 on=['busbar', 'date_time'],
                                 how='left')
            df_ret['PxQ'] = df_ret['price_value'] * df_ret['value_MWh']
            inx_pos = df_ret['PxQ'] >= 0 # boolean array
            inx_pos = inx_pos.astype(int) # 1 if ¡hourly_expend.' > 0
            df_ret['PxQ'] = df_ret['PxQ'] * inx_pos

            # Prepare df_ret_ppa: df_ret must have the sum grouped by 'clave'
            ppa_cols = ['date_time', 'clave', 'value_MWh']
            df_ret_ppa = df_ret[ppa_cols].copy(deep=True)
            group_cols = ['date_time', 'clave']
            df_ret_ppa = df_ret_ppa.groupby(by=group_cols).sum()
            df_ret_ppa.reset_index(inplace=True)
            df_ret_ppa['busbar'] = df_ret_ppa['clave'].map(map_clave_bus)
            df_ret_ppa['company'] = df_ret_ppa['clave'].map(map_clave_company)
            df_ret_ppa = df_ret_ppa.join(other=df_bus[['price_value']],
                                         on=['busbar', 'date_time'],
                                         how='left')
            df_ret_ppa['PxQ'] = (df_ret_ppa['price_value']
                                 * df_ret_ppa['value_MWh'])
            inx_pos = df_ret_ppa['PxQ'] >= 0 # boolean array
            inx_pos = inx_pos.astype(int) # 1 if ¡hourly_expend.' > 0
            df_ret_ppa['PxQ'] = df_ret_ppa['PxQ'] * inx_pos

            # Write pickle file. Name of the file begings with
            # the hidrology
            print_msg(f'   escribiendo PPA pickle file')
            output_file = hidro + '___' + profile.name
            output_file = output_file.replace('.pkl', '.zip')
            output_abspath = os.path.join(output_folder,
                                          output_file)
            PickleZip.dump(varname=df_ret, zip_file=output_abspath)

            # Release memory
            del df_ret
            gc.collect()   
            
            # Write pickle file (ppa). Name of the file begings with
            # the hidrology
            print_msg(f'   escribiendo pickle file')
            output_file = hidro + '___' + profile.name
            output_file = output_file.replace('.pkl', '.zip')
            output_abspath = os.path.join(ppa_intermediate_folder,
                                          output_file)
            PickleZip.dump(varname=df_ret_ppa, zip_file=output_abspath)

            # Release memory
            del df_ret_ppa
            gc.collect()

def balance_gen_by_company(input_folder:str, output_pickle_file:str,
                           company_col:str, columns_to_keep:list):
    
    print_msg(f'balance_gen_by_company: leyendo archivos en {input_folder}')
    list_balance = [] # store df with balance
    iter_files = os.scandir(input_folder) # Iter over pkl files with profiles
    files = (entry for entry in iter_files
             if entry.is_file() and entry.name[-4:] == '.zip')
    
    for file in files:
        foo = file.name.split('___')
        hidro = foo[0]
        print_msg(f'   Analizando hidrologia {hidro}')  
        input_abspath = os.path.join(input_folder, file.name)
        df = PickleZip.load(zip_file=input_abspath)
        groups = df.groupby(by=[company_col, 'month']).sum()
        df_f = pd.DataFrame(groups)
        df_f.drop(columns=['price_value'], inplace=True)
        df_f.reset_index(drop=False, inplace=True)
        df_f['hidrology'] = hidro
        df_f = df_f[columns_to_keep] 
        list_balance.append(df_f)
    
    df_balance = pd.concat(list_balance)
    df_balance.to_pickle(output_pickle_file)

def balance_ret_by_company(input_folder:str, output_pickle_file:str,
                           company_col:str, columns_to_keep:list,
                           hidrologies:list):
    
    print_msg(f'balance_ret_by_company: leyendo archivos en {input_folder}')
    df_list_balance = {hidro: [] for hidro in hidrologies}
    list_balance = []
    iter_files = os.scandir(input_folder) # Iter over pkl files with profiles
    files = (entry for entry in iter_files
             if entry.is_file() and entry.name[-4:] == '.zip')
    
    for file in files:
        foo = file.name.split('___')
        hidro = foo[0]
        print_msg(f'   Analizando hidrologia {hidro}')  
        input_abspath = os.path.join(input_folder, file.name)
        df = PickleZip.load(zip_file=input_abspath)
        groups = df.groupby(by=[company_col, 'month']).sum()
        df_f = pd.DataFrame(groups)
        df_f.drop(columns=['price_value'], inplace=True)
        df_f.reset_index(drop=False, inplace=True)
        df_f['hidrology'] = hidro
        df_f = df_f[columns_to_keep] 
        df_list_balance[hidro].append(df_f)
    
    for hidro in hidrologies:
        list_balance.append(pd.concat(df_list_balance[hidro]))

    df_balance = pd.concat(list_balance)
    df_balance.to_pickle(output_pickle_file)

def balance_ppa(input_folder:str, output_pickle_files:list,
                company_col:str, columns_to_keep:list):
    
    def group_by_company(df:pd.DataFrame):
        df_seller = df.copy(deep=True)
        df_seller.drop(columns=['buyer'], inplace=True)
        df_seller.rename(columns={'seller': 'company'}, inplace=True)
        
        df_seller['PxQ'] = -1 * df_seller['PxQ']
        df_seller['value_MWh_1'] = -1 * df_seller['value_MWh_1']

        df_buyer = df.copy(deep=True)
        df_buyer.drop(columns=['seller'], inplace=True)
        df_buyer.rename(columns={'buyer': 'company'}, inplace=True)
        df = pd.concat([df_seller, df_buyer])

        groups = df.groupby(by=[company_col, 'month']).sum()
        df_f = pd.DataFrame(groups)
        df_f.rename(columns={'value_MWh_1': 'value_MWh'}, inplace=True)
        df_f.reset_index(drop=False, inplace=True)
        df_f['hidrology'] = hidro
        df_f = df_f[columns_to_keep]
        return df_f

    def group_by_ppa(df:pd.DataFrame):
        df_p = df.groupby(by=['ppa_id', 'month']).sum() # index: ppa_id
        df_p = df_p[['PxQ', 'value_MWh_1']]
        df_p.rename(columns={'value_MWh_1': 'value_MWh'}, inplace=True)
        df_p.reset_index(drop=False, inplace=True)
        df_p['hidrology'] = hidro
        return df_p

    print_msg(f'balance_ppa: leyendo archivos en {input_folder}')
    balance_by_company = [] # store df with balance by company
    balance_by_ppa = [] # store df with balance by ppa
    iter_files = os.scandir(input_folder) # Iter over pkl files with profiles
    files = (entry for entry in iter_files
             if entry.is_file() and entry.name[-4:] == '.zip')
    
    for file in files:
        foo = file.name.split('___')
        hidro = foo[0]
        print_msg(f'   Analizando hidrologia {hidro}')  
        input_abspath = os.path.join(input_folder, file.name)
        df = PickleZip.load(zip_file=input_abspath)
       
        # Filter by "is_consistent"
        df = df[df['is_consistent'] == True].copy(deep=True)    
        # run balances
        df_company = group_by_company(df=df)
        balance_by_company.append(df_company)
        df_ppa = group_by_ppa(df=df)
        balance_by_ppa.append(df_ppa)
    
    # concat adn write results
    df_balance_by_company = pd.concat(balance_by_company)
    df_balance_by_company.to_pickle(output_pickle_files[0])
    df_balance_by_ppa = pd.concat(balance_by_ppa)
    df_balance_by_ppa.to_pickle(output_pickle_files[1])
    
def main():
    ##################################################################
    ## The good stuff!
    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')

    list_hidros = ['Sample 1', 'Sample 2', 'Sample 3']
   
    # Load Plexos results (generators and busbars)
    print_msg('main: leyendo informacion de barras Plexos')
    pkl_plx_busbars = 'pkl//input//plx_busbars.zip'
    plx_busbars = PickleZip.load(zip_file=pkl_plx_busbars)
    
    print_msg('main: leyendo informacion de generadores Plexos')
    pkl_plx_generators = 'pkl//input//plx_generators.zip'
    plx_generators = PickleZip.load(zip_file=pkl_plx_generators)
 
    # calculate PxQ Iny
    xlsx_dict_gen = 'Input_test//Garantias//diccionarios//Diccionario Centrales v.20220825.xlsx'
    dict_gen_bus = read_dictionary(file=xlsx_dict_gen,
                                   sheet_name='Centrales')
    output_folder= 'pkl//output//generators//PxQ'
    calculate_iny(dict_gen_bus=dict_gen_bus, plx_generators=plx_generators,
                  plx_busbars=plx_busbars, output_folder=output_folder,
                  hidrologies=list_hidros)

    # Release memory
    del plx_generators 
    gc.collect()

    # # Calculate PxQ Ret. As a subproduct, a new dataframe where "suministrador"
    # # is grouped by "clave" is also generated. This is necessary for PPA
    # # simulation

    # xlsx_dict_ret = 'Input_test//Garantias//diccionarios//Retiros_V06.xlsx'
    # dict_ret_bus = read_dictionary(file=xlsx_dict_ret,
    #                                sheet_name='Retiros')
    # input_folder = 'pkl//intermediate//new_profiles'
    # output_folder= 'pkl//output//ret//PxQ'
    # ppa_intermediate_folder = 'pkl//intermediate//ppa//ret'
    # calculate_ret(dict_ret_bus=dict_ret_bus, input_folder=input_folder,
    #               output_folder=output_folder,
    #               ppa_intermediate_folder=ppa_intermediate_folder,
    #               plx_busbars=plx_busbars,
    #               hidrologies=list_hidros)
    
    # # Release memory
    # del plx_busbars 
    # gc.collect()
    
    # # Calculate balance by company: generators
    # input_folder = 'pkl//output//generators//PxQ//'
    # output_pickle_file = 'pkl//output//balance_generators.pkl'
    # columns_to_keep = ['hidrology', 'company', 'month', 'value_MWh',
    #                    'PxQ']
    # balance_gen_by_company(input_folder=input_folder,
    #                        output_pickle_file=output_pickle_file,
    #                        company_col='company',
    #                        columns_to_keep=columns_to_keep)

    # # Calculate balance by company: retiros
    # input_folder = 'pkl//output//ret//PxQ//'
    # output_pickle_file = 'pkl//output//balance_ret.pkl'
    # columns_to_keep = ['hidrology', 'company', 'month', 'value_MWh',
    #                    'PxQ']
    # balance_ret_by_company(input_folder=input_folder,
    #                        output_pickle_file=output_pickle_file,
    #                        company_col='company',
    #                        columns_to_keep=columns_to_keep,
    #                        hidrologies=list_hidros)

    # # simulate PPA
    # gc.collect()
    # os.system('python foo2.py')

    # # Calculate PPA balance
    # input_folder = 'pkl//output//ppa//'
    # output_pickle_files = ['pkl//output//balance_ppa.pkl', 
    #                        'pkl//output//balance_by_ppa.pkl']
    # columns_to_keep = ['hidrology', 'company', 'month', 'value_MWh',
    #                    'PxQ']
    # balance_ppa(input_folder=input_folder,
    #             output_pickle_files=output_pickle_files,
    #             company_col='company',
    #             columns_to_keep=columns_to_keep)

    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo total de ejecucion: {t_delta}')