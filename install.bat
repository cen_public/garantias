@echo off
rem  Calculo de Garantias y automatizacion de carga de datos a Renova'
rem  Creado por: Frontera Ingenieria'
rem  Contacto  : pablo.medina@frontera-ingenieria.cl  +56977660201' 
rem  Archivo de procesamiento por lotes para instalar los scripts correspondientes
rem

rem Ejemplo de uso: install.bat 2024_10 msedge
rem   - La solución queda instalada en c:\Renova\2024_10, y se desplegará en navegador edge
rem   - Para utilizar Google Chrome, escribir "chrome"
rem Setea el navegador
set navigator=%2


rem Crear carpetas (si no existen)
set destiny=C:\Renova\%1\
set "source=%~dp0"
mkdir %destiny%
mkdir %destiny%\Prod
mkdir %destiny%\Input
mkdir %destiny%\Input\Renova
mkdir %destiny%\Input\Renova_test
mkdir %destiny%\Output
mkdir %destiny%\Output\Renova

rem Crea carpetas para cada input RENOVA
mkdir %destiny%\Input\Renova\VALORIZADO
mkdir %destiny%\Input\Renova\BERNC
mkdir %destiny%\Input\Renova\DICCIONARIO_GENERADORES
mkdir %destiny%\Input\Renova\DICCIONARIO_CLIENTES
mkdir %destiny%\Input\Renova\DICCIONARIO_CENTRALES
mkdir %destiny%\Input\Renova\DECIMALES

rem Crea carpetas para cada input RENOVA_test
mkdir %destiny%\Input\Renova_test\VALORIZADO
mkdir %destiny%\Input\Renova_test\BERNC
mkdir %destiny%\Input\Renova_test\DICCIONARIO_GENERADORES
mkdir %destiny%\Input\Renova_test\DICCIONARIO_CLIENTES
mkdir %destiny%\Input\Renova_test\DICCIONARIO_CENTRALES
mkdir %destiny%\Input\Renova_test\DECIMALES

call update_config.bat %destiny% %navigator%


rem Copia los archivos test
copy /y %source%tests\testRenova\input_data\11-2021\BERNCtest11-2021.xlsx %destiny%Input\Renova_test\BERNC\BERNCtest11-2021.xlsx
copy /y %source%tests\testRenova\input_data\11-2021\valorizado_test11_2021.accdb %destiny%Input\Renova_test\VALORIZADO\valorizado_test11_2021.accdb
copy /y %source%tests\testRenova\input_data\11-2021\Diccionario_gen_test11-2021.xlsx %destiny%Input\Renova_test\DICCIONARIO_GENERADORES\Diccionario_gen_test11-2021.xlsx
copy /y %source%tests\testRenova\input_data\11-2021\Diccionario_clientes_test11-2021.xlsx %destiny%Input\Renova_test\DICCIONARIO_CLIENTES\Diccionario_clientes_test11-2021.xlsx
copy /y %source%tests\testRenova\input_data\11-2021\Diccionario_cen_test11-2021.xlsx %destiny%Input\Renova_test\DICCIONARIO_CENTRALES\Diccionario_cen_test11-2021.xlsx
copy /y %source%tests\testRenova\input_data\11-2021\decimales0.xlsx %destiny%Input\Renova_test\DECIMALES\decimales0.xlsx

copy /y %source%tests\testRenova\input_data\12-2021\BERNCtest12-2021.xlsx %destiny%Input\Renova_test\BERNC\BERNCtest12-2021.xlsx
copy /y %source%tests\testRenova\input_data\12-2021\valorizado_test12_2021.accdb %destiny%Input\Renova_test\VALORIZADO\valorizado_test12_2021.accdb
copy /y %source%tests\testRenova\input_data\12-2021\valorizado_test12_2021.csv %destiny%Input\Renova_test\VALORIZADO\valorizado_test12_2021.csv
copy /y %source%tests\testRenova\input_data\12-2021\Diccionario_gen_test12-2021.xlsx %destiny%Input\Renova_test\DICCIONARIO_GENERADORES\Diccionario_gen_test12-2021.xlsx
copy /y %source%tests\testRenova\input_data\12-2021\Diccionario_clientes_test12-2021.xlsx %destiny%Input\Renova_test\DICCIONARIO_CLIENTES\Diccionario_clientes_test12-2021.xlsx
copy /y %source%tests\testRenova\input_data\12-2021\Diccionario_cen_test12-2021.xlsx %destiny%Input\Renova_test\DICCIONARIO_CENTRALES\Diccionario_cen_test12-2021.xlsx
copy /y %source%tests\testRenova\input_data\12-2021\decimales_sobrantes_2021-11.xlsx %destiny%Input\Renova_test\DECIMALES\decimales_sobrantes_2021-11.xlsx

rem  Copiar los archivos a la carpeta destiny, y sobreescribir lo que ahí estén

xcopy /s /y /EXCLUDE:%source%excludedfilelist.txt %source%*.* %destiny%Prod

rem crear ambiente virtual
python -m venv %destiny%\venv
call %destiny%\venv\Scripts\activate.bat
pip3 install -r %source%\requirements.txt
echo Fin install.bat
cmd \k