from configobj import ConfigObj
from SharedCode.Database.DBQuery import DBQueryMySQL
from SharedCode.LoadEnv import load_env
from SharedCode.PlexosReader import PlexosReader
import datetime, os, pickle
import pandas as pd
import itertools

def main(sol_file:str=None, plexos_load_path:str=None, drop_tables:bool=True, return_df:bool=False):
    def run_drop_tables():
        obj_db = DBQueryMySQL()
        with open(file='deploy_get_plexos_data.sql', mode='r') as f:
            deploy_str = f.read()
        obj_db.execute_query(query='standard',
                             operation = deploy_str,
                             params=None,
                             is_destructive=False) # It is non_destructive :\

    def create_df_plx_lod_lod(csv_path):
        # Creata a DataFrame from Plexos load file
        df = pd.read_csv(csv_path)
        
        # Get type of load using split in "NAME": There are some names with two "_"
        # (the last one should be used)
        bar = df['NAME'].str.split('_', n=2, expand=True)
        if len(bar.columns) == 2:
            df['type'] = bar[1]
        elif len(bar.columns) == 3:
            bar[2].fillna(bar[1], inplace=True)
            df['type'] = bar[2]

        df['PATTERN'] = df['PATTERN'].str.replace('_', ',')
        foo = df['PATTERN'].str.split(',', expand=True)
        foo['type'] = df['type']
        foo.rename(columns={0:"day", 1:"month", 2:"hour"}, inplace=True)
        df.index = pd.MultiIndex.from_frame(foo)
        df.index = df.index.reorder_levels(['month', 'day', 'hour', 'type'])
        df.drop(columns=['type'], inplace=True)
        df.sort_index(inplace=True)
        return df
    
    def create_df_plx_lod_esc(csv_path):
        # Creata a DataFrame from Plexos load file
        df = pd.read_csv(csv_path)
        df.rename(columns={'Growth_R': 'R', 'Growth_L': 'L', 'Growth_LD': 'LD'}, inplace=True)
        df['date_time'] = pd.to_datetime(df[['YEAR', 'MONTH', 'DAY']])
        return df
    
    def write_into_db(df:pd.DataFrame, table:str, index:bool, index_label:list, chunksize:int=None):
        # Send values to Database    
        obj_db = DBQueryMySQL()
        chunksize = len(df) if chunksize is None else chunksize
        it = itertools.islice(range(0, len(df)), 0, None, chunksize)
        for i in it:
            df_i = df.iloc[i : min(i + chunksize, len(df)), :]
            try:
                obj_db.insert_df(table=table, df=df_i, if_exists='append',
                                 index=index, index_label=index_label)
            except Exception as e:
                print(f'No se pudo insertar el dataframe')
                print(e)
    
    def write_into_pickle(varname, pickle_file):
        with open(file=pickle_file, mode='wb') as f:
            pickle.dump(varname, f)
        
    # For developing purposes
    if sol_file is None:
        # sol_file = 'Input_test\\Garantias\\plexos\\Model PRGdia_Full_Definitivo Solution.zip'
        # sol_file = 'Input_test\\Garantias\\plexos\\20220421 Model PRGMEN_Base Solution.zip'
        # sol_file = 'Input_test\\Garantias\\plexos\\20220818 - Model PRGMEN_Base Solution.zip'
        # sol_file = 'Input_test\\Garantias\\plexos\\20220831 - Model PRGMEN_Base Solution.zip'
        sol_file = 'Input_test\\Garantias\\plexos\\20220929 - Model PRGMEN_Base Solution.zip'
    
    if plexos_load_path is None:
        cwd = os.getcwd()
        plexos_load_path = os.path.join(cwd, 'Input_test',
                                        'Garantias', 'plexos', 'load')
    csv_lod_lod = 'Load_Load.csv'
    csv_lod_esc = 'Load_Escalator.csv'
    csv_path_lod_lod = os.path.join(plexos_load_path, csv_lod_lod)
    csv_path_lod_esc = os.path.join(plexos_load_path, csv_lod_esc)

    t_start = datetime.datetime.now()
    load_env(filename='local_dev.env')
    cfg = ConfigObj('config.cfg')
    run_drop_tables() if drop_tables == True else lambda x:None
    df_gen = None
    df_bus = None
    cfg_gen = cfg['GARANTIAS']['PLEXOS_COLUMNS']['GENERATORS']
    cfg_bus = cfg['GARANTIAS']['PLEXOS_COLUMNS']['BUSBARS']
    o_plexos = PlexosReader(sol_file=sol_file)
    
    index_label = ['hidrology', 'date_time', 'name'] # Same index for both df
    
    # Información de barras y costos marginales
    print(f'Obteniendo informacion de barras {datetime.datetime.now()}')
    df_bus= o_plexos.get_results_csv(type='busbars',
                                     year=2023,
                                     property='Price',
                                     file='plx_busbars.csv',
                                     usecols=cfg_bus.keys(),
                                     rename_cols=cfg_bus,
                                     datetime_col='_date',
                                     value_col='price_value',
                                     table = None,
                                     column_split='hidrology',
                                     index_db=index_label,
                                     index=False,
                                     index_label=index_label,
                                     upload_from_file=True,
                                     return_df=return_df)
    
    # Información de generadores y su producción neta
    print(f'Obteniendo informacion de generadores {datetime.datetime.now()}')
    
    df_gen = o_plexos.get_results_csv(type='generators',
                                      year=2023,
                                      property='Generation',
                                      file='plx_gen.csv',
                                      usecols=cfg_gen.keys(),
                                      rename_cols=cfg_gen,
                                      datetime_col='_date',
                                      value_col='gen_value',
                                      table = None,
                                      column_split='hidrology',
                                      index_db=index_label, 
                                      index=True,
                                      index_label=index_label,
                                      upload_from_file=True,
                                      return_df=return_df)
    
    # load_load.csv
    df_plx_lod_lod = create_df_plx_lod_lod(csv_path=csv_path_lod_lod)
    write_into_db(df=df_plx_lod_lod, table='lod_lod', index=True,
                  index_label=['month', 'day', 'hour', 'type'], chunksize=20000)
    pickle_file = 'pkl//input//plx_lod_lod.pkl'
    write_into_pickle(varname=df_plx_lod_lod, pickle_file=pickle_file)

    # load_escale.csv
    df_plx_lod_esc = create_df_plx_lod_esc(csv_path=csv_path_lod_esc)
    write_into_db(df=df_plx_lod_esc, table='lod_esc', index=False,
                  index_label=None)
    pickle_file = 'pkl//input//plx_lod_esc.pkl'
    write_into_pickle(varname=df_plx_lod_esc, pickle_file=pickle_file)

    t_end = datetime.datetime.now()
    t_delta = t_end - t_start
    print(f'Tiempo total de ejecucion: {t_delta}')