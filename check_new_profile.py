import pandas as pd
from SharedCode.Database.PickleZip import PickleZip

# Set date values
# month = '01'
# hour = '15'
# day = '03'
# day_n = 'TR'

month = '12'
hour = '12'
day = '07'
day_n = 'TR'

hours = {'00': 'H1',
         '01': 'H2',
         '02': 'H3',
         '03': 'H4',
         '04': 'H5',
         '05': 'H6',
         '06': 'H7',
         '07': 'H8',
         '08': 'H9',
         '09': 'H10',
         '10': 'H11',
         '11': 'H12',
         '12': 'H13',
         '13': 'H14',
         '14': 'H15',
         '15': 'H16',
         '16': 'H17',
         '17': 'H18',
         '18': 'H19',
         '19': 'H20',
         '20': 'H21',
         '21': 'H22',
         '22': 'H23',
         '23': 'H24'}


# Values from the new profile
df_new = pd.read_pickle(f'pkl//intermediate//new_profiles//RETIROS_23{month}.pkl') 
dt = f'2023-{month}-{day} {hour}:00:00'

df_new_dt = df_new[df_new['date_time'] == dt]
g_df_new = df_new_dt.groupby(['type'])

df_new_dt_sum = {}
for g in g_df_new.groups.keys():
    df_new_dt_sum.update({g:  g_df_new.get_group(g)['value_MWh'].sum()})

df_lod_lod = pd.read_pickle('pkl//input//plx_lod_lod.pkl')

# Values for lod_lod
g_df_lod_lod = df_lod_lod.groupby(['type'])
df_lod_lod_dt_sum = {}
for g in g_df_lod_lod.groups.keys():
    dt_n1 = (month, day_n, hours[hour], g)
    df_lod_lod_dt_sum.update({g: df_lod_lod.groupby(['type']).get_group(g)['VALUE'].loc[dt_n1].sum()})

# Values for lod_esc
df_lod_esc = pd.read_pickle('pkl//input//plx_lod_esc.pkl')

dt_n2 = f'2023-{month}-{day} 00:00:00'
condition1 = dt_n2 >= df_lod_esc['date_time']
row = df_lod_esc[(condition1)].iloc[-1:] # el ultimo que cumple la condicion

print(df_new_dt_sum)
print(df_lod_lod_dt_sum)
print(row)
idx = pd.IndexSlice

# exec(open('check_new_profile.py').read())