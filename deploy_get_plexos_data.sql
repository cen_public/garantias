use garantias_renova;
drop table if exists generators;
CREATE TABLE IF NOT EXISTS generators (
    hidrology NVARCHAR(15),
    date_time DATETIME,
    name NVARCHAR(40),
    gen_value FLOAT,
    unit CHAR(4));
/* ALTER TABLE generators ADD INDEX inx (hidrology, date_time, name); */

drop table if exists busbars;
CREATE TABLE IF NOT EXISTS busbars (
    hidrology NVARCHAR(15),
    date_time DATETIME,
    name NVARCHAR(40),
    price_value FLOAT,
    unit CHAR(5));
/* ALTER TABLE busbars ADD INDEX inx (hidrology, date_time, name); */

drop table if exists lod_lod;
CREATE TABLE IF NOT EXISTS lod_lod (
    day CHAR(2),
    month CHAR(2),
    hour CHAR(4),
    type CHAR(5),
    name NVARCHAR(40),
    pattern CHAR(10),
    value FLOAT);
/* ALTER TABLE lod_lod ADD INDEX inx (day, month, hour, type); */

drop table if exists lod_esc;
CREATE TABLE IF NOT EXISTS lod_esc (
    year INT,
    month INT,
    day INT,
    R FLOAT,
    L FLOAT,
    LD FLOAT,
    date_time DATETIME);